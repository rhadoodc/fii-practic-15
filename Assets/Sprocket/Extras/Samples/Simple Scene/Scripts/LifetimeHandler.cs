﻿using UnityEngine;
using System.Collections;
using SprocketTools.MemoryPooling;

public class LifetimeHandler : KPoolableMonoBehaviour
{
	private float currentLifeTime;
	public float lifeTime;
	public float lifeTimeVariance;

	private bool pooledObject = false;

	public override void Initialize()
	{

	}

	public override void Reset()
	{
		currentLifeTime = Random.Range(lifeTime - lifeTimeVariance, lifeTime + lifeTimeVariance);
	}

	void Start () 
	{
		//Disabling the MeshCollider reduces the runtime penalty. Its presence is only meant to simulate expensive initialization.
		MeshCollider meshCollider = GetComponent<MeshCollider>();
		if (meshCollider)
			meshCollider.enabled = false;

		currentLifeTime = Random.Range(lifeTime - lifeTimeVariance, lifeTime + lifeTimeVariance);

		if (Spawner.UseGameObjectPooling)
			pooledObject = true;
		else
			pooledObject = false;

		//simulate expensive startup
		//for (int i = 0; i < 100; i++)
		//{

		//}
	}
	
	void Update () 
	{
		currentLifeTime -= Time.deltaTime;

		if (currentLifeTime <= 0)
		{
			if (pooledObject)
			{
				KGameObjectPoolManager.Recover(gameObject);
			}
			else
			{
				GameObject.Destroy(gameObject);
			}
		}
	}
}
