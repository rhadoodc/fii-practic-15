﻿// MatCap Shader, (c) 2013,2014 Jean Moreno

Shader "Custom/Cull Test"
{
	Properties
	{
		_MainTex("Diffuse", 2D) = "white" {}
		_MatCap("MatCap (RGB)", 2D) = "white" {}
		_MatCapPower("MatCapPower", Range(1.0, 4.0)) = 2.0
	}

		Subshader
	{
		//Tags{ "Queue" = "Transparent"}
		Tags{ "Queue" = "Geometry" }

		Pass
		{
			Cull Front
			//Cull Off
			/*ZWrite Off*/
			Lighting Off			

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_nicest
			#include "UnityCG.cginc"

			struct v2f
			{
				float4 pos : SV_POSITION;
				float3 normal : NORMAL;
				float2 uv : TEXCOORD0;
			};

			uniform float4 _MainTex_ST;

			v2f vert(appdata_base v)
			{
				v2f o;

				v.vertex.xyz = v.vertex.xyz + v.normal * 0.015;

				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
				o.normal = mul(UNITY_MATRIX_MVP, v.normal);

				return o;
			}

			uniform sampler2D _MainTex;

			fixed4 frag(v2f i) : COLOR
			{
				//fixed4 tex = tex2D(_MainTex, i.uv);
				//if (i.normal.z <= 0.1)
				//	clip(-1);
				//clip(i.normal.z + 0.1);

				return fixed4(0, 0, 0, 0);
			}
			ENDCG
		}


		Pass
		{
			Offset 0, 0
			ZWrite On
			ZTest LEqual

			Tags{ "LightMode" = "Always" }

			CGPROGRAM
				#pragma vertex vert
				#pragma fragment frag
				#pragma fragmentoption ARB_precision_hint_nicest
				#include "UnityCG.cginc"

				struct v2f
				{
					float4 pos : SV_POSITION;
					float3 normal : NORMAL;
					float3 worldPos : TEXCOORD0;
					float2 uv : TEXCOORD1;
					float2 cap : TEXCOORD2;
				};

				uniform float4 _MainTex_ST;

				v2f vert(appdata_base v)
				{
					v2f o;
					o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
					o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
					o.normal = mul(UNITY_MATRIX_MVP, v.normal);
					o.worldPos = mul(_Object2World, v.vertex);

					half2 capCoord;
					capCoord.x = dot(UNITY_MATRIX_IT_MV[0].xyz, v.normal);
					capCoord.y = dot(UNITY_MATRIX_IT_MV[1].xyz, v.normal);
					o.cap = capCoord * 0.5 + 0.5;

					return o;
				}

				uniform sampler2D _MainTex;
				uniform sampler2D _MatCap;
				fixed _MatCapPower;

				fixed4 frag(v2f i) : COLOR
				{
					fixed4 tex = tex2D(_MainTex, i.uv);
					//float3 camViewDir = normalize(_WorldSpaceCameraPos - i.worldPos);
					//if (-i.normal.z < -0.1)
					//	return fixed4(0, 0, 0, 0);
					fixed4 mc = tex2D(_MatCap, i.cap);

					return (tex + (mc * _MatCapPower) - 1.0);
					//return tex;
				}
			ENDCG
		}
	}

	Fallback "VertexLit"
}