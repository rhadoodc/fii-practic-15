﻿Shader "Custom/DebugGBuffer"
{
/*
	Properties 
	{
		_Color ("Color", Color) = (1,1,1,1)
		_MainTex ("Albedo (RGB)", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
	}
*/

	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct v2f
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			sampler2D DummyTex;
			uniform float4 DummyTex_ST;

			v2f vert(appdata_base v)
			{
				v2f vOut;
				vOut.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				vOut.uv = TRANSFORM_TEX(v.texcoord, DummyTex);

				return vOut;
			}

			fixed4 frag(v2f vIn) : COLOR
			{
//				fixed4 col = tex2d(CameraGBufferTexture0, vIn.uv);

				return fixed4(0, 0, 0, 0);
			}
		
			ENDCG
		}
	}
	FallBack "Diffuse"
}
