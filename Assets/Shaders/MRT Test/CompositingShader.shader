﻿Shader "Custom/CompositingShader" {
	Properties
		{
			_Noise ("Noise", 2D) = "white" {}
			_rt_out0 ("RT0", 2D) = "" {}
			_rt_out1 ("RT1", 2D) = "" {}
			_rt_out2 ("RT2", 2D) = "" {}
			_rt_out3 ("RT3", 2D) = "" {}

			_A ("Shoulder Strength", Range(0.0,1.0)) = 0.15
			_B  ("Linear Strength", Range(0.0,1.0)) = 0.55
			_C  ("Linear Angle", Range(0.0,1.0)) = 0.1
			_D ("Toe Strength", Range(0.0,1.0)) = 0.2
			_E ("Toe Numerator", Range(0.0,1.0)) = 0.02
			_F ("Toe Denominator", Range(0.0,2.0)) = 0.7
			_W ("Weight", Range(0.0,20.0)) = 10.2
			_ExposureAdjustment ("Exposure Adjustment", Range(0.0,20.0)) = 16.0
			_ExposureBias ("Exposure Bias", Range(0.0,3.0)) = 2.0
		}

	SubShader {

		Tags { "Queue" = "Transparent" "RenderType" = "Transparent" }
		LOD 200
		
		Pass
		{
			ZTest Always

			CGPROGRAM
			// Physically based Standard lighting model, and enable shadows on all light types
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			sampler2D _rt_out0; //Diffuse Albedo
			sampler2D _rt_out1; //Normals & Depth
			sampler2D _rt_out2;
			sampler2D _rt_out3; //Light Contribution

			sampler2D _Noise;

			uniform float _A;
			uniform float _B;
			uniform float _C;
			uniform float _D;
			uniform float _E;
			uniform float _F;
			uniform float _W;

			uniform float _ExposureAdjustment;
			uniform float _ExposureBias;

			float3 FilmicTonemap(float3 x)
			{
				return ((x*(_A*x+_C*_B)+_D*_E)/(x*(_A*x+_B)+_D*_F))-_E/_F;
			}

			struct v_in
			{
				float4 vertex : POSITION;
				float4 texcoord : TEXCOORD0;
			};

			struct f_in
			{
				float4 position : POSITION;
				float4 texcoord : TEXCOORD0;
			};

			f_in vert(v_in input, uint vertexID : SV_VertexID)
			{
				f_in output;

				output.position = mul (UNITY_MATRIX_MVP, input.vertex);
				output.texcoord = input.texcoord;

				return output;
			}

		

			float4 frag(f_in input) : sv_target
			{
				float4 albedo = tex2D(_rt_out0, input.texcoord.xy);
				float4 depth_normal = tex2D(_rt_out1, input.texcoord.xy);
				float4 lightContribution = tex2D(_rt_out3, input.texcoord.xy);

				float3 RTColor = albedo * unity_AmbientSky + lightContribution.xyz;
				RTColor *= _ExposureAdjustment;

				float3 TonemappedColor = FilmicTonemap(_ExposureBias * RTColor);

				float3 WhiteScale = FilmicTonemap(_W);
				float3 WhiteBalancedColor = TonemappedColor * WhiteScale;

				float3 FinalColor = pow(WhiteBalancedColor, 1.0/2.2);

				return float4(RTColor, 1.0f);
			}

			ENDCG
		}
	} 
	
	FallBack Off
}
