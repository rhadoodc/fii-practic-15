﻿Shader "Custom/MRTDebugShader" {
	Properties
		{
			_rt_out0 ("RT0", 2D) = "" {}
			_rt_out1 ("RT1", 2D) = "" {}
			_rt_out2 ("RT2", 2D) = "" {}
			_rt_out3 ("RT3", 2D) = "" {}

			_A ("Shoulder Strength", Range(0.0,1.0)) = 0.15
			_B  ("Linear Strength", Range(0.0,1.0)) = 0.55
			_C  ("Linear Angle", Range(0.0,1.0)) = 0.1
			_D ("Toe Strength", Range(0.0,1.0)) = 0.2
			_E ("Toe Numerator", Range(0.0,1.0)) = 0.02
			_F ("Toe Denominator", Range(0.0,2.0)) = 0.7
			_W ("Weight", Range(0.0,20.0)) = 10.2
			_ExposureAdjustment ("Exposure Adjustment", Range(0.0,20.0)) = 16.0
			_ExposureBias ("Exposure Bias", Range(0.0,3.0)) = 2.0
		}

	SubShader {

		Tags { "Queue" = "Transparent" "RenderType" = "Transparent" }
		LOD 200
		
		Pass
		{
			ZTest Always

			CGPROGRAM
			// Physically based Standard lighting model, and enable shadows on all light types
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			sampler2D _rt_out0;
			sampler2D _rt_out1;
			sampler2D _rt_out2;
			sampler2D _rt_out3;

			uniform float _A;
			uniform float _B;
			uniform float _C;
			uniform float _D;
			uniform float _E;
			uniform float _F;
			uniform float _W;

			uniform float _ExposureAdjustment;
			uniform float _ExposureBias;
		
			struct v_in
			{
				float4 vertex : POSITION;
				float4 texcoord0 : TEXCOORD0;
			};

			struct f_in
			{
				float4 position : POSITION;
				float4 texcoord0 : TEXCOORD0;
			};

			float3 FilmicTonemap(float3 x)
			{
				return ((x*(_A*x+_C*_B)+_D*_E)/(x*(_A*x+_B)+_D*_F))-_E/_F;
			}

			float3 ReinhardTonemap(float3 x)
			{
				return x / (x + 1.0f);
			}

			f_in vert(v_in input)
			{
				f_in output;

				output.position = mul (UNITY_MATRIX_MVP, input.vertex);
				output.texcoord0 = input.texcoord0;

				return output;
			}

			float4 frag(f_in input) : SV_Target
			{
				if ( input.texcoord0.x > 0.5f ) 
				{
					if (input.texcoord0.y > 0.5f) //upper right
					{
						return float4(tex2D(_rt_out0, (input.texcoord0.xy - float2(0.5f, 0.5f)) * float2(2.0f, 2.0f)));
					}
					else //lower right
					{
						float depth;
						float3 normal;
						float4 depth_normal_sample = tex2D(_rt_out1,  (input.texcoord0.xy - float2(0.5f, 0.0f)) * float2(2.0f, 2.0f));
						normal = depth_normal_sample.xyz;
						depth = depth_normal_sample.w;

						//DecodeDepthNormal (tex2D(_rt_out1,  (input.texcoord0.xy - float2(0.5f, 0.0f)) * float2(2.0f, 2.0f)), depth, normal);

						return float4(normal, 1.0f);
					}
				} 
				else 
				{
					if (input.texcoord0.y > 0.5f) //upper left
					{
						float4 RTValue = tex2D(_rt_out3,  (input.texcoord0.xy - float2(0.0f, 0.5f)) * float2(2.0f, 2.0f));

						float3 RTColor = RTValue.xyz;
						RTColor *= _ExposureAdjustment; //Exposure Adjustment

						float3 TonemappedColor = FilmicTonemap(_ExposureBias * RTColor);

						float3 WhiteScale = FilmicTonemap(_W);
						float3 WhiteBalancedColor = TonemappedColor * WhiteScale;

						float3 FinalColor = pow(WhiteBalancedColor, 1.0/2.2);

						return float4(FinalColor, 1.0f);
					}
					else //lower left
					{
						float depth;
						float3 normal;

						float4 depth_normal_sample = tex2D(_rt_out1,  (input.texcoord0.xy - float2(0.0f, 0.0f)) * float2(2.0f, 2.0f));
						normal = depth_normal_sample.xyz;
						depth = depth_normal_sample.w;

						//DecodeDepthNormal (tex2D(_rt_out1,  (input.texcoord0.xy - float2(0.0f, 0.0f)) * float2(2.0f, 2.0f)), depth, normal);

						depth = Linear01Depth(depth);

						return float4(depth, depth, depth, 1.0f);
					}
				}
			}

			ENDCG
		}
	} 
	
	FallBack Off
}
