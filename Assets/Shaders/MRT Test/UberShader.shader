﻿Shader "Custom/UberShader" 
{
	Properties 
	{
		_AddColor ("Additive Color", Color) = (0,0,0,1)
		_MulColor ("Multiplicative Color", Color) = (1,1,1,1)
		_Albedo ("Albedo (RGB)", 2D) = "white" {}
		[NoScaleOffset]
		_Normal ("Normals", 2D) = "white" {}
		_Glossiness ("Smoothness", Range(0,1)) = 0.5
		_Metallic ("Metallic", Range(0,1)) = 0.0
	}

	SubShader 
	{
		Tags { "RenderType" = "Opaque" }
		LOD 200
		
		Pass
		{
			ZTest Less
            ZWrite On

			CGPROGRAM

			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			sampler2D _Albedo;
			sampler2D _Normal;
			
			float4 _Albedo_ST;

			float _ZNear;
			float _ZFar;

			fixed4 _AddColor;
			fixed4 _MulColor;

			struct VS_INPUT
			{
				float4 position : POSITION;
				float3 normal : NORMAL;
				float4 tangent : TANGENT;

				float4 texcoord0 : TEXCOORD0;
			};

			struct VS_OUTPUT
			{
				float4 position : SV_POSITION;
				float3 normal : NORMAL;
				float3 tangent : TANGENT;
				float3 binormal : TEXCOORD1;
				float4 diffuse : COLOR0;
				float2 texcoord0 : TEXCOORD0;
			};

			struct FS_OUTPUT
			{
				float4 diffuse : SV_Target0;
				float4 normal_depth : SV_Target1;
				//float4 specular : SV_Target2;
				//float4 matID : SV_Target3;
			};

			VS_OUTPUT vert(VS_INPUT input)
			{
				VS_OUTPUT output;
				
				output.position = mul (UNITY_MATRIX_MVP, input.position);
				output.normal = normalize(mul(float4 (input.normal, 0.0f), _World2Object).xyz);
				output.tangent = normalize(mul(_Object2World, input.tangent).xyz);
				output.binormal = normalize(cross(output.normal, output.tangent) * input.tangent.w);
				output.texcoord0 = input.texcoord0;//TRANSFORM_TEX(input.texcoord0, _Albedo);

				return output;
			}

			FS_OUTPUT frag(VS_OUTPUT input)
			{
				FS_OUTPUT output;

				output.diffuse = tex2D(_Albedo, input.texcoord0) * _MulColor + _AddColor;

				fixed3 objectSpaceNormalMap = UnpackNormal(tex2D(_Normal, input.texcoord0));

				float3x3 localToWorldTranspose = float3x3(input.tangent, input.binormal, input.normal);

				float3 normalDirection = normalize(mul(objectSpaceNormalMap, localToWorldTranspose));

				// Write raw Z depth value (thus inverse projection in PointLightShader appears to be working correctly as also exemplified here:
				// http://stackoverflow.com/questions/7692988/opengl-math-projecting-screen-space-to-world-space-coords-solved )
				
				output.normal_depth = float4(normalDirection, input.position.z);

				return output;
			}

			ENDCG
		}
	}

	FallBack Off
}
