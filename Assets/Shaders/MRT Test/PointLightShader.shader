﻿Shader "Custom/PointLightShader" 
{
	Properties 
	{
		_Color ("Light Color", Color) = (1,1,1,1)
	}

	SubShader 
	{
		Tags { "Queue"="Transparent" "RenderType"="Transparent" }
		LOD 200
		
		Pass
		{
            ZWrite Off
			ZTest Off
			Blend One One
			Cull Back

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag

			#include "UnityCG.cginc"

			fixed4 _Color;
			fixed4 _LightColor;
			float _Range;
			half _LightIntensity;
			float4 _Position;
			float4 _CamPosition;

			float _ZNear;
			float _ZFar;

//			float4 _ScreenSize;

			uniform float4x4 _ViewProjectInverse;
			float4x4 _CameraToWorld;
			float4x4 _WorldToCamera;

			sampler2D _rt_in0; //Diffuse Albedo
			sampler2D _rt_in1; //Normals & Depth
			sampler2D _rt_in2;
			//sampler2D _rt_in_out; //Light Contribution RT Input

			struct VS_INPUT
			{
				float4 position : SV_POSITION;
				float4 normal : NORMAL;
			};

			struct VS_OUTPUT
			{
				float4 position : SV_POSITION;
				float4 screenSpace : TEXCOORD0;
				float4 normal : NORMAL;
				float4 diffuse : COLOR0;
				
			};

			struct FS_OUTPUT
			{
				float4 light_contribution : SV_Target0;
			};

			VS_OUTPUT vert(VS_INPUT input)
			{
				VS_OUTPUT output;
				
				output.position = mul (UNITY_MATRIX_MVP, input.position);
				output.normal = mul(UNITY_MATRIX_IT_MV, input.normal);
				output.screenSpace = ComputeScreenPos(output.position);

				return output;
			}

			FS_OUTPUT frag(VS_OUTPUT input)
			{
				FS_OUTPUT output;
				
				float3 lightPosition = _Position.xyz;

				float2 fragmentScreenCoords = input.screenSpace.xy / input.screenSpace.w;

				float depth;
				float3 normal;

				float4 depth_normal_sample = tex2D(_rt_in1, fragmentScreenCoords);

				normal = depth_normal_sample.xyz;
				depth = 2.0 * depth_normal_sample.w - 1.0;

				//Reconstruct MRT fragment world position from depth and screen coords
				float4 H = float4(fragmentScreenCoords.x * 2.0 - 1.0, fragmentScreenCoords.y * 2.0 - 1.0, depth, 1.0);
				float4 D = mul (_ViewProjectInverse, H);
				
				float3 fragmentWorldPosition = (D / D.w).xyz;

				//calculate distance between fragment & light
				//surface-to-light vector
				float3 lightVector = lightPosition - fragmentWorldPosition;

				//compute attenuation based on distance - linear attenuation
				float attenuation = saturate(1.0 - length(lightVector) / _Range);
				
				// alternate attenuation method 
				//float attenuationDenom = length(lightVector) / _Range + 1.0;
				//float attenuation = 1.0 / (attenuationDenom * attenuationDenom);
				//attenuation = max((attenuation - 0.25) / (1.0 - 0.25), 0.0);

				//normalize light vector
				lightVector = normalize(lightVector);

				//compute diffuse light (lambertian reflectance)
				float NdL = max(0, dot(normal, lightVector));
				float3 diffuseLight = NdL * _LightColor.rgb;

				//reflection vector
				float3 reflectionVector = normalize(reflect(-lightVector, normal));
				//camera-to-surface vector
				float3 directionToCamera = normalize(_CamPosition - fragmentWorldPosition);
				//compute specular light
				float specularLight = /*specularIntensity*/0.2 * pow(saturate(dot(reflectionVector, directionToCamera)), /*specularPower*/ 0.5 * 255);

				//take into account attenuation and lightIntensity.
				output.light_contribution = float4(attenuation * _LightIntensity * diffuseLight.rgb, 1.0f);

				return output;
			}
			ENDCG
		}
	} 
	FallBack "Diffuse"
}
