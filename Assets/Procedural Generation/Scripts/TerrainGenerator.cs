﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

[Serializable]
public class TerrainGenerator
{
	private class GeneratorWrapper
	{
		public GeneratorWrapper(Generator gen, double _premulX = 1.0d, double _premulY = 1.0d, double _premulZ = 1.0d, double _preaddX = 0.0d, double _preaddY = 0.0d, double _preaddZ = 0.0d, double _premulUniform = 1.0d, double _postmul = 1.0d, double _postadd = 0.0d)
		{
			generate = gen;
			premulX = _premulX;
			premulY = _premulY;
			premulZ = _premulZ;
			preaddX = _preaddX;
			preaddY = _preaddY;
			preaddZ = _preaddZ;
			postmul = _postmul;
			postadd = _postadd;
			premulUniform = _premulUniform;
		}

		public Generator generate;
		public double premulX = 1.0f;
		public double premulY = 1.0f;
		public double premulZ = 1.0f;
		public double premulUniform = 1.0f;
		public double preaddX = 0.0f;
		public double preaddY = 0.0f;
		public double preaddZ = 0.0f;
		public double postmul = 1.0f;
		public double postadd = 0.0f;
	}

	public delegate double Generator(double x, double y, double z/*, double premulX = 1.0d, double premulY = 1.0d, double premulZ = 1.0d, double preaddX = 0.0d, double preaddY = 0.0d, double preaddZ = 0.0d, double postmul = 1.0d, double postadd = 0.0d*/);

	private List<GeneratorWrapper> generatorStack = new List<GeneratorWrapper>();

	public double GetSample(double x, double y, double z)
	{
		double ret = 0;

		foreach (var generator in generatorStack)
		{
			ret += generator.generate((x * generator.premulX + generator.preaddX) * generator.premulUniform, (y * generator.premulY + generator.preaddY) * generator.premulUniform, (z * generator.premulZ + generator.preaddZ) * generator.premulUniform) * generator.postmul + generator.postadd;
		}

		return ret;
	}

	public void AddGenerator(Generator gen, double premulX = 1.0d, double premulY = 1.0d, double premulZ = 1.0d, double preaddX = 0.0d, double preaddY = 0.0d, double preaddZ = 0.0d, double premulUniform = 1.0d, double postmul = 1.0d, double postadd = 0.0d)
	{
		generatorStack.Add(new GeneratorWrapper(gen, _premulX: premulX, _premulY: premulY, _premulZ: premulZ, _premulUniform: premulUniform, _preaddX: preaddX, _preaddY: preaddY, _preaddZ: preaddZ, _postmul: postmul, _postadd: postadd));
	}

	public void ClearGeneratorStack()
	{
		generatorStack.Clear();
	}
}
