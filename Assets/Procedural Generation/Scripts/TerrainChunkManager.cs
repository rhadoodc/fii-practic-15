﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEngine;

public class TerrainChunkManager : MonoBehaviour
{
	private static TerrainChunkManager _instance;

	public Material material;

	public int ChunkSizeX = 16;
	public int ChunkSizeY = 16;
	public int ChunkSizeZ = 16;

	public float ChunkSamplingSizeX = 1.5f;
	public float ChunkSamplingSizeY = 1.5f;
	public float ChunkSamplingSizeZ = 1.5f;

	public float VoxelRadius = 0.5f;

	public bool AdaptiveSampling = true;

	public bool DrawGizmos = false;

	public bool SingleSideNormals = false;

	public bool GenerateCollider = false;

	public bool AnalysisRequired
	{
		set 
		{
			foreach (var chunk in instancedChunks.Values)
			{
				chunk.AnalysisRequired = value;
			}
		}
	}

	private static object _lock = new object();

	private static bool applicationIsQuitting = false;
	/// <summary>
	/// When Unity quits, it destroys objects in a random order.
	/// In principle, a Singleton is only destroyed when application quits.
	/// If any script calls Instance after it has been destroyed, 
	///   it will create a buggy ghost object that will stay on the Editor scene
	///   even after stopping play mode. Really bad!
	/// So, this was made to be sure we're not creating that buggy ghost object.
	/// </summary>
	public void OnDestroy()
	{
		applicationIsQuitting = true;
	}

	public static TerrainChunkManager Instance
	{
		get
		{
			if (_instance == null)
			{
				if (applicationIsQuitting)
				{
					return null;
				}

				lock (_lock)
				{
					if (_instance == null)
					{
						var instances = FindObjectsOfType(typeof(TerrainChunkManager));
						_instance = (TerrainChunkManager)instances[0];

						if (instances.Length > 1)
						{
							Debug.LogError("Something went really wrong - there should never be more than 1 TerrainChunkManager!");
						}

						if (_instance == null)
						{
							GameObject go = new GameObject();
							_instance = go.AddComponent<TerrainChunkManager>();
							go.name = "TerrainChunkManager";

							DontDestroyOnLoad(go);
						}
					}

					return _instance;
				}
			}

			return _instance;
		}
	}

	public static TerrainChunkManager ThreadInstance
	{
		get
		{
			return _instance;
		}
	}
	
	public void Nudge()
	{
		return;
	}

	public Dictionary<Vector3, TerrainChunk> instancedChunks = new Dictionary<Vector3, TerrainChunk>();

	public void CreateChunk(Vector3 position)
	{
		var go = new GameObject("TerrainChunk" + position.ToString());

		var meshFilter = go.AddComponent<MeshFilter>();
		var meshRenderer = go.AddComponent<MeshRenderer>();
		var meshCollider = go.AddComponent<MeshCollider>();
		var chunk = go.AddComponent<TerrainChunk>();

		meshRenderer.material = material;

		chunk.InitializeComponents(meshFilter, meshRenderer, meshCollider);

		go.transform.position = new Vector3(position.x * ChunkSizeX * VoxelRadius * 2f, position.y * ChunkSizeY * VoxelRadius * 2f, position.z * ChunkSizeZ * VoxelRadius * 2f);

		instancedChunks[position] = chunk;
	}

	Vector3[] chunkPositions = new Vector3[]
	{
		new Vector3(0f, 0f, 0f),
		new Vector3(-1f, 0f, 0f),
		new Vector3(1f, 0f, 0f),
		
		new Vector3(-1f, 0f, -1f),
		new Vector3(0f, 0f, -1f),
		new Vector3(1f, 0f, -1f),
		
		new Vector3(-1f, 0f, 1f),
		new Vector3(0f, 0f, 1f),
		new Vector3(1f, 0f, 1f),

		new Vector3(-1f, -1f, 0f),
		new Vector3(0f, -1f, 0f),
		new Vector3(1f, -1f, 0f),
		
		new Vector3(-1f, -1f, -1f),
		new Vector3(0f, -1f, -1f),
		new Vector3(1f, -1f, -1f),
		
		new Vector3(-1f, -1f, 1f),
		new Vector3(0f, -1f, 1f),
		new Vector3(1f, -1f, 1f),

		/*
		new Vector3(-1f, -1f, 0f),
		//new Vector3(0f, -1f, 0f),
		//new Vector3(1f, -1f, 0f),
		
		new Vector3(-1f, -1f, -1f),
		new Vector3(0f, -1f, -1f),
		//new Vector3(1f, -1f, -1f),
		
		new Vector3(-1f, -1f, 1f),
		//new Vector3(0f, -1f, 1f),
		//new Vector3(1f, -1f, 1f),

		new Vector3(1f, 1f, -1f),
		new Vector3(1f, 1f, 0f),
		new Vector3(1f, 1f, 1f),

		new Vector3(0f, 1f, -1f),
		new Vector3(0f, 1f, 0f),
		new Vector3(0f, 1f, 1f),
		*/
	};
	
	public float status = -1.0f;
	int i = 0;

	public void Start()
	{
		i = 0;		
	}
	
	public void Update()
	{
		status = -1.0f;
		if (i < chunkPositions.Length)
		{
			status = i / chunkPositions.Length;
				
			var chunkPos = chunkPositions[i];

			CreateChunk(chunkPos);
			
			i++;
		}
	}
}

