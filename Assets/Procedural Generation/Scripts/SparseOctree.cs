﻿using System;
using System.Collections;
using System.Linq;
using System.Text;

using UnityEngine;

public class SparseOctree
{
	float volumeRadius;
	const float voxelRadius = 0.25f;

	private class Node
	{
		public float value;
		public int childOffset;
		public byte childMask;

		public Vector3 center;

		public Node()
		{
			value = 0.0f;
			childOffset = 0;
			childMask = 0;

		}
	}

	ArrayList nodes = new ArrayList();

	private readonly short[] bitMasks = new short[] { 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048 };

	SparseOctree()
	{
		var root = new Node();

		nodes.Add(root);
	}

	float this[float whereX, float whereY, float whereZ]
	{
		get 
		{
			var currentNode = nodes[0] as Node;
			var radius = volumeRadius * 0.5f;
			byte childIndex = 0;

			while (radius >= voxelRadius)
			{
				if (whereX > currentNode.center.x)
				{
					childIndex &= 1;
				}

				if (whereY > currentNode.center.y)
				{
					childIndex &= 2;
				}

				if (whereZ > currentNode.center.z)
				{
					childIndex &= 4;
				}

				if ((currentNode.childMask & bitMasks[childIndex]) == 0)
				{
					Debug.LogError("Voxel has not been previously set for required depth, returning last valid value. [voxel position: " + whereX + " " + whereY + " " + whereZ + "].");
					return currentNode.value;
				}

				var childNumber = 0;

				for (int i = 0; i < childIndex; i++)
				{
					childNumber += currentNode.childMask & bitMasks[i];
				}

				currentNode = nodes[currentNode.childOffset + childNumber] as Node;
				radius *= 0.5f;
				childIndex = 0;
			}

			return currentNode.value; 
		}

		set 
		{

		}
	}
}
