﻿using UnityEngine;
using System.Collections;

//[ExecuteInEditMode]
public class EdgeDetectionFilter : MonoBehaviour {

	public Material EdgeDetectionMaterial;
	public Material CompositionMaterial;

	public bool EffectEnabled = true;

	public RenderTexture downscaleSrc;
	public RenderTexture downscaleDest;

	private float downscaleFactor = 1.0f;

	private SetupMRT mrt;

	void Awake ()
	{
		CreateRenderTextures();
	}

	void OnEnable ()
	{
		Camera.main.depthTextureMode = DepthTextureMode.None;
		if (downscaleSrc == null) {
			CreateRenderTextures ();
		}
		EffectEnabled = true;

		mrt = GetComponent<SetupMRT>();
	}

	void CreateRenderTextures ()
	{
		int width = (int)((float)Screen.width * downscaleFactor);
		int height = (int)((float)Screen.height * downscaleFactor);
		downscaleSrc = RenderTexture.GetTemporary(width, height, 32);
		downscaleDest = RenderTexture.GetTemporary(width, height, 32);
	}

	void OnDisable ()
	{
		if (downscaleSrc != null) {
			RenderTexture.ReleaseTemporary(downscaleSrc);
			RenderTexture.ReleaseTemporary(downscaleDest);

			downscaleSrc = null;
			downscaleDest = null;
		}
	}

	private void UpdateRenderTextures ()
	{
		OnDisable ();
		CreateRenderTextures ();
	}

//	void OnGUI ()
//	{
//		if (GUI.Button (new Rect(0f, 0f, 200f, 100f), "1x")) {
//			downscaleFactor = 1.0f;
//			UpdateRenderTextures();
//		}
//		if (GUI.Button (new Rect(0f, 100f, 200f, 100f), "0.75x")) {
//			downscaleFactor = 0.75f;
//			UpdateRenderTextures();
//		}
//		if (GUI.Button (new Rect(0f, 200f, 200f, 100f), "0.66x")) {
//			downscaleFactor = 0.66f;
//			UpdateRenderTextures();
//		}
//		if (GUI.Button (new Rect(0f, 300f, 200f, 100f), "0.5x")) {
//			downscaleFactor = 0.5f;
//			UpdateRenderTextures();
//		}
//		if (GUI.Button (new Rect(0f, 400f, 200f, 100f), "0.33x")) {
//			downscaleFactor = 0.33f;
//			UpdateRenderTextures();
//		}
//		if (GUI.Button (new Rect(0f, 500f, 200f, 100f), "0.25x")) {
//			downscaleFactor = 0.25f;
//			UpdateRenderTextures();
//		}
//
//		if (GUI.Button (new Rect(0f, 600f, 200f, 100f), "Off")) {
//			EffectEnabled = !EffectEnabled;
//		}
//	}

	void SetTextureSize (Material mat)
	{
		mat.SetFloat ("fHeight", Screen.height);
		mat.SetFloat ("fWidth", Screen.width);
		mat.SetFloat ("oneOverHeight", 1.0f / (float)Screen.height);
		mat.SetFloat ("oneOverWidth", 1.0f / (float)Screen.width);
		mat.SetVector ("mask", new Vector4(-1.0f, 1.0f, 1.0f, -1.0f));
		mat.SetVector ("unit", new Vector4(1.0f, 1.0f, 1.0f, 1.0f));
	}

	public void OnRenderImage (RenderTexture src, RenderTexture dest)
	{
		if (EffectEnabled) {
			SetTextureSize (EdgeDetectionMaterial);

			downscaleSrc.DiscardContents();
			downscaleDest.DiscardContents();

			Graphics.Blit (mrt.normalBuffer, downscaleSrc);
			Graphics.Blit (downscaleSrc, downscaleDest, EdgeDetectionMaterial);
			CompositionMaterial.SetTexture ("_Overlay", downscaleDest);
			Graphics.Blit (mrt.colorBuffer, dest, CompositionMaterial);

//			Graphics.Blit (downscaleDest, dest);
		} else {
			Graphics.Blit (mrt.colorBuffer, dest);
		}

//		Graphics.Blit (mrt.colorBuffer, dest);
	}
}
