using UnityEngine;
using System.Collections;

//[ExecuteInEditMode]
public class SetupMRT : MonoBehaviour {

	public RenderTexture normalBuffer;
	public RenderTexture colorBuffer;
	public RenderTexture depthBuffer;
	RenderBuffer[] mrt;
	Camera theCam;

	GameObject shaderCamera;

	void OnDisable () {
		Debug.Log ("OnDisable");
		Graphics.SetRenderTarget(null);
		if (normalBuffer != null) {
			Graphics.SetRenderTarget(null);
			RenderTexture.ReleaseTemporary(normalBuffer);
			normalBuffer = null;
		}

		if (colorBuffer != null) {
			Graphics.SetRenderTarget(null);
			RenderTexture.ReleaseTemporary(colorBuffer);
			colorBuffer = null;
		}

		if (depthBuffer != null) {
			Graphics.SetRenderTarget (null);
			RenderTexture.ReleaseTemporary(depthBuffer);
			depthBuffer = null;
		}

		if (shaderCamera != null) {
			DestroyImmediate(shaderCamera);	
		}
	}

	public bool UsesMRT () {
		return true;
//#if UNITY_EDITOR
//		return true;
//#else
//		return SystemInfo.supportedRenderTargetCount > 1;
//#endif
	}

	private bool isActive = false;

	IEnumerator Start ()
	{
		yield return new WaitForSeconds(0.1f);
		isActive = true;
		yield return null;
		GetComponent<EdgeDetectionFilter>().enabled = true;
	}

	void OnPreRender () {
		if (!enabled || !gameObject.activeInHierarchy || !isActive) {
			return;
		}


		if (theCam == null) {
			theCam = this.GetComponent<Camera>();	
		}

		if (normalBuffer == null) {
			normalBuffer = RenderTexture.GetTemporary (Screen.width, Screen.height, 0);
			normalBuffer.name = "Normal";
			Shader.SetGlobalTexture ("_CameraNormalBuffer", normalBuffer);
		}

		if (colorBuffer == null) {
			colorBuffer = RenderTexture.GetTemporary ((int)(Screen.width * 1f), (int)(Screen.height * 1f), 24);
			colorBuffer.name = "Color";
			Shader.SetGlobalTexture ("_CameraColorBuffer", colorBuffer);
		}

		if (depthBuffer == null) {
			depthBuffer = RenderTexture.GetTemporary(Screen.width, Screen.height, 0);
			depthBuffer.name = "Depth";
			Shader.SetGlobalTexture ("_CameraDepthBuffer", depthBuffer);
		}

		if (mrt == null) 
		{
			mrt = new RenderBuffer[] { colorBuffer.colorBuffer, normalBuffer.colorBuffer, depthBuffer.colorBuffer };
			Shader.SetGlobalMatrix("_CameraViewMatrix", theCam.worldToCameraMatrix);
		}

		if (UsesMRT ()) {
			Graphics.SetRenderTarget (mrt, colorBuffer.depthBuffer);
		} else {
			if (!shaderCamera) {
				shaderCamera = new GameObject("DepthCamera", typeof(Camera));
				shaderCamera.GetComponent<Camera>().enabled = false;
				shaderCamera.hideFlags = HideFlags.DontSave;
			}

			Camera cam = shaderCamera.GetComponent<Camera>();
			cam.CopyFrom (theCam);
			cam.clearFlags = CameraClearFlags.SolidColor;
			cam.targetTexture = colorBuffer;
			cam.depth = -2;
			
			Shader s = Shader.Find ("Hidden/Camera-DepthNormalTexture");
			cam.RenderWithShader (s, "RenderType");

//			Shader.SetGlobalTexture ("_CameraDepthNormalsBuffer", depthNormalsBuffer);
//			Shader.SetGlobalTexture ("_CameraColorBuffer", colorBuffer);
		}
	}

//	public void OnRenderImage (RenderTexture src, RenderTexture dest) 
//	{
////		if(isActive) {
////			Graphics.Blit(colorBuffer, dest);
////		}
//	}
}