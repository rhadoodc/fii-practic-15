﻿using UnityEngine;
using System.Collections;

public class Drift : MonoBehaviour {
	private float FORCE_MULTIPLIER = 100.0f;
	private float TORQUE_MULTIPLIER = 10.0f;

	void FixedUpdate () {
		if (Input.GetKey(KeyCode.W)) {
			GetComponent<Rigidbody>().AddForce(transform.forward * FORCE_MULTIPLIER, ForceMode.Acceleration);
		} else if (Input.GetKey(KeyCode.S)){
			GetComponent<Rigidbody>().AddForce(-transform.forward * FORCE_MULTIPLIER, ForceMode.Acceleration);
		} 

		if (Input.GetKey(KeyCode.A)) {
			GetComponent<Rigidbody>().AddForce(-transform.right * FORCE_MULTIPLIER, ForceMode.Acceleration);
		} else if (Input.GetKey(KeyCode.D)){
			GetComponent<Rigidbody>().AddForce(transform.right * FORCE_MULTIPLIER, ForceMode.Acceleration);
		} 

		if (Input.GetKey(KeyCode.Z)) {
			GetComponent<Rigidbody>().AddForce(Vector3.up * FORCE_MULTIPLIER, ForceMode.Acceleration);
		} else if (Input.GetKey(KeyCode.C)){
			GetComponent<Rigidbody>().AddForce(-Vector3.up * FORCE_MULTIPLIER, ForceMode.Acceleration);
		} 

		if (Input.GetKey(KeyCode.RightArrow)) {
			GetComponent<Rigidbody>().AddTorque(Vector3.up * TORQUE_MULTIPLIER, ForceMode.Acceleration);
		} else if (Input.GetKey(KeyCode.LeftArrow)){
			GetComponent<Rigidbody>().AddTorque(-Vector3.up * TORQUE_MULTIPLIER, ForceMode.Acceleration);
		}

		if (Input.GetKey(KeyCode.DownArrow)) {
			GetComponent<Rigidbody>().AddTorque(-transform.right * TORQUE_MULTIPLIER, ForceMode.Acceleration);
		} else if (Input.GetKey(KeyCode.UpArrow)){
			GetComponent<Rigidbody>().AddTorque(transform.right * TORQUE_MULTIPLIER, ForceMode.Acceleration);
		}
	}
}
