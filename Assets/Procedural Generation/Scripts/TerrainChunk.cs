﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;
using System.Threading;

public partial class TerrainChunk : MonoBehaviour
{
	#region Types
	class IndexCache
	{
		static public byte[] edges = new byte[] { 4, 5, 9 };
		static public byte edgeCount = 3;
	}

	class GridCell
	{
		public Vector3[] p = new Vector3[8];
		public Vector3 voxelCenter;
		public Vector3 position;

		public double[] val = new double[8];

		public byte index;

		public GridCell()
		{

		}

		public GridCell(GridCell other)
		{
			p = other.p.ToArray();

			voxelCenter = other.voxelCenter;
			position = other.position;

			val = other.val.ToArray();

			index = other.index;
		}
	}
	#endregion

	#region Properties
	private int ChunkSizeX
	{
		get { return TerrainChunkManager.ThreadInstance.ChunkSizeX; }
	}
	private int ChunkSizeY
	{
		get { return TerrainChunkManager.ThreadInstance.ChunkSizeY; }
	}
	private int ChunkSizeZ
	{
		get { return TerrainChunkManager.ThreadInstance.ChunkSizeZ; }
	}

	private float ChunkSamplingSizeX
	{
		get { return TerrainChunkManager.ThreadInstance.ChunkSamplingSizeX; }
	}
	private float ChunkSamplingSizeY
	{
		get { return TerrainChunkManager.ThreadInstance.ChunkSamplingSizeY; }
	}
	private float ChunkSamplingSizeZ
	{
		get { return TerrainChunkManager.ThreadInstance.ChunkSamplingSizeZ; }
	}

	private float VoxelRadius
	{
		get { return TerrainChunkManager.ThreadInstance.VoxelRadius; }
	}

	private bool AdaptiveSampling
	{
		get { return TerrainChunkManager.ThreadInstance.AdaptiveSampling; }
	}

	private bool DrawGizmos
	{
		get { return TerrainChunkManager.ThreadInstance.DrawGizmos; }
	}

	private bool SingleSideNormals
	{
		get { return TerrainChunkManager.ThreadInstance.SingleSideNormals; }
	}

	private bool GenerateCollider
	{
		get { return TerrainChunkManager.ThreadInstance.GenerateCollider; }
	}

	private float SamplingOffsetX
	{
		get { return cachedPosition.x / ChunkSizeX * ChunkSamplingSizeX; }
	}

	private float SamplingOffsetY
	{
		get { return cachedPosition.y / ChunkSizeY * ChunkSamplingSizeY; }
	}

	private float SamplingOffsetZ
	{
		get { return cachedPosition.z / ChunkSizeZ * ChunkSamplingSizeZ; }
	}
	#endregion

	public bool AnalysisRequired = true;

	public MeshFilter meshFilter;
	public MeshRenderer meshRenderer;
	public MeshCollider meshCollider;

	private float scaledSamplingX;
	private float scaledSamplingY;
	private float scaledSamplingZ;
	
	Transform cachedTransform;
	Vector3 cachedPosition;

	/*
	private TerrainGenerator Generator
	{ 
		get { return TerrainChunkManager.Instance.Generator; }
	}
	*/
	private TerrainGenerator Generator;

	/*GenerateTerrainMesh scope variables*/
	private Vector3 currentVertexNormal = new Vector3();

	private GridCell currentCell = new GridCell();

	private Vector3 currentVertexInterpolated;

	private const float stride = 1f;

	private int indexTargetCounter = 0;
	private int vertexTargetCounter = 0;
	private int triangleIndex = 0;

	private Dictionary<int, GridCell> cellsToSampleList = new Dictionary<int, GridCell>();
	private GridCell[] cellsToSample;
	/**/

	/*FillIndices variables*/
	private int[] address;
	private int strideX;
	private int strideY;
	/**/

	/*AnalyzeTerrainMesh variables*/
	private Vector3[] vertexTarget = null;
	private Vector3[] normalsTarget = null;
	private int[] indexTarget = null;
	private int[] indexMatrix = null;
	/**/

	float[] userMods = null;

	#region Samplers
	public double GetUserModification(double whereX, double whereY, double whereZ)
	{
		if (userMods == null)
			return 0d;



		return 0d;
	}

	public double GetScaledSimplexNoise(double whereX, double whereY, double whereZ/*, double premulX = 1.0f, double premulY = 1.0f, double premulZ = 1.0f, double preaddX = 0.0f, double preaddY = 0.0f, double preaddZ = 0.0f, double postmul = 1.0f, double postadd = 0.0f*/)
	{
		/*
		//sphere
		centerX = (SizeX - 1.0f) * 0.5f;
		centerY = (SizeX - 1.0f) * 0.5f;
		centerZ = (SizeX - 1.0f) * 0.5f;

		radius = SizeX * 0.5f - 1;
		radius *= radius;

		var distance = (whereX - centerX) * (whereX - centerX) + (whereY - centerY) * (whereY - centerY) + (whereZ - centerZ) * (whereZ - centerZ);

		return radius - distance;
		*/

		/*
		//ellipsoid		
		centerX = (SizeX - 1.0f) * 0.5f;
		centerY = (SizeY - 1.0f) * 0.5f;
		centerZ = (SizeZ - 1.0f) * 0.5f;

		var radiusX = SizeX * 0.5f - 1.0f;
		var radiusY = SizeY * 0.5f - 1.0f;
		var radiusZ = SizeZ * 0.5f - 1.0f;

		return - ((whereX - centerX) * (whereX - centerX) / (radiusX * radiusX) + (whereY - centerY) * (whereY - centerY) / (radiusY * radiusY) + (whereZ - centerZ) * (whereZ - centerZ) / (radiusZ * radiusZ)) + 1.0f;
		*/

		//return (float)SimplexNoise.noise(whereX / SizeX * samplingSizeX + samplingOffsetX, whereY / SizeY * samplingSizeY + samplingOffsetY, whereZ / SizeZ * samplingSizeZ + samplingOffsetZ);
		//return SimplexNoise.noise(whereX * premulX + preaddX, whereY * premulY + preaddY, whereZ * premulZ + preaddZ) * postmul + postadd;
		return SimplexNoise.noise(whereX, whereY, whereZ);
	}

	public double GetGroundSample(double whereX, double whereY, double whereZ/*, double premulX = 1.0f, double premulY = 1.0f, double premulZ = 1.0f, double preaddX = 0.0f, double preaddY = 0.0f, double preaddZ = 0.0f, double postmul = 1.0f, double postadd = 0.0f*/)
	{
		//return (whereY * premulY + preaddY) * postmul + postadd;
		return (whereY);
	}
	#endregion

	#region Helpers
	public void InitializeComponents(MeshFilter filter, MeshRenderer renderer, MeshCollider collider)
	{
		meshCollider = collider;
		meshFilter = filter;
		meshRenderer = renderer;
	}

	Vector3 VertexInterp(Vector3 p1, Vector3 p2, double valp1, double valp2, bool adaptive = true)
	{
		if (adaptive)
		{
			if (valp1 * valp1 < 1e-6f)
				return p1;
			if (valp2 * valp2 < 1e-6f)
				return p2;

			var diff = valp1 - valp2;

			if (diff * diff < 1e-6f)
				return p1;

			//diff is actually a sign preserving sum (preserves the sign of valp1)
			double factor = (valp1) / (diff);

			return Vector3.Lerp(p1, p2, (float)factor);
		}
		else
		{
			if (valp1 * valp1 < 1e-6f)
				return (p1);

			if (valp2 * valp2 < 1e-6f)
				return (p2);

			return Vector3.Lerp(p1, p2, 0.5f);
		}

	}

	void FillIndices()
	{
		//Profiler.BeginSample("FillIndices");
		for (triangleIndex = 0; triTable[currentCell.index, triangleIndex] != -1; ++triangleIndex)
		{
			address = indexAddresses[triTable[currentCell.index, triangleIndex]];
			var target = Mathf.FloorToInt((currentCell.position.y - address[1]) * strideY + (currentCell.position.x - address[0]) * strideX + currentCell.position.z - address[2]) * IndexCache.edgeCount + address[3];

			if ((currentCell.position.x < 1f)
			|| (currentCell.position.y < 1f)
			|| (currentCell.position.z < 1f))
				continue;

			indexTarget[indexTargetCounter] = indexMatrix[target];
			++indexTargetCounter;
		}
		//Profiler.EndSample();
	}

	static readonly int[][] sampleAddresses = new int[][]
	{
		new int[] {1, 1, 0}, //0
		new int[] {0, 1, 0}, //1
		new int[] {0, 1, 1}, //2
		new int[] {1, 1, 1}, //3

		new int[] {1, 0, 0}, //4
		new int[] {0, 0, 0}, //5
		new int[] {0, 0, 1}, //6
		new int[] {1, 0, 1}, //7
	};

	bool SampleVolume(ref float voxelPositionX, ref float voxelPositionY, ref float voxelPositionZ, float resolution, ref GridCell cell)
	{
		cell.position.x = voxelPositionX;
		cell.position.y = voxelPositionY;
		cell.position.z = voxelPositionZ;

		cell.voxelCenter = cell.position * resolution * 2f;
		
		var li = cell.voxelCenter.x - resolution;
		var lj = cell.voxelCenter.y - resolution;
		var lk = cell.voxelCenter.z - resolution;
		var hi = cell.voxelCenter.x + resolution;
		var hj = cell.voxelCenter.y + resolution;
		var hk = cell.voxelCenter.z + resolution;
		
		//Profiler.BeginSample("SampleVolume: get samples");

		//double testVal = 0.0d;
		
		//cell.val[0] = Generator.GetSample(li, lj, hk);
		//testVal = Generator.GetSample(li, lj, hk);
		address = sampleAddresses[0];
		int target = Mathf.FloorToInt((cell.position.y - address[1]) * strideY + (cell.position.x - address[0]) * strideX + cell.position.z - address[2]);
		
		//if ((voxelPositionX > 0) && (voxelPositionY > 0) && (voxelPositionZ > 0) && (!cellsToSampleList.ContainsKey(target)))
		//{
		//	Debug.LogError("No key " + target.ToString());
		//}

		cell.val[0] = (voxelPositionX > 0) && (voxelPositionY > 0) && (voxelPositionZ > 0) && (cellsToSampleList.ContainsKey(target)) ? cellsToSampleList[target].val[5] : Generator.GetSample(li, lj, hk);

		//if ((testVal != cell.val[0]) && (voxelPositionX > 0) && (voxelPositionY > 0) && (voxelPositionZ > 0))
		//	Debug.LogError(String.Format("diff {0} {1} {2} - testval {3} target {4} sample {5}", cell.position.x, cell.position.y, cell.position.z, testVal, target, cell.val[0]));
		
		//cell.val[1] = Generator.GetSample(hi, lj, hk);
		//testVal = Generator.GetSample(hi, lj, hk);
		address = sampleAddresses[1];
		target = Mathf.FloorToInt((cell.position.y - address[1]) * strideY + (cell.position.x - address[0]) * strideX + cell.position.z - address[2]);

		//if ((voxelPositionX > 0) && (voxelPositionY > 0) && (voxelPositionZ > 0) && (!cellsToSampleList.ContainsKey(target)))
		//{
		//	Debug.LogError("No key " + target.ToString());
		//}

		cell.val[1] = (voxelPositionX > 0) && (voxelPositionY > 0) && (voxelPositionZ > 0) && (cellsToSampleList.ContainsKey(target)) ? cellsToSampleList[target].val[5] : Generator.GetSample(hi, lj, hk);

		//if ((testVal != cell.val[1]) && (voxelPositionX > 0) && (voxelPositionY > 0) && (voxelPositionZ > 0))
		//	Debug.LogError(String.Format("diff {0} {1} {2} - testval {3} target {4} sample {5}", cell.position.x, cell.position.y, cell.position.z, testVal, target, cell.val[1]));

		//cell.val[2] = Generator.GetSample(hi, lj, lk);
		//testVal = Generator.GetSample(hi, lj, lk);
		address = sampleAddresses[2];
		target = Mathf.FloorToInt((cell.position.y - address[1]) * strideY + (cell.position.x - address[0]) * strideX + cell.position.z - address[2]);

		//if ((voxelPositionX > 0) && (voxelPositionY > 0) && (voxelPositionZ > 0) && (!cellsToSampleList.ContainsKey(target)))
		//{
		//	Debug.LogError("No key " + target.ToString());
		//}

		cell.val[2] = (voxelPositionX > 0) && (voxelPositionY > 0) && (voxelPositionZ > 0) && (cellsToSampleList.ContainsKey(target)) ? cellsToSampleList[target].val[5] : Generator.GetSample(hi, lj, lk);

		//if ((testVal != cell.val[2]) && (voxelPositionX > 0) && (voxelPositionY > 0) && (voxelPositionZ > 0))
		//	Debug.LogError(String.Format("diff {0} {1} {2} - testval {3} target {4} sample {5}", cell.position.x, cell.position.y, cell.position.z, testVal, target, cell.val[2]));

		//cell.val[3] = Generator.GetSample(li, lj, lk);
		//testVal = Generator.GetSample(li, lj, lk);
		address = sampleAddresses[3];
		target = Mathf.FloorToInt((cell.position.y - address[1]) * strideY + (cell.position.x - address[0]) * strideX + cell.position.z - address[2]);

		//if ((voxelPositionX > 0) && (voxelPositionY > 0) && (voxelPositionZ > 0) && (!cellsToSampleList.ContainsKey(target)))
		//{
		//	Debug.LogError("No key " + target.ToString());
		//}

		cell.val[3] = (voxelPositionX > 0) && (voxelPositionY > 0) && (voxelPositionZ > 0) && (cellsToSampleList.ContainsKey(target)) ? cellsToSampleList[target].val[5] : Generator.GetSample(li, lj, lk);

		//if ((testVal != cell.val[3]) && (voxelPositionX > 0) && (voxelPositionY > 0) && (voxelPositionZ > 0))
		//	Debug.LogError(String.Format("diff {0} {1} {2} - testval {3} target {4} sample {5}", cell.position.x, cell.position.y, cell.position.z, testVal, target, cell.val[3]));

		//cell.val[4] = Generator.GetSample(li, hj, hk);
		//testVal = Generator.GetSample(li, hj, hk);
		address = sampleAddresses[4];
		target = Mathf.FloorToInt((cell.position.y - address[1]) * strideY + (cell.position.x - address[0]) * strideX + cell.position.z - address[2]);

		//if ((voxelPositionX > 0) && (voxelPositionY > 0) && (voxelPositionZ > 0) && (!cellsToSampleList.ContainsKey(target)))
		//{
		//	Debug.LogError("No key " + target.ToString());
		//}

		cell.val[4] = (voxelPositionX > 0) && (voxelPositionY > 0) && (voxelPositionZ > 0) && (cellsToSampleList.ContainsKey(target)) ? cellsToSampleList[target].val[5] : Generator.GetSample(li, hj, hk);

		//if ((testVal != cell.val[4]) && (voxelPositionX > 0) && (voxelPositionY > 0) && (voxelPositionZ > 0))
		//	Debug.LogError(String.Format("diff {0} {1} {2} - testval {3} target {4} sample {5}", cell.position.x, cell.position.y, cell.position.z, testVal, target, cell.val[4]));

		cell.val[5] = Generator.GetSample(hi, hj, hk);
		
		//cell.val[6] = Generator.GetSample(hi, hj, lk);
		//testVal = Generator.GetSample(hi, hj, lk);
		address = sampleAddresses[6];
		target = Mathf.FloorToInt((cell.position.y - address[1]) * strideY + (cell.position.x - address[0]) * strideX + cell.position.z - address[2]);

		//if ((voxelPositionX > 0) && (voxelPositionY > 0) && (voxelPositionZ > 0) && (!cellsToSampleList.ContainsKey(target)))
		//{
		//	Debug.LogError("No key " + target.ToString());
		//}

		cell.val[6] = (voxelPositionX > 0) && (voxelPositionY > 0) && (voxelPositionZ > 0) && (cellsToSampleList.ContainsKey(target)) ? cellsToSampleList[target].val[5] : Generator.GetSample(hi, hj, lk);

		//if ((testVal != cell.val[6]) && (voxelPositionX > 0) && (voxelPositionY > 0) && (voxelPositionZ > 0))
		//	Debug.LogError(String.Format("diff {0} {1} {2} - testval {3} target {4} sample {5}", cell.position.x, cell.position.y, cell.position.z, testVal, target, cell.val[6]));

		//cell.val[7] = Generator.GetSample(li, hj, lk);
		//testVal = Generator.GetSample(li, hj, lk);
		address = sampleAddresses[7];
		target = Mathf.FloorToInt((cell.position.y - address[1]) * strideY + (cell.position.x - address[0]) * strideX + cell.position.z - address[2]);

		//if ((voxelPositionX > 0) && (voxelPositionY > 0) && (voxelPositionZ > 0) && (!cellsToSampleList.ContainsKey(target)))
		//{
		//	Debug.LogError("No key " + target.ToString());
		//}

		cell.val[7] = (voxelPositionX > 0) && (voxelPositionY > 0) && (voxelPositionZ > 0) && (cellsToSampleList.ContainsKey(target)) ? cellsToSampleList[target].val[5] : Generator.GetSample(li, hj, lk);

		//if ((testVal != cell.val[7]) && (voxelPositionX > 0) && (voxelPositionY > 0) && (voxelPositionZ > 0))
		//	Debug.LogError(String.Format("diff {0} {1} {2} - testval {3} target {4} sample {5}", cell.position.x, cell.position.y, cell.position.z, testVal, target, cell.val[7]));

		//Profiler.EndSample();

		cell.index = 0;
		if (cell.val[0] <= 0.0f) cell.index |= 1;
		if (cell.val[1] <= 0.0f) cell.index |= 2;
		if (cell.val[2] <= 0.0f) cell.index |= 4;
		if (cell.val[3] <= 0.0f) cell.index |= 8;
		if (cell.val[4] <= 0.0f) cell.index |= 16;
		if (cell.val[5] <= 0.0f) cell.index |= 32;
		if (cell.val[6] <= 0.0f) cell.index |= 64;
		if (cell.val[7] <= 0.0f) cell.index |= 128;

		if ((cell.index == 0) || (cell.index == 255))
			return false;
		
		cell.p[0].x = li;
		cell.p[0].y = lj;
		cell.p[0].z = hk;

		cell.p[1].x = hi;
		cell.p[1].y = lj;
		cell.p[1].z = hk;

		cell.p[2].x = hi;
		cell.p[2].y = lj;
		cell.p[2].z = lk;

		cell.p[3].x = li;
		cell.p[3].y = lj;
		cell.p[3].z = lk;

		cell.p[4].x = li;
		cell.p[4].y = hj;
		cell.p[4].z = hk;

		cell.p[5].x = hi;
		cell.p[5].y = hj;
		cell.p[5].z = hk;

		cell.p[6].x = hi;
		cell.p[6].y = hj;
		cell.p[6].z = lk;

		cell.p[7].x = li;
		cell.p[7].y = hj;
		cell.p[7].z = lk;

		return true;
	}
	#endregion

	void GenerateTerrainMesh()
	{
		indexTargetCounter = 0;
		vertexTargetCounter = 0;

		//Profiler.BeginSample("GenerateTerrainMesh: voxel loop");		
		for (var j = 0; j < cellsToSample.Length; ++j)
		{
			currentCell = cellsToSample[j];

			var currentEdgeIndex = edgeTable[currentCell.index];

			//Profiler.BeginSample("GenerateTerrainMesh: vertex generation");
			for (var i = 0; i < IndexCache.edgeCount; ++i)
			{
				var index = IndexCache.edges[i];

				if ((currentEdgeIndex & bitMasks[index]) != 0)
				{
					//Profiler.BeginSample("GenerateTerrainMesh: vertex interpolation");
					currentVertexInterpolated = VertexInterp(currentCell.p[edgeMasks[index][0]], currentCell.p[edgeMasks[index][1]], currentCell.val[edgeMasks[index][0]], currentCell.val[edgeMasks[index][1]], AdaptiveSampling);
					//Profiler.EndSample();
					
					//Profiler.BeginSample("GenerateTerrainMesh: edge init");
					vertexTarget[vertexTargetCounter] = currentVertexInterpolated;
					//Profiler.EndSample();

					//Profiler.BeginSample("GenerateTerrainMesh: normal generation");
					if (SingleSideNormals)
					{
						var currentVoxelCenterSample = Generator.GetSample(currentVertexInterpolated.x, currentVertexInterpolated.y, currentVertexInterpolated.z);

						currentVertexNormal.x = (float)(Generator.GetSample(currentVertexInterpolated.x + VoxelRadius, currentVertexInterpolated.y, currentVertexInterpolated.z)
							- currentVoxelCenterSample);

						currentVertexNormal.y = (float)(Generator.GetSample(currentVertexInterpolated.x, currentVertexInterpolated.y + VoxelRadius, currentVertexInterpolated.z)
							- currentVoxelCenterSample);

						currentVertexNormal.z = (float)(Generator.GetSample(currentVertexInterpolated.x, currentVertexInterpolated.y, currentVertexInterpolated.z + VoxelRadius)
							- currentVoxelCenterSample);
					}
					else
					{
						currentVertexNormal.x = (float)(Generator.GetSample(currentVertexInterpolated.x + VoxelRadius, currentVertexInterpolated.y, currentVertexInterpolated.z)
						- Generator.GetSample(currentVertexInterpolated.x - VoxelRadius, currentVertexInterpolated.y, currentVertexInterpolated.z));

						currentVertexNormal.y = (float)(Generator.GetSample(currentVertexInterpolated.x, currentVertexInterpolated.y + VoxelRadius, currentVertexInterpolated.z)
							- Generator.GetSample(currentVertexInterpolated.x, currentVertexInterpolated.y - VoxelRadius, currentVertexInterpolated.z));

						currentVertexNormal.z = (float)(Generator.GetSample(currentVertexInterpolated.x, currentVertexInterpolated.y, currentVertexInterpolated.z + VoxelRadius)
							- Generator.GetSample(currentVertexInterpolated.x, currentVertexInterpolated.y, currentVertexInterpolated.z - VoxelRadius));
					}
					//Profiler.EndSample();

					//Profiler.BeginSample("GenerateTerrainMesh: normal assignment");
					normalsTarget[vertexTargetCounter] = -currentVertexNormal.normalized;

					var target = Mathf.FloorToInt(currentCell.position.y * strideY + currentCell.position.x * strideX + currentCell.position.z) * IndexCache.edgeCount + i;

					//splat
					indexMatrix[target] = vertexTargetCounter;
					
					++vertexTargetCounter;
					//Profiler.EndSample();
				}
			}
			//Profiler.EndSample();

			FillIndices();
		}
		//Profiler.EndSample();
	}

	bool ready = false;

	void AssignVerticesToMesh()
	{
		//Profiler.BeginSample("GenerateTerrainMesh: mesh assignation");
		meshFilter.mesh.Clear();

		meshFilter.mesh.vertices = vertexTarget;
		meshFilter.mesh.normals = normalsTarget;
		meshFilter.mesh.triangles = indexTarget;

		meshFilter.mesh.RecalculateBounds();
		//Profiler.EndSample();

		//Profiler.BeginSample("GenerateTerrainMesh: mesh collider assignation");
		if (GenerateCollider && meshCollider != null)
		{
			meshCollider.sharedMesh = null;
			meshCollider.sharedMesh = meshFilter.mesh;
		}
		//Profiler.EndSample();
	}

	void AnalyzeTerrainMesh(ref Vector3[] vertexArray, ref Vector3[] normalsArray, ref int[] indexArray)
	{
		//Profiler.BeginSample("AnalyzeTerrainMesh: frame init");
		strideX = (ChunkSizeZ + 1);
		strideY = strideX * (ChunkSizeX + 1);

		int indexCount = 0;
		int vertexCount = 0;

		scaledSamplingX = ChunkSamplingSizeX / ChunkSizeX;
		scaledSamplingY = ChunkSamplingSizeY / ChunkSizeY;
		scaledSamplingZ = ChunkSamplingSizeZ / ChunkSizeZ;

		Generator.ClearGeneratorStack();

		Generator.AddGenerator(GetGroundSample, preaddY: cachedPosition.y - 2.0d, postmul: -1.0d);
		//return -(whereY - 2.0f);
		//Generator.AddGenerator(GetScaledSimplexNoise, premulX: scaledSamplingX, preaddX: SamplingOffsetX, premulY: scaledSamplingY, preaddY: SamplingOffsetY, premulZ: scaledSamplingZ, preaddZ: SamplingOffsetZ);
		
		//Generator.AddGenerator(GetScaledSimplexNoise, premulX: scaledSamplingX, preaddX: SamplingOffsetX, premulY: scaledSamplingY, preaddY: SamplingOffsetY, premulZ: scaledSamplingZ, preaddZ: SamplingOffsetZ, premulUniform: 0.47d, postmul: 32.0d);
		
		//Generator.AddGenerator(GetScaledSimplexNoise, premulX: scaledSamplingX, preaddX: SamplingOffsetX, premulY: scaledSamplingY, preaddY: SamplingOffsetY, premulZ: scaledSamplingZ, preaddZ: SamplingOffsetZ, premulUniform: 1.01d, postmul: 16.0d);
		Generator.AddGenerator(GetScaledSimplexNoise, premulX: scaledSamplingX, preaddX: SamplingOffsetX, premulY: scaledSamplingY, preaddY: SamplingOffsetY, premulZ: scaledSamplingZ, preaddZ: SamplingOffsetZ, premulUniform: 1.96d, postmul: 32.0d);


		//Generator.AddGenerator(GetScaledSimplexNoise, premulX: scaledSamplingX, preaddX: SamplingOffsetX, premulY: scaledSamplingY, preaddY: SamplingOffsetY, premulZ: scaledSamplingZ, preaddZ: SamplingOffsetZ, premulUniform: 4.03d, postmul: 4.0d);
		//Generator.AddGenerator(GetScaledSimplexNoise, premulX: scaledSamplingX, preaddX: SamplingOffsetX, premulY: scaledSamplingY, preaddY: SamplingOffsetY, premulZ: scaledSamplingZ, preaddZ: SamplingOffsetZ, premulUniform: 7.835d, postmul: 2.0d);

		Generator.AddGenerator(GetScaledSimplexNoise, premulX: scaledSamplingX, preaddX: SamplingOffsetX, premulY: scaledSamplingY, preaddY: SamplingOffsetY, premulZ: scaledSamplingZ, preaddZ: SamplingOffsetZ, premulUniform: 16.013d, postmul: 1.0d);
		Generator.AddGenerator(GetScaledSimplexNoise, premulX: scaledSamplingX, preaddX: SamplingOffsetX, premulY: scaledSamplingY, preaddY: SamplingOffsetY, premulZ: scaledSamplingZ, preaddZ: SamplingOffsetZ, premulUniform: 31.767d, postmul: 0.5d);
		
		//return SimplexNoise.noise(whereX * scaledSamplingX + SamplingOffsetX, whereY * scaledSamplingY + SamplingOffsetY, whereZ * scaledSamplingZ + SamplingOffsetZ);

		cellsToSampleList.Clear();
		//Profiler.EndSample();
		
		//Profiler.BeginSample("AnalyzeTerrainMesh: voxel loop");		
		for (var fj = 0.0f; fj <= ChunkSizeY; fj += stride)
		for (var fi = 0.0f; fi <= ChunkSizeX; fi += stride)
		for (var fk = 0.0f; fk <= ChunkSizeZ; fk += stride)
		{			
			if (!SampleVolume(ref fi, ref fj, ref fk, VoxelRadius, ref currentCell))
				continue;

			var target = Mathf.FloorToInt(fj * strideY + fi * strideX + fk);

			cellsToSampleList.Add(target, 
			new GridCell(currentCell));

			var currentEdgeIndex = edgeTable[currentCell.index];

			indexCount += triTableOpt[currentCell.index].Length;

			//Profiler.BeginSample("AnalyzeTerrainMesh: vertex count");
			for (var i = 0; i < IndexCache.edgeCount; ++i)
			{
				if ((currentEdgeIndex & bitMasks[IndexCache.edges[i]]) != 0)
				{
					++vertexCount;
				}
			}
			//Profiler.EndSample();
		}
		//Profiler.EndSample();

		//Profiler.BeginSample("AnalyzeTerrainMesh: return allocation");
		if ((vertexArray == null) || (vertexCount != vertexArray.Length))
		{
			vertexArray = new Vector3[vertexCount];
		}

		if ((normalsArray == null) || (vertexCount != normalsArray.Length))
		{
			normalsArray = new Vector3[vertexCount];
		}

		if ((indexArray == null) || (indexCount != indexArray.Length))
		{
			indexArray = new int[indexCount];
		}

		if ((indexMatrix == null) || ((ChunkSizeY + 1) * strideY * IndexCache.edgeCount != indexArray.Length))
		{
			indexMatrix = new int[(ChunkSizeY + 1) * strideY * IndexCache.edgeCount];
			//indexMatrix = Enumerable.Repeat(-1, (sizeY + 1) * strideY * IndexCache.edgeCount).ToArray();
		}

		cellsToSample = cellsToSampleList.Values.ToArray();
		//Profiler.EndSample();
	}

	#region Editor
	void OnDrawGizmos()
	{
		var color = Gizmos.color;

		if (Selection.Contains(gameObject))
		{
			Gizmos.color = Color.cyan;
		}

		Gizmos.DrawWireCube(new Vector3(ChunkSizeX * VoxelRadius, ChunkSizeY * VoxelRadius, ChunkSizeZ * VoxelRadius) + transform.position, new Vector3(ChunkSizeX * VoxelRadius * 2f, ChunkSizeY * VoxelRadius * 2f, ChunkSizeZ * VoxelRadius * 2f));

		if (!DrawGizmos)
		{
			Gizmos.color = color;
			return;
		}

		var voxelStride = new Vector3(2f * VoxelRadius, 2f * VoxelRadius, 2f * VoxelRadius);
		var thinStride = voxelStride;
		thinStride.y = 0.05f;

		for (float fj = 0f; fj < ChunkSizeY; fj += stride)
		for (float fi = 0f; fi < ChunkSizeX; fi += stride)
		for (float fk = 0f; fk < ChunkSizeZ; fk += stride)
		{
			var isoLevel = Generator.GetSample(fi, fj, fk);

			if (isoLevel > 0.0f)
			{
				Gizmos.color = Color.black;
				Gizmos.DrawWireCube(new Vector3(fi * 2f * VoxelRadius, fj * 2f * VoxelRadius, fk * 2f * VoxelRadius) + transform.position, voxelStride);
				Gizmos.color = Color.red;
				Gizmos.DrawWireCube(new Vector3(fi * 2f * VoxelRadius, fj * 2f * VoxelRadius - VoxelRadius + (float)isoLevel, fk * 2f * VoxelRadius) + transform.position, thinStride);
			}
		}

		Gizmos.color = color;
	}
	#endregion

	#region MonoBehavior
	void Start()
	{
		Mesh mesh = new Mesh();
		
		if (meshFilter != null)
			meshFilter.mesh = mesh;
		else
			Debug.LogError("MarchingCubes::Unasigned MeshFilter!");

		SimplexNoise.Initialize();

		Generator = new TerrainGenerator();
	}
	
	void Update () 
	{
		if (AnalysisRequired)
		{
			AnalysisRequired = false;

			TerrainChunkManager.Instance.Nudge ();
			cachedTransform = transform;
			cachedPosition = transform.position;
			
			Generate();
		}

		if (generating && ready)
		{
			generating = false;
			ready = false;
			AssignVerticesToMesh();
		}
	}

	bool generating = false;

	//List<string> issues = new List<string>();

	public void Generate()
	{
		generating = true;

		ThreadPool.QueueUserWorkItem((context) =>
		{
			//try
			//{
				AnalyzeTerrainMesh(ref vertexTarget, ref normalsTarget, ref indexTarget);
				
				GenerateTerrainMesh();
	
				ready = true;
			//}
			//catch (Exception ex)
			//{
			//	var str = ex.ToString();
				
			//	issues.Add (str);
			//}
		});
	}
	#endregion	
	
	#region LookUpTables
	private readonly short[] bitMasks = new short[] { 1, 2, 4, 8, 16, 32, 64, 128, 256, 512, 1024, 2048 };
	/*
	private readonly Vector3[] vertices = new Vector3[]
	{
		//bottom face
		new Vector3(-1.0f, -1.0f, 1.0f),
		new Vector3(1.0f, -1.0f, 1.0f),
		new Vector3(1.0f, -1.0f, -1.0f),
		new Vector3(-1.0f, -1.0f, -1.0f),
		

		//top face
		new Vector3(-1.0f, 1.0f, 1.0f),
		new Vector3(1.0f, 1.0f, 1.0f),
		new Vector3(1.0f, 1.0f, -1.0f),
		new Vector3(-1.0f, 1.0f, -1.0f),		
	};
	*/
	private readonly byte[][] edgeMasks = new byte[12][]
	{
		new byte[2] { 0, 1 },
		new byte[2] { 1, 2 },
		new byte[2] { 2, 3 },
		new byte[2] { 3, 0 },

		new byte[2] { 4, 5 },
		new byte[2] { 5, 6 },
		new byte[2] { 6, 7 },
		new byte[2] { 7, 4 },

		new byte[2] { 0, 4 },
		new byte[2] { 1, 5 },
		new byte[2] { 2, 6 },
		new byte[2] { 3, 7 },
	};

	static readonly int[][] indexAddresses = new int[][]
	{
		new int[] {0, 1, 0, 0}, //0
		new int[] {0, 1, 0, 1}, //1
		new int[] {0, 1, 1, 0}, //2
		new int[] {1, 1, 0, 1}, //3

		new int[] {0, 0, 0, 0}, //4
		new int[] {0, 0, 0, 1}, //5
		new int[] {0, 0, 1, 0}, //6
		new int[] {1, 0, 0, 1}, //7

		new int[] {1, 0, 0, 2}, //8
		new int[] {0, 0, 0, 2}, //9
		new int[] {0, 0, 1, 2}, //10
		new int[] {1, 0, 1, 2}, //11
	};

	private static readonly short[] edgeTable = new short[256] {
	0x0  , 0x109, 0x203, 0x30a, 0x406, 0x50f, 0x605, 0x70c,
	0x80c, 0x905, 0xa0f, 0xb06, 0xc0a, 0xd03, 0xe09, 0xf00,
	0x190, 0x99 , 0x393, 0x29a, 0x596, 0x49f, 0x795, 0x69c,
	0x99c, 0x895, 0xb9f, 0xa96, 0xd9a, 0xc93, 0xf99, 0xe90,
	0x230, 0x339, 0x33 , 0x13a, 0x636, 0x73f, 0x435, 0x53c,
	0xa3c, 0xb35, 0x83f, 0x936, 0xe3a, 0xf33, 0xc39, 0xd30,
	0x3a0, 0x2a9, 0x1a3, 0xaa , 0x7a6, 0x6af, 0x5a5, 0x4ac,
	0xbac, 0xaa5, 0x9af, 0x8a6, 0xfaa, 0xea3, 0xda9, 0xca0,
	0x460, 0x569, 0x663, 0x76a, 0x66 , 0x16f, 0x265, 0x36c,
	0xc6c, 0xd65, 0xe6f, 0xf66, 0x86a, 0x963, 0xa69, 0xb60,
	0x5f0, 0x4f9, 0x7f3, 0x6fa, 0x1f6, 0xff , 0x3f5, 0x2fc,
	0xdfc, 0xcf5, 0xfff, 0xef6, 0x9fa, 0x8f3, 0xbf9, 0xaf0,
	0x650, 0x759, 0x453, 0x55a, 0x256, 0x35f, 0x55 , 0x15c,
	0xe5c, 0xf55, 0xc5f, 0xd56, 0xa5a, 0xb53, 0x859, 0x950,
	0x7c0, 0x6c9, 0x5c3, 0x4ca, 0x3c6, 0x2cf, 0x1c5, 0xcc ,
	0xfcc, 0xec5, 0xdcf, 0xcc6, 0xbca, 0xac3, 0x9c9, 0x8c0,
	0x8c0, 0x9c9, 0xac3, 0xbca, 0xcc6, 0xdcf, 0xec5, 0xfcc,
	0xcc , 0x1c5, 0x2cf, 0x3c6, 0x4ca, 0x5c3, 0x6c9, 0x7c0,
	0x950, 0x859, 0xb53, 0xa5a, 0xd56, 0xc5f, 0xf55, 0xe5c,
	0x15c, 0x55 , 0x35f, 0x256, 0x55a, 0x453, 0x759, 0x650,
	0xaf0, 0xbf9, 0x8f3, 0x9fa, 0xef6, 0xfff, 0xcf5, 0xdfc,
	0x2fc, 0x3f5, 0xff , 0x1f6, 0x6fa, 0x7f3, 0x4f9, 0x5f0,
	0xb60, 0xa69, 0x963, 0x86a, 0xf66, 0xe6f, 0xd65, 0xc6c,
	0x36c, 0x265, 0x16f, 0x66 , 0x76a, 0x663, 0x569, 0x460,
	0xca0, 0xda9, 0xea3, 0xfaa, 0x8a6, 0x9af, 0xaa5, 0xbac,
	0x4ac, 0x5a5, 0x6af, 0x7a6, 0xaa , 0x1a3, 0x2a9, 0x3a0,
	0xd30, 0xc39, 0xf33, 0xe3a, 0x936, 0x83f, 0xb35, 0xa3c,
	0x53c, 0x435, 0x73f, 0x636, 0x13a, 0x33 , 0x339, 0x230,
	0xe90, 0xf99, 0xc93, 0xd9a, 0xa96, 0xb9f, 0x895, 0x99c,
	0x69c, 0x795, 0x49f, 0x596, 0x29a, 0x393, 0x99 , 0x190,
	0xf00, 0xe09, 0xd03, 0xc0a, 0xb06, 0xa0f, 0x905, 0x80c,
	0x70c, 0x605, 0x50f, 0x406, 0x30a, 0x203, 0x109, 0x0   };
	
	private static readonly int[,] triTable = new int[,]
	{{-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{0, 8, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{0, 1, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{1, 8, 3, 9, 8, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{1, 2, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{0, 8, 3, 1, 2, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{9, 2, 10, 0, 2, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{2, 8, 3, 2, 10, 8, 10, 9, 8, -1, -1, -1, -1, -1, -1, -1},
	{3, 11, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{0, 11, 2, 8, 11, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1}, 
	{1, 9, 0, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1}, //10
	{1, 11, 2, 1, 9, 11, 9, 8, 11, -1, -1, -1, -1, -1, -1, -1},
	{3, 10, 1, 11, 10, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{0, 10, 1, 0, 8, 10, 8, 11, 10, -1, -1, -1, -1, -1, -1, -1},
	{3, 9, 0, 3, 11, 9, 11, 10, 9, -1, -1, -1, -1, -1, -1, -1},
	{9, 8, 10, 10, 8, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{4, 7, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{4, 3, 0, 7, 3, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{0, 1, 9, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{4, 1, 9, 4, 7, 1, 7, 3, 1, -1, -1, -1, -1, -1, -1, -1},
	{1, 2, 10, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1}, //20
	{3, 4, 7, 3, 0, 4, 1, 2, 10, -1, -1, -1, -1, -1, -1, -1},
	{9, 2, 10, 9, 0, 2, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1},
	{2, 10, 9, 2, 9, 7, 2, 7, 3, 7, 9, 4, -1, -1, -1, -1},
	{8, 4, 7, 3, 11, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{11, 4, 7, 11, 2, 4, 2, 0, 4, -1, -1, -1, -1, -1, -1, -1},
	{9, 0, 1, 8, 4, 7, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1},
	{4, 7, 11, 9, 4, 11, 9, 11, 2, 9, 2, 1, -1, -1, -1, -1},
	{3, 10, 1, 3, 11, 10, 7, 8, 4, -1, -1, -1, -1, -1, -1, -1},
	{1, 11, 10, 1, 4, 11, 1, 0, 4, 7, 11, 4, -1, -1, -1, -1},
	{4, 7, 8, 9, 0, 11, 9, 11, 10, 11, 0, 3, -1, -1, -1, -1}, //30
	{4, 7, 11, 4, 11, 9, 9, 11, 10, -1, -1, -1, -1, -1, -1, -1},
	{9, 5, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{9, 5, 4, 0, 8, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{0, 5, 4, 1, 5, 0, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{8, 5, 4, 8, 3, 5, 3, 1, 5, -1, -1, -1, -1, -1, -1, -1},
	{1, 2, 10, 9, 5, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{3, 0, 8, 1, 2, 10, 4, 9, 5, -1, -1, -1, -1, -1, -1, -1},
	{5, 2, 10, 5, 4, 2, 4, 0, 2, -1, -1, -1, -1, -1, -1, -1},
	{2, 10, 5, 3, 2, 5, 3, 5, 4, 3, 4, 8, -1, -1, -1, -1},
	{9, 5, 4, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1}, //40
	{0, 11, 2, 0, 8, 11, 4, 9, 5, -1, -1, -1, -1, -1, -1, -1},
	{0, 5, 4, 0, 1, 5, 2, 3, 11, -1, -1, -1, -1, -1, -1, -1},
	{2, 1, 5, 2, 5, 8, 2, 8, 11, 4, 8, 5, -1, -1, -1, -1},
	{10, 3, 11, 10, 1, 3, 9, 5, 4, -1, -1, -1, -1, -1, -1, -1},
	{4, 9, 5, 0, 8, 1, 8, 10, 1, 8, 11, 10, -1, -1, -1, -1},
	{5, 4, 0, 5, 0, 11, 5, 11, 10, 11, 0, 3, -1, -1, -1, -1},
	{5, 4, 8, 5, 8, 10, 10, 8, 11, -1, -1, -1, -1, -1, -1, -1},
	{9, 7, 8, 5, 7, 9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{9, 3, 0, 9, 5, 3, 5, 7, 3, -1, -1, -1, -1, -1, -1, -1},
	{0, 7, 8, 0, 1, 7, 1, 5, 7, -1, -1, -1, -1, -1, -1, -1}, //50
	{1, 5, 3, 3, 5, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{9, 7, 8, 9, 5, 7, 10, 1, 2, -1, -1, -1, -1, -1, -1, -1},
	{10, 1, 2, 9, 5, 0, 5, 3, 0, 5, 7, 3, -1, -1, -1, -1},
	{8, 0, 2, 8, 2, 5, 8, 5, 7, 10, 5, 2, -1, -1, -1, -1},
	{2, 10, 5, 2, 5, 3, 3, 5, 7, -1, -1, -1, -1, -1, -1, -1},
	{7, 9, 5, 7, 8, 9, 3, 11, 2, -1, -1, -1, -1, -1, -1, -1},
	{9, 5, 7, 9, 7, 2, 9, 2, 0, 2, 7, 11, -1, -1, -1, -1},
	{2, 3, 11, 0, 1, 8, 1, 7, 8, 1, 5, 7, -1, -1, -1, -1},
	{11, 2, 1, 11, 1, 7, 7, 1, 5, -1, -1, -1, -1, -1, -1, -1},
	{9, 5, 8, 8, 5, 7, 10, 1, 3, 10, 3, 11, -1, -1, -1, -1}, //60
	{5, 7, 0, 5, 0, 9, 7, 11, 0, 1, 0, 10, 11, 10, 0, -1},
	{11, 10, 0, 11, 0, 3, 10, 5, 0, 8, 0, 7, 5, 7, 0, -1},
	{11, 10, 5, 7, 11, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{10, 6, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{0, 8, 3, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{9, 0, 1, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{1, 8, 3, 1, 9, 8, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1},
	{1, 6, 5, 2, 6, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{1, 6, 5, 1, 2, 6, 3, 0, 8, -1, -1, -1, -1, -1, -1, -1},
	{9, 6, 5, 9, 0, 6, 0, 2, 6, -1, -1, -1, -1, -1, -1, -1}, //70
	{5, 9, 8, 5, 8, 2, 5, 2, 6, 3, 2, 8, -1, -1, -1, -1},
	{2, 3, 11, 10, 6, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{11, 0, 8, 11, 2, 0, 10, 6, 5, -1, -1, -1, -1, -1, -1, -1},
	{0, 1, 9, 2, 3, 11, 5, 10, 6, -1, -1, -1, -1, -1, -1, -1},
	{5, 10, 6, 1, 9, 2, 9, 11, 2, 9, 8, 11, -1, -1, -1, -1},
	{6, 3, 11, 6, 5, 3, 5, 1, 3, -1, -1, -1, -1, -1, -1, -1},
	{0, 8, 11, 0, 11, 5, 0, 5, 1, 5, 11, 6, -1, -1, -1, -1},
	{3, 11, 6, 0, 3, 6, 0, 6, 5, 0, 5, 9, -1, -1, -1, -1},
	{6, 5, 9, 6, 9, 11, 11, 9, 8, -1, -1, -1, -1, -1, -1, -1},
	{5, 10, 6, 4, 7, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1}, //80
	{4, 3, 0, 4, 7, 3, 6, 5, 10, -1, -1, -1, -1, -1, -1, -1},
	{1, 9, 0, 5, 10, 6, 8, 4, 7, -1, -1, -1, -1, -1, -1, -1},
	{10, 6, 5, 1, 9, 7, 1, 7, 3, 7, 9, 4, -1, -1, -1, -1},
	{6, 1, 2, 6, 5, 1, 4, 7, 8, -1, -1, -1, -1, -1, -1, -1},
	{1, 2, 5, 5, 2, 6, 3, 0, 4, 3, 4, 7, -1, -1, -1, -1},
	{8, 4, 7, 9, 0, 5, 0, 6, 5, 0, 2, 6, -1, -1, -1, -1},
	{7, 3, 9, 7, 9, 4, 3, 2, 9, 5, 9, 6, 2, 6, 9, -1},
	{3, 11, 2, 7, 8, 4, 10, 6, 5, -1, -1, -1, -1, -1, -1, -1},
	{5, 10, 6, 4, 7, 2, 4, 2, 0, 2, 7, 11, -1, -1, -1, -1},
	{0, 1, 9, 4, 7, 8, 2, 3, 11, 5, 10, 6, -1, -1, -1, -1}, //90
	{9, 2, 1, 9, 11, 2, 9, 4, 11, 7, 11, 4, 5, 10, 6, -1},
	{8, 4, 7, 3, 11, 5, 3, 5, 1, 5, 11, 6, -1, -1, -1, -1},
	{5, 1, 11, 5, 11, 6, 1, 0, 11, 7, 11, 4, 0, 4, 11, -1},
	{0, 5, 9, 0, 6, 5, 0, 3, 6, 11, 6, 3, 8, 4, 7, -1},
	{6, 5, 9, 6, 9, 11, 4, 7, 9, 7, 11, 9, -1, -1, -1, -1},
	{10, 4, 9, 6, 4, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{4, 10, 6, 4, 9, 10, 0, 8, 3, -1, -1, -1, -1, -1, -1, -1},
	{10, 0, 1, 10, 6, 0, 6, 4, 0, -1, -1, -1, -1, -1, -1, -1},
	{8, 3, 1, 8, 1, 6, 8, 6, 4, 6, 1, 10, -1, -1, -1, -1},
	{1, 4, 9, 1, 2, 4, 2, 6, 4, -1, -1, -1, -1, -1, -1, -1}, //100
	{3, 0, 8, 1, 2, 9, 2, 4, 9, 2, 6, 4, -1, -1, -1, -1},
	{0, 2, 4, 4, 2, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{8, 3, 2, 8, 2, 4, 4, 2, 6, -1, -1, -1, -1, -1, -1, -1},
	{10, 4, 9, 10, 6, 4, 11, 2, 3, -1, -1, -1, -1, -1, -1, -1},
	{0, 8, 2, 2, 8, 11, 4, 9, 10, 4, 10, 6, -1, -1, -1, -1},
	{3, 11, 2, 0, 1, 6, 0, 6, 4, 6, 1, 10, -1, -1, -1, -1},
	{6, 4, 1, 6, 1, 10, 4, 8, 1, 2, 1, 11, 8, 11, 1, -1},
	{9, 6, 4, 9, 3, 6, 9, 1, 3, 11, 6, 3, -1, -1, -1, -1},
	{8, 11, 1, 8, 1, 0, 11, 6, 1, 9, 1, 4, 6, 4, 1, -1},
	{3, 11, 6, 3, 6, 0, 0, 6, 4, -1, -1, -1, -1, -1, -1, -1}, //110
	{6, 4, 8, 11, 6, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{7, 10, 6, 7, 8, 10, 8, 9, 10, -1, -1, -1, -1, -1, -1, -1},
	{0, 7, 3, 0, 10, 7, 0, 9, 10, 6, 7, 10, -1, -1, -1, -1},
	{10, 6, 7, 1, 10, 7, 1, 7, 8, 1, 8, 0, -1, -1, -1, -1},
	{10, 6, 7, 10, 7, 1, 1, 7, 3, -1, -1, -1, -1, -1, -1, -1},
	{1, 2, 6, 1, 6, 8, 1, 8, 9, 8, 6, 7, -1, -1, -1, -1},
	{2, 6, 9, 2, 9, 1, 6, 7, 9, 0, 9, 3, 7, 3, 9, -1},
	{7, 8, 0, 7, 0, 6, 6, 0, 2, -1, -1, -1, -1, -1, -1, -1},
	{7, 3, 2, 6, 7, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{2, 3, 11, 10, 6, 8, 10, 8, 9, 8, 6, 7, -1, -1, -1, -1}, //120
	{2, 0, 7, 2, 7, 11, 0, 9, 7, 6, 7, 10, 9, 10, 7, -1},
	{1, 8, 0, 1, 7, 8, 1, 10, 7, 6, 7, 10, 2, 3, 11, -1},
	{11, 2, 1, 11, 1, 7, 10, 6, 1, 6, 7, 1, -1, -1, -1, -1},
	{8, 9, 6, 8, 6, 7, 9, 1, 6, 11, 6, 3, 1, 3, 6, -1},
	{0, 9, 1, 11, 6, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{7, 8, 0, 7, 0, 6, 3, 11, 0, 11, 6, 0, -1, -1, -1, -1},
	{7, 11, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{7, 6, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{3, 0, 8, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{0, 1, 9, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1}, //130
	{8, 1, 9, 8, 3, 1, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1}, 
	{10, 1, 2, 6, 11, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{1, 2, 10, 3, 0, 8, 6, 11, 7, -1, -1, -1, -1, -1, -1, -1},
	{2, 9, 0, 2, 10, 9, 6, 11, 7, -1, -1, -1, -1, -1, -1, -1},
	{6, 11, 7, 2, 10, 3, 10, 8, 3, 10, 9, 8, -1, -1, -1, -1},
	{7, 2, 3, 6, 2, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{7, 0, 8, 7, 6, 0, 6, 2, 0, -1, -1, -1, -1, -1, -1, -1},
	{2, 7, 6, 2, 3, 7, 0, 1, 9, -1, -1, -1, -1, -1, -1, -1},
	{1, 6, 2, 1, 8, 6, 1, 9, 8, 8, 7, 6, -1, -1, -1, -1},
	{10, 7, 6, 10, 1, 7, 1, 3, 7, -1, -1, -1, -1, -1, -1, -1}, //140
	{10, 7, 6, 1, 7, 10, 1, 8, 7, 1, 0, 8, -1, -1, -1, -1},
	{0, 3, 7, 0, 7, 10, 0, 10, 9, 6, 10, 7, -1, -1, -1, -1},
	{7, 6, 10, 7, 10, 8, 8, 10, 9, -1, -1, -1, -1, -1, -1, -1},
	{6, 8, 4, 11, 8, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{3, 6, 11, 3, 0, 6, 0, 4, 6, -1, -1, -1, -1, -1, -1, -1},
	{8, 6, 11, 8, 4, 6, 9, 0, 1, -1, -1, -1, -1, -1, -1, -1},
	{9, 4, 6, 9, 6, 3, 9, 3, 1, 11, 3, 6, -1, -1, -1, -1},
	{6, 8, 4, 6, 11, 8, 2, 10, 1, -1, -1, -1, -1, -1, -1, -1},
	{1, 2, 10, 3, 0, 11, 0, 6, 11, 0, 4, 6, -1, -1, -1, -1},
	{4, 11, 8, 4, 6, 11, 0, 2, 9, 2, 10, 9, -1, -1, -1, -1}, //150
	{10, 9, 3, 10, 3, 2, 9, 4, 3, 11, 3, 6, 4, 6, 3, -1},
	{8, 2, 3, 8, 4, 2, 4, 6, 2, -1, -1, -1, -1, -1, -1, -1},
	{0, 4, 2, 4, 6, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{1, 9, 0, 2, 3, 4, 2, 4, 6, 4, 3, 8, -1, -1, -1, -1},
	{1, 9, 4, 1, 4, 2, 2, 4, 6, -1, -1, -1, -1, -1, -1, -1},
	{8, 1, 3, 8, 6, 1, 8, 4, 6, 6, 10, 1, -1, -1, -1, -1},
	{10, 1, 0, 10, 0, 6, 6, 0, 4, -1, -1, -1, -1, -1, -1, -1},
	{4, 6, 3, 4, 3, 8, 6, 10, 3, 0, 3, 9, 10, 9, 3, -1},
	{10, 9, 4, 6, 10, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{4, 9, 5, 7, 6, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1}, //160
	{0, 8, 3, 4, 9, 5, 11, 7, 6, -1, -1, -1, -1, -1, -1, -1}, 
	{5, 0, 1, 5, 4, 0, 7, 6, 11, -1, -1, -1, -1, -1, -1, -1},
	{11, 7, 6, 8, 3, 4, 3, 5, 4, 3, 1, 5, -1, -1, -1, -1},
	{9, 5, 4, 10, 1, 2, 7, 6, 11, -1, -1, -1, -1, -1, -1, -1},
	{6, 11, 7, 1, 2, 10, 0, 8, 3, 4, 9, 5, -1, -1, -1, -1},
	{7, 6, 11, 5, 4, 10, 4, 2, 10, 4, 0, 2, -1, -1, -1, -1},
	{3, 4, 8, 3, 5, 4, 3, 2, 5, 10, 5, 2, 11, 7, 6, -1},
	{7, 2, 3, 7, 6, 2, 5, 4, 9, -1, -1, -1, -1, -1, -1, -1},
	{9, 5, 4, 0, 8, 6, 0, 6, 2, 6, 8, 7, -1, -1, -1, -1},
	{3, 6, 2, 3, 7, 6, 1, 5, 0, 5, 4, 0, -1, -1, -1, -1}, //170
	{6, 2, 8, 6, 8, 7, 2, 1, 8, 4, 8, 5, 1, 5, 8, -1},
	{9, 5, 4, 10, 1, 6, 1, 7, 6, 1, 3, 7, -1, -1, -1, -1},
	{1, 6, 10, 1, 7, 6, 1, 0, 7, 8, 7, 0, 9, 5, 4, -1},
	{4, 0, 10, 4, 10, 5, 0, 3, 10, 6, 10, 7, 3, 7, 10, -1},
	{7, 6, 10, 7, 10, 8, 5, 4, 10, 4, 8, 10, -1, -1, -1, -1},
	{6, 9, 5, 6, 11, 9, 11, 8, 9, -1, -1, -1, -1, -1, -1, -1},
	{3, 6, 11, 0, 6, 3, 0, 5, 6, 0, 9, 5, -1, -1, -1, -1},
	{0, 11, 8, 0, 5, 11, 0, 1, 5, 5, 6, 11, -1, -1, -1, -1},
	{6, 11, 3, 6, 3, 5, 5, 3, 1, -1, -1, -1, -1, -1, -1, -1},
	{1, 2, 10, 9, 5, 11, 9, 11, 8, 11, 5, 6, -1, -1, -1, -1}, //180
	{0, 11, 3, 0, 6, 11, 0, 9, 6, 5, 6, 9, 1, 2, 10, -1},
	{11, 8, 5, 11, 5, 6, 8, 0, 5, 10, 5, 2, 0, 2, 5, -1},
	{6, 11, 3, 6, 3, 5, 2, 10, 3, 10, 5, 3, -1, -1, -1, -1},
	{5, 8, 9, 5, 2, 8, 5, 6, 2, 3, 8, 2, -1, -1, -1, -1},
	{9, 5, 6, 9, 6, 0, 0, 6, 2, -1, -1, -1, -1, -1, -1, -1},
	{1, 5, 8, 1, 8, 0, 5, 6, 8, 3, 8, 2, 6, 2, 8, -1},
	{1, 5, 6, 2, 1, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{1, 3, 6, 1, 6, 10, 3, 8, 6, 5, 6, 9, 8, 9, 6, -1},
	{10, 1, 0, 10, 0, 6, 9, 5, 0, 5, 6, 0, -1, -1, -1, -1},
	{0, 3, 8, 5, 6, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1}, //190
	{10, 5, 6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{11, 5, 10, 7, 5, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{11, 5, 10, 11, 7, 5, 8, 3, 0, -1, -1, -1, -1, -1, -1, -1},
	{5, 11, 7, 5, 10, 11, 1, 9, 0, -1, -1, -1, -1, -1, -1, -1},
	{10, 7, 5, 10, 11, 7, 9, 8, 1, 8, 3, 1, -1, -1, -1, -1},
	{11, 1, 2, 11, 7, 1, 7, 5, 1, -1, -1, -1, -1, -1, -1, -1},
	{0, 8, 3, 1, 2, 7, 1, 7, 5, 7, 2, 11, -1, -1, -1, -1},
	{9, 7, 5, 9, 2, 7, 9, 0, 2, 2, 11, 7, -1, -1, -1, -1},
	{7, 5, 2, 7, 2, 11, 5, 9, 2, 3, 2, 8, 9, 8, 2, -1},
	{2, 5, 10, 2, 3, 5, 3, 7, 5, -1, -1, -1, -1, -1, -1, -1}, //200
	{8, 2, 0, 8, 5, 2, 8, 7, 5, 10, 2, 5, -1, -1, -1, -1},
	{9, 0, 1, 5, 10, 3, 5, 3, 7, 3, 10, 2, -1, -1, -1, -1},
	{9, 8, 2, 9, 2, 1, 8, 7, 2, 10, 2, 5, 7, 5, 2, -1},
	{1, 3, 5, 3, 7, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{0, 8, 7, 0, 7, 1, 1, 7, 5, -1, -1, -1, -1, -1, -1, -1},
	{9, 0, 3, 9, 3, 5, 5, 3, 7, -1, -1, -1, -1, -1, -1, -1},
	{9, 8, 7, 5, 9, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{5, 8, 4, 5, 10, 8, 10, 11, 8, -1, -1, -1, -1, -1, -1, -1},
	{5, 0, 4, 5, 11, 0, 5, 10, 11, 11, 3, 0, -1, -1, -1, -1},
	{0, 1, 9, 8, 4, 10, 8, 10, 11, 10, 4, 5, -1, -1, -1, -1}, //210
	{10, 11, 4, 10, 4, 5, 11, 3, 4, 9, 4, 1, 3, 1, 4, -1},
	{2, 5, 1, 2, 8, 5, 2, 11, 8, 4, 5, 8, -1, -1, -1, -1},
	{0, 4, 11, 0, 11, 3, 4, 5, 11, 2, 11, 1, 5, 1, 11, -1},
	{0, 2, 5, 0, 5, 9, 2, 11, 5, 4, 5, 8, 11, 8, 5, -1},
	{9, 4, 5, 2, 11, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{2, 5, 10, 3, 5, 2, 3, 4, 5, 3, 8, 4, -1, -1, -1, -1},
	{5, 10, 2, 5, 2, 4, 4, 2, 0, -1, -1, -1, -1, -1, -1, -1},
	{3, 10, 2, 3, 5, 10, 3, 8, 5, 4, 5, 8, 0, 1, 9, -1},
	{5, 10, 2, 5, 2, 4, 1, 9, 2, 9, 4, 2, -1, -1, -1, -1},
	{8, 4, 5, 8, 5, 3, 3, 5, 1, -1, -1, -1, -1, -1, -1, -1}, //220
	{0, 4, 5, 1, 0, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{8, 4, 5, 8, 5, 3, 9, 0, 5, 0, 3, 5, -1, -1, -1, -1},
	{9, 4, 5, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{4, 11, 7, 4, 9, 11, 9, 10, 11, -1, -1, -1, -1, -1, -1, -1},
	{0, 8, 3, 4, 9, 7, 9, 11, 7, 9, 10, 11, -1, -1, -1, -1},
	{1, 10, 11, 1, 11, 4, 1, 4, 0, 7, 4, 11, -1, -1, -1, -1},
	{3, 1, 4, 3, 4, 8, 1, 10, 4, 7, 4, 11, 10, 11, 4, -1},
	{4, 11, 7, 9, 11, 4, 9, 2, 11, 9, 1, 2, -1, -1, -1, -1},
	{9, 7, 4, 9, 11, 7, 9, 1, 11, 2, 11, 1, 0, 8, 3, -1},
	{11, 7, 4, 11, 4, 2, 2, 4, 0, -1, -1, -1, -1, -1, -1, -1}, //230
	{11, 7, 4, 11, 4, 2, 8, 3, 4, 3, 2, 4, -1, -1, -1, -1},
	{2, 9, 10, 2, 7, 9, 2, 3, 7, 7, 4, 9, -1, -1, -1, -1},
	{9, 10, 7, 9, 7, 4, 10, 2, 7, 8, 7, 0, 2, 0, 7, -1},
	{3, 7, 10, 3, 10, 2, 7, 4, 10, 1, 10, 0, 4, 0, 10, -1},
	{1, 10, 2, 8, 7, 4, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{4, 9, 1, 4, 1, 7, 7, 1, 3, -1, -1, -1, -1, -1, -1, -1},
	{4, 9, 1, 4, 1, 7, 0, 8, 1, 8, 7, 1, -1, -1, -1, -1},
	{4, 0, 3, 7, 4, 3, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{4, 8, 7, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{9, 10, 8, 10, 11, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1}, //240
	{3, 0, 9, 3, 9, 11, 11, 9, 10, -1, -1, -1, -1, -1, -1, -1},
	{0, 1, 10, 0, 10, 8, 8, 10, 11, -1, -1, -1, -1, -1, -1, -1},
	{3, 1, 10, 11, 3, 10, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{1, 2, 11, 1, 11, 9, 9, 11, 8, -1, -1, -1, -1, -1, -1, -1},
	{3, 0, 9, 3, 9, 11, 1, 2, 9, 2, 11, 9, -1, -1, -1, -1},
	{0, 2, 11, 8, 0, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{3, 2, 11, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{2, 3, 8, 2, 8, 10, 10, 8, 9, -1, -1, -1, -1, -1, -1, -1},
	{9, 10, 2, 0, 9, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{2, 3, 8, 2, 8, 10, 0, 1, 8, 1, 10, 8, -1, -1, -1, -1}, //250
	{1, 10, 2, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{1, 3, 8, 9, 1, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{0, 9, 1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{0, 3, 8, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1},
	{-1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1}};

	private static readonly byte[][] triTableOpt = new byte[256][]
	{
		new byte[]{},
		new byte[]{0, 2, 1, },
		new byte[]{0, 1, 2, },
		new byte[]{0, 2, 1, 3, 2, 0, },
		new byte[]{0, 1, 2, },
		new byte[]{0, 4, 3, 1, 2, 5, },
		new byte[]{2, 1, 3, 0, 1, 2, },
		new byte[]{0, 2, 1, 0, 4, 2, 4, 3, 2, },
		new byte[]{1, 2, 0, },
		new byte[]{0, 3, 1, 2, 3, 0, },
		new byte[]{1, 4, 0, 2, 3, 5, },
		new byte[]{0, 4, 1, 0, 3, 4, 3, 2, 4, },
		new byte[]{1, 2, 0, 3, 2, 1, },
		new byte[]{0, 3, 1, 0, 2, 3, 2, 4, 3, },
		new byte[]{1, 2, 0, 1, 4, 2, 4, 3, 2, },
		new byte[]{1, 0, 2, 2, 0, 3, },
		new byte[]{0, 1, 2, },
		new byte[]{2, 1, 0, 3, 1, 2, },
		new byte[]{0, 1, 5, 4, 2, 3, },
		new byte[]{2, 0, 4, 2, 3, 0, 3, 1, 0, },
		new byte[]{0, 1, 5, 4, 2, 3, },
		new byte[]{3, 4, 5, 3, 0, 4, 1, 2, 6, },
		new byte[]{5, 1, 6, 5, 0, 1, 4, 2, 3, },
		new byte[]{0, 5, 4, 0, 4, 3, 0, 3, 1, 3, 4, 2, },
		new byte[]{4, 2, 3, 1, 5, 0, },
		new byte[]{4, 2, 3, 4, 1, 2, 1, 0, 2, },
		new byte[]{7, 0, 1, 6, 4, 5, 2, 3, 8, },
		new byte[]{2, 3, 5, 4, 2, 5, 4, 5, 1, 4, 1, 0, },
		new byte[]{1, 5, 0, 1, 6, 5, 3, 4, 2, },
		new byte[]{1, 5, 4, 1, 2, 5, 1, 0, 2, 3, 5, 2, },
		new byte[]{2, 3, 4, 5, 0, 7, 5, 7, 6, 7, 0, 1, },
		new byte[]{0, 1, 4, 0, 4, 2, 2, 4, 3, },
		new byte[]{2, 1, 0, },
		new byte[]{5, 3, 2, 0, 4, 1, },
		new byte[]{0, 3, 2, 1, 3, 0, },
		new byte[]{4, 3, 2, 4, 1, 3, 1, 0, 3, },
		new byte[]{0, 1, 5, 4, 3, 2, },
		new byte[]{3, 0, 6, 1, 2, 8, 4, 7, 5, },
		new byte[]{3, 1, 4, 3, 2, 1, 2, 0, 1, },
		new byte[]{0, 5, 3, 1, 0, 3, 1, 3, 2, 1, 2, 4, },
		new byte[]{4, 3, 2, 0, 1, 5, },
		new byte[]{0, 6, 1, 0, 4, 6, 2, 5, 3, },
		new byte[]{0, 5, 4, 0, 1, 5, 2, 3, 6, },
		new byte[]{1, 0, 3, 1, 3, 4, 1, 4, 5, 2, 4, 3, },
		new byte[]{5, 1, 6, 5, 0, 1, 4, 3, 2, },
		new byte[]{2, 5, 3, 0, 4, 1, 4, 6, 1, 4, 7, 6, },
		new byte[]{3, 2, 0, 3, 0, 5, 3, 5, 4, 5, 0, 1, },
		new byte[]{1, 0, 2, 1, 2, 3, 3, 2, 4, },
		new byte[]{3, 1, 2, 0, 1, 3, },
		new byte[]{4, 1, 0, 4, 2, 1, 2, 3, 1, },
		new byte[]{0, 3, 4, 0, 1, 3, 1, 2, 3, },
		new byte[]{0, 2, 1, 1, 2, 3, },
		new byte[]{5, 3, 4, 5, 2, 3, 6, 0, 1, },
		new byte[]{7, 1, 2, 6, 4, 0, 4, 3, 0, 4, 5, 3, },
		new byte[]{4, 0, 1, 4, 1, 2, 4, 2, 3, 5, 2, 1, },
		new byte[]{0, 4, 2, 0, 2, 1, 1, 2, 3, },
		new byte[]{3, 5, 2, 3, 4, 5, 1, 6, 0, },
		new byte[]{4, 2, 3, 4, 3, 1, 4, 1, 0, 1, 3, 5, },
		new byte[]{2, 3, 7, 0, 1, 6, 1, 5, 6, 1, 4, 5, },
		new byte[]{4, 1, 0, 4, 0, 3, 3, 0, 2, },
		new byte[]{5, 2, 4, 4, 2, 3, 6, 0, 1, 6, 1, 7, },
		new byte[]{2, 3, 0, 2, 0, 4, 3, 6, 0, 1, 0, 5, 6, 5, 0, },
		new byte[]{6, 5, 0, 6, 0, 1, 5, 2, 0, 4, 0, 3, 2, 3, 0, },
		new byte[]{3, 2, 0, 1, 3, 0, },
		new byte[]{2, 1, 0, },
		new byte[]{0, 4, 1, 2, 5, 3, },
		new byte[]{4, 0, 1, 2, 5, 3, },
		new byte[]{0, 4, 1, 0, 5, 4, 2, 6, 3, },
		new byte[]{0, 3, 2, 1, 3, 0, },
		new byte[]{1, 5, 4, 1, 2, 5, 3, 0, 6, },
		new byte[]{4, 3, 2, 4, 0, 3, 0, 1, 3, },
		new byte[]{2, 5, 4, 2, 4, 0, 2, 0, 3, 1, 0, 4, },
		new byte[]{0, 1, 5, 4, 3, 2, },
		new byte[]{6, 0, 4, 6, 1, 0, 5, 3, 2, },
		new byte[]{0, 1, 6, 2, 3, 8, 4, 7, 5, },
		new byte[]{2, 6, 3, 0, 5, 1, 5, 7, 1, 5, 4, 7, },
		new byte[]{3, 1, 4, 3, 2, 1, 2, 0, 1, },
		new byte[]{0, 4, 5, 0, 5, 2, 0, 2, 1, 2, 5, 3, },
		new byte[]{1, 5, 3, 0, 1, 3, 0, 3, 2, 0, 2, 4, },
		new byte[]{1, 0, 3, 1, 3, 4, 4, 3, 2, },
		new byte[]{1, 5, 2, 0, 3, 4, },
		new byte[]{2, 1, 0, 2, 5, 1, 4, 3, 6, },
		new byte[]{1, 7, 0, 3, 8, 4, 6, 2, 5, },
		new byte[]{7, 4, 3, 0, 6, 5, 0, 5, 1, 5, 6, 2, },
		new byte[]{4, 0, 1, 4, 3, 0, 2, 5, 6, },
		new byte[]{1, 2, 5, 5, 2, 6, 3, 0, 4, 3, 4, 7, },
		new byte[]{6, 2, 5, 7, 0, 3, 0, 4, 3, 0, 1, 4, },
		new byte[]{5, 1, 6, 5, 6, 2, 1, 0, 6, 3, 6, 4, 0, 4, 6, },
		new byte[]{1, 8, 0, 5, 6, 2, 7, 4, 3, },
		new byte[]{3, 6, 4, 2, 5, 1, 2, 1, 0, 1, 5, 7, },
		new byte[]{0, 1, 9, 4, 7, 8, 2, 3, 11, 5, 10, 6, },
		new byte[]{6, 1, 0, 6, 8, 1, 6, 2, 8, 5, 8, 2, 3, 7, 4, },
		new byte[]{6, 2, 5, 1, 7, 3, 1, 3, 0, 3, 7, 4, },
		new byte[]{3, 1, 6, 3, 6, 4, 1, 0, 6, 5, 6, 2, 0, 2, 6, },
		new byte[]{0, 3, 7, 0, 4, 3, 0, 1, 4, 8, 4, 1, 6, 2, 5, },
		new byte[]{2, 1, 4, 2, 4, 5, 0, 3, 4, 3, 5, 4, },
		new byte[]{3, 0, 2, 1, 0, 3, },
		new byte[]{2, 6, 3, 2, 5, 6, 0, 4, 1, },
		new byte[]{4, 0, 1, 4, 3, 0, 3, 2, 0, },
		new byte[]{4, 1, 0, 4, 0, 3, 4, 3, 2, 3, 0, 5, },
		new byte[]{0, 2, 4, 0, 1, 2, 1, 3, 2, },
		new byte[]{3, 0, 6, 1, 2, 7, 2, 4, 7, 2, 5, 4, },
		new byte[]{0, 1, 2, 2, 1, 3, },
		new byte[]{4, 1, 0, 4, 0, 2, 2, 0, 3, },
		new byte[]{5, 2, 4, 5, 3, 2, 6, 0, 1, },
		new byte[]{0, 4, 1, 1, 4, 7, 2, 5, 6, 2, 6, 3, },
		new byte[]{3, 7, 2, 0, 1, 5, 0, 5, 4, 5, 1, 6, },
		new byte[]{3, 2, 0, 3, 0, 5, 2, 4, 0, 1, 0, 6, 4, 6, 0, },
		new byte[]{4, 3, 2, 4, 1, 3, 4, 0, 1, 5, 3, 1, },
		new byte[]{4, 6, 1, 4, 1, 0, 6, 3, 1, 5, 1, 2, 3, 2, 1, },
		new byte[]{1, 4, 3, 1, 3, 0, 0, 3, 2, },
		new byte[]{1, 0, 2, 3, 1, 2, },
		new byte[]{1, 4, 0, 1, 2, 4, 2, 3, 4, },
		new byte[]{0, 3, 1, 0, 5, 3, 0, 4, 5, 2, 3, 5, },
		new byte[]{5, 2, 3, 1, 5, 3, 1, 3, 4, 1, 4, 0, },
		new byte[]{4, 2, 3, 4, 3, 0, 0, 3, 1, },
		new byte[]{0, 1, 2, 0, 2, 4, 0, 4, 5, 4, 2, 3, },
		new byte[]{2, 4, 6, 2, 6, 1, 4, 5, 6, 0, 6, 3, 5, 3, 6, },
		new byte[]{3, 4, 0, 3, 0, 2, 2, 0, 1, },
		new byte[]{3, 1, 0, 2, 3, 0, },
		new byte[]{0, 1, 7, 6, 2, 4, 6, 4, 5, 4, 2, 3, },
		new byte[]{1, 0, 3, 1, 3, 6, 0, 4, 3, 2, 3, 5, 4, 5, 3, },
		new byte[]{1, 6, 0, 1, 5, 6, 1, 7, 5, 4, 5, 7, 2, 3, 8, },
		new byte[]{5, 1, 0, 5, 0, 3, 4, 2, 0, 2, 3, 0, },
		new byte[]{4, 5, 2, 4, 2, 3, 5, 0, 2, 6, 2, 1, 0, 1, 2, },
		new byte[]{0, 4, 1, 5, 2, 3, },
		new byte[]{3, 4, 0, 3, 0, 2, 1, 5, 0, 5, 2, 0, },
		new byte[]{1, 2, 0, },
		new byte[]{1, 0, 2, },
		new byte[]{1, 0, 4, 5, 3, 2, },
		new byte[]{0, 1, 4, 5, 3, 2, },
		new byte[]{4, 0, 5, 4, 1, 0, 6, 3, 2, },
		new byte[]{4, 0, 1, 2, 5, 3, },
		new byte[]{1, 2, 7, 3, 0, 6, 4, 8, 5, },
		new byte[]{1, 4, 0, 1, 5, 4, 2, 6, 3, },
		new byte[]{2, 7, 3, 0, 6, 1, 6, 4, 1, 6, 5, 4, },
		new byte[]{3, 0, 1, 2, 0, 3, },
		new byte[]{3, 0, 4, 3, 2, 0, 2, 1, 0, },
		new byte[]{2, 5, 4, 2, 3, 5, 0, 1, 6, },
		new byte[]{0, 2, 1, 0, 4, 2, 0, 5, 4, 4, 3, 2, },
		new byte[]{4, 3, 2, 4, 0, 3, 0, 1, 3, },
		new byte[]{5, 3, 2, 1, 3, 5, 1, 4, 3, 1, 0, 4, },
		new byte[]{0, 1, 3, 0, 3, 5, 0, 5, 4, 2, 5, 3, },
		new byte[]{1, 0, 4, 1, 4, 2, 2, 4, 3, },
		new byte[]{1, 2, 0, 3, 2, 1, },
		new byte[]{1, 3, 4, 1, 0, 3, 0, 2, 3, },
		new byte[]{4, 3, 6, 4, 2, 3, 5, 0, 1, },
		new byte[]{4, 2, 3, 4, 3, 1, 4, 1, 0, 5, 1, 3, },
		new byte[]{3, 4, 2, 3, 6, 4, 1, 5, 0, },
		new byte[]{1, 2, 6, 3, 0, 7, 0, 5, 7, 0, 4, 5, },
		new byte[]{2, 7, 4, 2, 3, 7, 0, 1, 5, 1, 6, 5, },
		new byte[]{5, 4, 1, 5, 1, 0, 4, 2, 1, 6, 1, 3, 2, 3, 1, },
		new byte[]{4, 0, 1, 4, 2, 0, 2, 3, 0, },
		new byte[]{0, 2, 1, 2, 3, 1, },
		new byte[]{1, 7, 0, 2, 3, 4, 2, 4, 5, 4, 3, 6, },
		new byte[]{0, 4, 2, 0, 2, 1, 1, 2, 3, },
		new byte[]{4, 0, 1, 4, 3, 0, 4, 2, 3, 3, 5, 0, },
		new byte[]{4, 1, 0, 4, 0, 3, 3, 0, 2, },
		new byte[]{2, 3, 1, 2, 1, 4, 3, 6, 1, 0, 1, 5, 6, 5, 1, },
		new byte[]{3, 2, 0, 1, 3, 0, },
		new byte[]{0, 4, 1, 3, 2, 5, },
		new byte[]{0, 6, 1, 2, 7, 3, 8, 5, 4, },
		new byte[]{3, 0, 1, 3, 2, 0, 5, 4, 6, },
		new byte[]{7, 5, 4, 6, 1, 2, 1, 3, 2, 1, 0, 3, },
		new byte[]{6, 3, 2, 7, 0, 1, 5, 4, 8, },
		new byte[]{6, 11, 7, 1, 2, 10, 0, 8, 3, 4, 9, 5, },
		new byte[]{5, 4, 7, 3, 2, 6, 2, 1, 6, 2, 0, 1, },
		new byte[]{1, 2, 6, 1, 3, 2, 1, 0, 3, 7, 3, 0, 8, 5, 4, },
		new byte[]{5, 0, 1, 5, 4, 0, 3, 2, 6, },
		new byte[]{7, 3, 2, 0, 6, 4, 0, 4, 1, 4, 6, 5, },
		new byte[]{3, 6, 2, 3, 7, 6, 1, 5, 0, 5, 4, 0, },
		new byte[]{4, 1, 6, 4, 6, 5, 1, 0, 6, 2, 6, 3, 0, 3, 6, },
		new byte[]{6, 3, 2, 7, 0, 4, 0, 5, 4, 0, 1, 5, },
		new byte[]{1, 4, 8, 1, 5, 4, 1, 0, 5, 6, 5, 0, 7, 3, 2, },
		new byte[]{2, 0, 6, 2, 6, 3, 0, 1, 6, 4, 6, 5, 1, 5, 6, },
		new byte[]{3, 2, 5, 3, 5, 4, 1, 0, 5, 0, 4, 5, },
		new byte[]{1, 3, 0, 1, 4, 3, 4, 2, 3, },
		new byte[]{1, 3, 5, 0, 3, 1, 0, 2, 3, 0, 4, 2, },
		new byte[]{0, 5, 4, 0, 2, 5, 0, 1, 2, 2, 3, 5, },
		new byte[]{3, 4, 1, 3, 1, 2, 2, 1, 0, },
		new byte[]{0, 1, 6, 5, 2, 7, 5, 7, 4, 7, 2, 3, },
		new byte[]{0, 8, 3, 0, 5, 8, 0, 6, 5, 4, 5, 6, 1, 2, 7, },
		new byte[]{6, 4, 2, 6, 2, 3, 4, 0, 2, 5, 2, 1, 0, 1, 2, },
		new byte[]{3, 5, 1, 3, 1, 2, 0, 4, 1, 4, 2, 1, },
		new byte[]{2, 4, 5, 2, 0, 4, 2, 3, 0, 1, 4, 0, },
		new byte[]{4, 2, 3, 4, 3, 0, 0, 3, 1, },
		new byte[]{1, 4, 6, 1, 6, 0, 4, 5, 6, 3, 6, 2, 5, 2, 6, },
		new byte[]{0, 2, 3, 1, 0, 3, },
		new byte[]{0, 1, 3, 0, 3, 6, 1, 4, 3, 2, 3, 5, 4, 5, 3, },
		new byte[]{5, 1, 0, 5, 0, 3, 4, 2, 0, 2, 3, 0, },
		new byte[]{0, 1, 4, 2, 3, 5, },
		new byte[]{2, 0, 1, },
		new byte[]{3, 0, 2, 1, 0, 3, },
		new byte[]{6, 2, 5, 6, 3, 2, 4, 1, 0, },
		new byte[]{2, 6, 3, 2, 5, 6, 1, 4, 0, },
		new byte[]{6, 3, 2, 6, 7, 3, 5, 4, 0, 4, 1, 0, },
		new byte[]{4, 0, 1, 4, 3, 0, 3, 2, 0, },
		new byte[]{0, 6, 3, 1, 2, 5, 1, 5, 4, 5, 2, 7, },
		new byte[]{4, 3, 2, 4, 1, 3, 4, 0, 1, 1, 5, 3, },
		new byte[]{3, 2, 0, 3, 0, 6, 2, 5, 0, 1, 0, 4, 5, 4, 0, },
		new byte[]{0, 2, 4, 0, 1, 2, 1, 3, 2, },
		new byte[]{4, 1, 0, 4, 2, 1, 4, 3, 2, 5, 1, 2, },
		new byte[]{6, 0, 1, 4, 7, 3, 4, 3, 5, 3, 7, 2, },
		new byte[]{5, 4, 1, 5, 1, 0, 4, 3, 1, 6, 1, 2, 3, 2, 1, },
		new byte[]{0, 1, 2, 1, 3, 2, },
		new byte[]{0, 4, 3, 0, 3, 1, 1, 3, 2, },
		new byte[]{4, 0, 1, 4, 1, 2, 2, 1, 3, },
		new byte[]{3, 2, 1, 0, 3, 1, },
		new byte[]{1, 2, 0, 1, 3, 2, 3, 4, 2, },
		new byte[]{3, 0, 2, 3, 5, 0, 3, 4, 5, 5, 1, 0, },
		new byte[]{0, 1, 5, 4, 2, 6, 4, 6, 7, 6, 2, 3, },
		new byte[]{5, 6, 2, 5, 2, 3, 6, 1, 2, 4, 2, 0, 1, 0, 2, },
		new byte[]{1, 3, 0, 1, 4, 3, 1, 5, 4, 2, 3, 4, },
		new byte[]{0, 4, 6, 0, 6, 3, 4, 5, 6, 2, 6, 1, 5, 1, 6, },
		new byte[]{0, 1, 3, 0, 3, 5, 1, 6, 3, 2, 3, 4, 6, 4, 3, },
		new byte[]{4, 2, 3, 0, 5, 1, },
		new byte[]{0, 3, 5, 1, 3, 0, 1, 2, 3, 1, 4, 2, },
		new byte[]{3, 4, 1, 3, 1, 2, 2, 1, 0, },
		new byte[]{3, 8, 2, 3, 5, 8, 3, 6, 5, 4, 5, 6, 0, 1, 7, },
		new byte[]{3, 5, 1, 3, 1, 2, 0, 4, 1, 4, 2, 1, },
		new byte[]{4, 2, 3, 4, 3, 1, 1, 3, 0, },
		new byte[]{0, 2, 3, 1, 0, 3, },
		new byte[]{4, 2, 3, 4, 3, 1, 5, 0, 3, 0, 1, 3, },
		new byte[]{2, 0, 1, },
		new byte[]{0, 4, 1, 0, 2, 4, 2, 3, 4, },
		new byte[]{0, 4, 1, 2, 5, 3, 5, 7, 3, 5, 6, 7, },
		new byte[]{1, 4, 5, 1, 5, 2, 1, 2, 0, 3, 2, 5, },
		new byte[]{1, 0, 2, 1, 2, 4, 0, 5, 2, 3, 2, 6, 5, 6, 2, },
		new byte[]{2, 5, 3, 4, 5, 2, 4, 1, 5, 4, 0, 1, },
		new byte[]{7, 5, 4, 7, 8, 5, 7, 1, 8, 2, 8, 1, 0, 6, 3, },
		new byte[]{4, 3, 2, 4, 2, 1, 1, 2, 0, },
		new byte[]{5, 3, 2, 5, 2, 0, 4, 1, 2, 1, 0, 2, },
		new byte[]{0, 4, 5, 0, 3, 4, 0, 1, 3, 3, 2, 4, },
		new byte[]{5, 6, 3, 5, 3, 2, 6, 1, 3, 4, 3, 0, 1, 0, 3, },
		new byte[]{3, 5, 6, 3, 6, 2, 5, 4, 6, 1, 6, 0, 4, 0, 6, },
		new byte[]{0, 5, 1, 4, 3, 2, },
		new byte[]{2, 4, 0, 2, 0, 3, 3, 0, 1, },
		new byte[]{2, 5, 1, 2, 1, 3, 0, 4, 1, 4, 3, 1, },
		new byte[]{2, 0, 1, 3, 2, 1, },
		new byte[]{0, 2, 1, },
		new byte[]{1, 2, 0, 2, 3, 0, },
		new byte[]{1, 0, 2, 1, 2, 4, 4, 2, 3, },
		new byte[]{0, 1, 3, 0, 3, 2, 2, 3, 4, },
		new byte[]{1, 0, 2, 3, 1, 2, },
		new byte[]{0, 1, 4, 0, 4, 3, 3, 4, 2, },
		new byte[]{3, 0, 4, 3, 4, 5, 1, 2, 4, 2, 5, 4, },
		new byte[]{0, 1, 3, 2, 0, 3, },
		new byte[]{1, 0, 2, },
		new byte[]{0, 1, 2, 0, 2, 4, 4, 2, 3, },
		new byte[]{2, 3, 1, 0, 2, 1, },
		new byte[]{2, 3, 4, 2, 4, 5, 0, 1, 4, 1, 5, 4, },
		new byte[]{0, 2, 1, },
		new byte[]{0, 1, 2, 3, 0, 2, },
		new byte[]{0, 2, 1, },
		new byte[]{0, 1, 2, },
		new byte[]{},
	};
	#endregion
}
