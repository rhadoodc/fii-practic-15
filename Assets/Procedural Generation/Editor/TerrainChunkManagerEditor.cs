﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(TerrainChunkManager))]
public class TerrainChunkManagerEditor : Editor
{
	public override void OnInspectorGUI()
	{
		var tgt = (TerrainChunkManager)target;

		var newSamplingSizeX = Math.Max(EditorGUILayout.FloatField("Sampling Size X", tgt.ChunkSamplingSizeX), 0.0f);
		var newSamplingSizeY = Math.Max(EditorGUILayout.FloatField("Sampling Size Y", tgt.ChunkSamplingSizeY), 0.0f);
		var newSamplingSizeZ = Math.Max(EditorGUILayout.FloatField("Sampling Size Z", tgt.ChunkSamplingSizeZ), 0.0f);

		var newGridSizeX = Math.Max(EditorGUILayout.IntField("Size X", tgt.ChunkSizeX), 0);
		var newGridSizeY = Math.Max(EditorGUILayout.IntField("Size Y", tgt.ChunkSizeY), 0);
		var newGridSizeZ = Math.Max(EditorGUILayout.IntField("Size Z", tgt.ChunkSizeZ), 0);

		var newGridResolution = Math.Max(EditorGUILayout.FloatField("Voxel Resolution", tgt.VoxelRadius), 0.125f);

		if (tgt.ChunkSamplingSizeX != newSamplingSizeX)
		{
			tgt.ChunkSamplingSizeX = newSamplingSizeX;
			tgt.AnalysisRequired = true;
		}

		if (tgt.ChunkSamplingSizeY != newSamplingSizeY)
		{
			tgt.ChunkSamplingSizeY = newSamplingSizeY;
			tgt.AnalysisRequired = true;
		}

		if (tgt.ChunkSamplingSizeZ != newSamplingSizeZ)
		{
			tgt.ChunkSamplingSizeZ = newSamplingSizeZ;
			tgt.AnalysisRequired = true;
		}

		if (tgt.ChunkSizeX != newGridSizeX)
		{
			tgt.ChunkSizeX = newGridSizeX;
			tgt.AnalysisRequired = true;
		}

		if (tgt.ChunkSizeY != newGridSizeY)
		{
			tgt.ChunkSizeY = newGridSizeY;
			tgt.AnalysisRequired = true;
		}

		if (tgt.ChunkSizeZ != newGridSizeZ)
		{
			tgt.ChunkSizeZ = newGridSizeZ;
			tgt.AnalysisRequired = true;
		}

		if (tgt.VoxelRadius != newGridResolution)
		{
			tgt.VoxelRadius = newGridResolution;
			tgt.AnalysisRequired = true;
		}
		
		tgt.material = (Material)EditorGUILayout.ObjectField("Material", tgt.material, typeof(Material));
		
		tgt.AdaptiveSampling = EditorGUILayout.Toggle("Adaptive Sampling", tgt.AdaptiveSampling);
		tgt.SingleSideNormals = EditorGUILayout.Toggle("Single Sided Normals", tgt.SingleSideNormals);
		tgt.GenerateCollider = EditorGUILayout.Toggle("Generate Collider", tgt.GenerateCollider);
		tgt.DrawGizmos = EditorGUILayout.Toggle("Draw Gizmos", tgt.DrawGizmos);
	}

	void OnSceneGUI()
	{
		var mousePos = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 100.0f);
		var ray = Camera.current.ScreenPointToRay(mousePos);
		var hit = HandleUtility.RaySnap(ray);

		if (hit != null)
		{
			var normal = ((RaycastHit)hit).normal;
			var pos = ((RaycastHit)hit).point;
			
			var col = Handles.color;
			Handles.color = Color.green;
			
			Handles.DrawLine(pos, pos + normal);
			
			Handles.color = col;
		}
	}
}
