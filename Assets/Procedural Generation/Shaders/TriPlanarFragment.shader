﻿Shader "Custom/TriPlanarFragment" {
	Properties {
		colorTex1 ("X Texture ", 2D) = "green" {}
		colorTex2 ("Y Texture ", 2D) = "white" {}
		colorTex3 ("Z Texture ", 2D) = "red" {}
		
		bumpTex1 ("X Bump ", 2D) = "white" {}
		bumpTex2 ("T Bump ", 2D) = "white" {}
		bumpTex3 ("Z Bump ", 2D) = "white"  {}
				
		tex_scale ("Texture Scale", Range (0.0001,2)) = 0.001
	}
	
	SubShader {
		Pass {
			CGPROGRAM

			#pragma vertex vsMain
			#pragma fragment fsMain
			
			#include "UnityCG.cginc"
			
			sampler2D colorTex1;
			sampler2D colorTex2;
			sampler2D colorTex3;
			
			sampler2D bumpTex1;
			sampler2D bumpTex2;
			sampler2D bumpTex3;
			
			float tex_scale;
			
			struct VS_OUTPUT 
			{
				float4 pos : POSITION;
				float4 wsCoord : TEXCOORD0;
				float3 vNormal : TEXCOORD1;
			};

			VS_OUTPUT vsMain(in appdata_base v) 
			{			
				VS_OUTPUT o;	
				
				o.wsCoord = v.vertex; //mul(UNITY_MATRIX_MVP, v.vertex);
				
				o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
    
			    o.vNormal = v.normal;//normalize(mul(float4(v.normal, 0.0), _World2Object).xyz);
			    
			    return o;
			}			

			float4 fsMain(VS_OUTPUT v2f) : COLOR 
			{		
				//return v2f.vNormal.xyzx;

				float3 N_orig = v2f.vNormal;
				// Determine the blend weights for the 3 planar projections.  
				// N_orig is the vertex-interpolated normal vector.  
				float3 blend_weights = abs( N_orig.xyz );   // Tighten up the blending zone:  
				blend_weights = (blend_weights - 0.2) * 7;  
				blend_weights = max(blend_weights, 0);      // Force weights to sum to 1.0 (very important!)  
				blend_weights /= (blend_weights.x + blend_weights.y + blend_weights.z ).xxx;   
				// Now determine a color value and bump vector for each of the 3  
				// projections, blend them, and store blended results in these two  
				// vectors:  
				float4 blended_color; // .w hold spec value  
				float3 blended_bump_vec;  
				{  
				// Compute the UV coords for each of the 3 planar projections.  
				// tex_scale (default ~ 1.0) determines how big the textures appear.  
				float2 coord1 = v2f.wsCoord.yz * tex_scale;  
				float2 coord2 = v2f.wsCoord.zx * tex_scale;  
				float2 coord3 = v2f.wsCoord.xy * tex_scale;  
				// This is where you would apply conditional displacement mapping.  
				//if (blend_weights.x > 0) coord1 = . . .  
				//if (blend_weights.y > 0) coord2 = . . .  
				//if (blend_weights.z > 0) coord3 = . . .  
				// Sample color maps for each projection, at those UV coords.  
				float4 col1 = tex2D(colorTex1, coord1);  
				float4 col2 = tex2D(colorTex2, coord2);  
				float4 col3 = tex2D(colorTex3, coord3);  
				// Sample bump maps too, and generate bump vectors.  
				// (Note: this uses an oversimplified tangent basis.)  
				float2 bumpFetch1 = tex2D(bumpTex1, coord1).xy - 0.5;  
				float2 bumpFetch2 = tex2D(bumpTex2, coord2).xy - 0.5;  
				float2 bumpFetch3 = tex2D(bumpTex3, coord3).xy - 0.5;  
				float3 bump1 = float3(0, bumpFetch1.x, bumpFetch1.y);  
				float3 bump2 = float3(bumpFetch2.y, 0, bumpFetch2.x);  
				float3 bump3 = float3(bumpFetch3.x, bumpFetch3.y, 0);  
				// Finally, blend the results of the 3 planar projections.  
				blended_color = col1.xyzw * blend_weights.xxxx +  
									col2.xyzw * blend_weights.yyyy +  
									col3.xyzw * blend_weights.zzzz;  
				blended_bump_vec = bump1.xyz * blend_weights.xxx +  
									bump2.xyz * blend_weights.yyy +  
									bump3.xyz * blend_weights.zzz;  
				}  
				// Apply bump vector to vertex-interpolated normal vector.  
				float3 N_for_lighting = normalize(N_orig + blended_bump_vec); 
				
				return blended_color;
			}

			ENDCG
		}
	}
}
