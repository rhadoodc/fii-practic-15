﻿Shader "Neuro/CompositeSobel" {
Properties {
    _MainTex ("Main Texture", 2D) = "white" {}
}
SubShader {
    ZTest Always Cull Off ZWrite Off
	Fog { Mode off }
    
    Pass {
CGPROGRAM
#pragma vertex vert
#pragma fragment frag

#include "UnityCG.cginc"

sampler2D _MainTex;
sampler2D _Overlay;

struct a2f 
{
	float4 vertex : POSITION; 
	float2 uv_main : TEXCOORD0;
};

struct v2f 
{
	float4 vertex : SV_POSITION;
	float2 uv_main : TEXCOORD0;
};

float4 _MainTex_ST;

v2f vert (a2f v)
{
    v2f o;
    o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
    o.uv_main = TRANSFORM_TEX(v.uv_main, _MainTex);

    return o;
}

half4 frag( v2f i ) : COLOR
{
	
	fixed4 final = tex2D(_MainTex, i.uv_main);
//	#if UNITY_UV_STARTS_AT_TOP
//		i.uv_main.y = 1 - i.uv_main.y;
//	#endif
	fixed4 overlay = tex2D(_Overlay, i.uv_main);
//	return overlay; 

	final = final - tex2D(_Overlay, i.uv_main);
	
	return final;
}


ENDCG
    }
} 
}