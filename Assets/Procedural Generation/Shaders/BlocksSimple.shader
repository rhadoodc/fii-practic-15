﻿Shader "Neuro/Blocks" {
Properties {
    _MainTex ("Main Texture", 2D) = "white" {}
    _NormalMap ("Normal Map", 2D) = "bump" {}
    
    _EmissiveIntensity ("Emissive intensity", Float) = 0.5
    
    _Specular ("Specular", Float) = 1.0
    
    _Glossiness ("Max Glossiness", Float) = 10
    
    
}
SubShader {
	LOD 400
    Tags { "LightMode" = "ForwardBase" "RenderType" = "NormalMap" }
    Pass {
CGPROGRAM

#pragma exclude_renderers d3d11 xbox360 ps3 flash glesdesktop
#pragma glsl 
#pragma glsl_no_auto_normalization 
#pragma fragmentoption ARB_precision_hint_fastest

#pragma vertex vert
#pragma fragment frag

#include "UnityCG.cginc"

uniform sampler2D _MainTex;
uniform sampler2D _NormalMap;

uniform fixed4 _LightColor0;

uniform half _EmissiveIntensity;

uniform half _Specular;

uniform half _Glossiness;

float4x4 _CameraViewMatrix;

struct a2f 
{
	float4 vertex : POSITION;  
	float3 normal : NORMAL;
	float4 tangent : TANGENT;
	float2 uv_main : TEXCOORD0;
};

struct v2f 
{
	float4 vertex : SV_POSITION;
	
	float3 uv_main : TEXCOORD0;
	float3 ambient : TEXCOORD1;
	fixed3 worldLightDir : TEXCOORD2;
	fixed3 worldNormal : TEXCOORD3;
};

float4 _MainTex_ST;

v2f vert ( a2f v )
{
    v2f o;
    o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
    
    o.worldNormal = normalize(mul(float4(v.normal, 0.0), _World2Object).xyz);
   
    o.worldLightDir = _WorldSpaceLightPos0.xyz;
    o.uv_main.xy = TRANSFORM_TEX(v.uv_main, _MainTex);
    
    o.uv_main.z = -(mul( UNITY_MATRIX_MV, v.vertex ).z * _ProjectionParams.w);
//	float Fcoef = 2.0 / log2(_ProjectionParams.z + 1.0);
//	o.uv_main.z = log2(max(1e-6, 1.0 + o.vertex.w)) * Fcoef - 1.0;
    
    return o;
}

struct f2a 
{
	float4 Colour0 : COLOR0;
	float4 Colour1 : COLOR1;
	float4 Colour2 : COLOR2;
};

inline float2 EncodeFloatRGCustom( float v )
{
	float2 kEncodeMul = float2(1.0, 256.0);
	float kEncodeBit = 1.0/256.0;
	float2 enc = kEncodeMul * v;
	enc.y = frac (enc.y);
	enc.x -= enc.y * kEncodeBit;
	return enc;
}

f2a frag( v2f i )
//float4 frag( v2f i )
{
	fixed3 L = i.worldLightDir;
	fixed3 N = i.worldNormal;
	
	half NdotL = dot(N, L);
	half diff = 0.5 * NdotL + 0.5;
	diff = diff * diff;
	/*
	if (diff > 0.5) {
		diff = 0.7;
	} else {
		diff = 0.4;
	}
	*/
	fixed4 finalColor = _LightColor0 * (diff) * 2.0;
	finalColor.a = 1.0;
	
	
	f2a o;
	
	o.Colour0 = finalColor;
//	o.Colour1 = (float4(mul(_CameraViewMatrix,float4(N, 0.0)).xyz, 0.0) + 1.0) * 0.5; 
	
	float3 normal = mul(UNITY_MATRIX_V, float4(N, 0.0)).xyz;
//	o.Colour0 = float4(normal, 0.0);
//	float2 encodedNormal = EncodeViewNormalStereo(normal);
	
	o.Colour1 = float4(EncodeFloatRGCustom((normal.x + 1.0) * 0.5), EncodeFloatRGCustom((normal.y + 1.0) * 0.5));
//	o.Colour2 = i.uv_main.z;
//	float2 encodedOnce = EncodeFloatRG(i.uv_main.z);
	
	float maxBits = 256.0;
	
	float v = -log(1.0 - i.uv_main.z) / log(2000000.0);
//	float v = i.uv_main.z;
	
	float fv = frac(v);
	float fv256 = frac(v*maxBits);
	
	float y = frac(fv*maxBits - fv256);
	float x = frac(fv - fv256 / maxBits);// - y/maxBits;
	float w = frac(fv256 * maxBits);
	float z = fv256 - w/maxBits;
	
	
//	o.Colour2 = float4(EncodeFloatRG(encodedOnce.x),EncodeFloatRG(encodedOnce.y));
	o.Colour2 = float4(x, y, z, w);
	
	return o;
} 


ENDCG
    }
    
} 
	CustomEditor "CharacterMaterialEditor"
}