﻿Shader "Neuro/EdgeFilter" {
Properties {
    _DepthCutoff ("Depth Cutoff value", Range(0.01, 0.025)) = 0.1
	_ValleyCutoff ("Valley Cutoff", Range(0.0, 1.0)) = 0.5
	_CliffCutoff ("Cliff Cutoff", Range(0.0, 1.0)) = 0.5
	
	_DepthMultiplier ("Depth Multiplier", float) = 0.5
	_ValleyMultiplier ("Valley Multiplier", float) = 0.5
	_CliffMultiplier ("Cliff Multiplier", float) = 0.5
	
	_SamplingProfile ("Sampling Profile", float) = 2.0
}
SubShader {
    ZTest Always Cull Off ZWrite Off
	Fog { Mode off }
    
    Pass {
CGPROGRAM
#pragma vertex vert
#pragma fragment frag
#pragma exclude_renderers d3d11 xbox360 ps3 flash glesdesktop
#pragma glsl 
#pragma glsl_no_auto_normalization 
#pragma fragmentoption ARB_precision_hint_fastest
#pragma target 3.0

#include "UnityCG.cginc"

float fWidth;
float fHeight;

float oneOverWidth;
float oneOverHeight;

float _DepthCutoff;

float _ValleyCutoff;
float _CliffCutoff;
	
float _DepthMultiplier;
	
float _ValleyMultiplier;
float _CliffMultiplier;

float _SamplingProfile;

sampler2D _CameraNormalBuffer;
sampler2D _CameraColorBuffer;
sampler2D _CameraDepthBuffer;

struct a2f 
{
	float4 vertex : POSITION; 
	float2 uv_main : TEXCOORD0;
};

struct v2f 
{
	float4 vertex : SV_POSITION;
	float2 uv_main : TEXCOORD0;
	
	float4 deltaX4 : TEXCOORD1;
	float4 deltaY4 : TEXCOORD2;
};

v2f vert (a2f v)
{
    v2f o;
    o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
    o.uv_main = v.uv_main;

	 if (_SamplingProfile >= 4) {
	 	o.deltaX4 = float4(0.5, 0.5, -1.5, -1.5);
	 	o.deltaY4 = float4(-0.5, -0.5, -1.5, 1.5);
	 }
	 else if (_SamplingProfile >= 3) {
	 	o.deltaX4 = float4(1.5, 1.5, -1.5, -1.5);
	 	o.deltaY4 = float4(1.5, -1.5, -1.5, 1.5);
	 }
	 else if (_SamplingProfile >= 2) {
	 	o.deltaX4 = float4(0.5, 1.5, -0.5, -1.5);
	 	o.deltaY4 = float4(1.5, 0.5, -1.5, -0.5);
	 }
	 else if (_SamplingProfile >= 1) {
	 	o.deltaX4 = float4(0.5, 1.0, -0.5, -1.0);
	 	o.deltaY4 = float4(1.0, -0.5, -1.0, -0.5);
	 }
	 else {
	 	o.deltaX4 = float4(0.0, 1.0, 0.0, -1.0);
	 	o.deltaY4 = float4(1.0, 0.0, -1.0, 0.0);
	 }

    return o;
}




float fXIndex;
float fYIndex;

float3 currentNormal;
float currentDepth;

float4 buffer0;
float4 buffer1;
float4 buffer2;
float4 buffer3;

float4 depthTex0;
float4 depthTex1;
float4 depthTex2;
float4 depthTex3;

float3 normal0;
float3 normal1;
float3 normal2;
float3 normal3;

float3 crossVal0;
float3 crossVal1;
float3 crossVal2;
float3 crossVal3;

float4 depth;

float4 mask;
float4 unit;

inline float DecodeDepth( float4 encoded )
{
	return encoded.x + (encoded.y + encoded.z + encoded.w * 0.00390625) * 0.00390625; // /256.0
}

inline float DecodeFloatRGCustom( float2 enc )
{
	float2 kDecodeDot = float2(1.0, 1/256.0);
	return dot( enc, kDecodeDot );
}

inline void DecodeDepthNormalCustom (float4 normalTex, float4 depthTex, out float depth, out float3 normal)
{
	depth = DecodeDepth(depthTex);
//	normal = normalTex.xyz * 2.0 - 1.0;
	normal = float3(DecodeFloatRGCustom(normalTex.xy) * 2.0 - 1.0, DecodeFloatRGCustom(normalTex.zw) * 2.0 - 1.0, 0.0);
	normal.z = sqrt(1.0 - normal.x * normal.x - normal.y * normal.y);
}

float4 frag( v2f i ) : COLOR
{
    fXIndex = i.uv_main.x * fWidth;
    fYIndex = i.uv_main.y * fHeight;

	float diffDepth = 0.0;
	float cliffDiff = 0.0;
	float valleyDiff = 0.0;
			
	float4 fTempX4 = (float4(fXIndex) + i.deltaX4) * oneOverWidth;
	float4 fTempY4 = (float4(fYIndex) + i.deltaY4) * oneOverHeight;

	buffer0 = tex2D( _CameraNormalBuffer, float2( fTempX4.x, fTempY4.x ));
	buffer1 = tex2D( _CameraNormalBuffer, float2( fTempX4.y, fTempY4.y ));
	buffer2 = tex2D( _CameraNormalBuffer, float2( fTempX4.z, fTempY4.z ));
	buffer3 = tex2D( _CameraNormalBuffer, float2( fTempX4.w, fTempY4.w ));
	
	
	
	float4 currentNormalTex = tex2D( _CameraNormalBuffer, i.uv_main );
	float4 currentDepthTex = tex2D( _CameraDepthBuffer, i.uv_main );
	
//	return currentNormalTex;
	
	depthTex0 = tex2D(_CameraDepthBuffer, float2( fTempX4.x, fTempY4.x ));
	depthTex1 = tex2D(_CameraDepthBuffer, float2( fTempX4.y, fTempY4.y ));
	depthTex2 = tex2D(_CameraDepthBuffer, float2( fTempX4.z, fTempY4.z ));
	depthTex3 = tex2D(_CameraDepthBuffer, float2( fTempX4.w, fTempY4.w ));
	
	DecodeDepthNormalCustom(currentNormalTex, currentDepthTex, currentDepth, currentNormal);
	
//	return currentDepth * 10.0;
	
	DecodeDepthNormalCustom(buffer0, depthTex0, depth.x, normal0);
	DecodeDepthNormalCustom(buffer1, depthTex1, depth.y, normal1);
	DecodeDepthNormalCustom(buffer2, depthTex2, depth.z, normal2);
	DecodeDepthNormalCustom(buffer3, depthTex3, depth.w, normal3);
	
//	return float4(currentNormal + 0.5, 0.0);
	
//	return depth.w * 10.0;
	
//	return currentDepth;

//	return abs(currentDepth - depth.y) * 10.0;
	
	float4 diffDepth4 = abs(depth - currentDepth);
		
	crossVal0 = cross(currentNormal, normal0);
	crossVal1 = cross(currentNormal, normal1);
	crossVal2 = cross(currentNormal, normal2);
	crossVal3 = cross(currentNormal, normal3);
		
//	return diffDepth4.y * abs(crossVal1.x) * 10.0;
	
	
//	diffDepth = diffDepth4.x * crossVal0.x * crossVal0.x + 
//				diffDepth4.y * crossVal1.x * crossVal1.x + 
//				diffDepth4.z * crossVal2.x * crossVal2.x + 
//				diffDepth4.w * crossVal3.x * crossVal3.x;
	
	currentDepth = exp(currentDepth * log(2000000.0)) - 1.0;
	
	diffDepth4 /= currentDepth;
	diffDepth4 = (sign(diffDepth4 - _DepthCutoff) + 1.0) / 2.0;
	
	//diffDepth = diffDepth4.x + diffDepth4.y + diffDepth4.z + diffDepth4.w;
	
	
	
//	return currentDepth * 100.0;
	
//	if (diffDepth < 0.005) {
//		diffDepth = 0.0;
//	}
	
//	return diffDepth * 150.0;
	
	//1
	float4 crossVal = float4(crossVal0.x, crossVal1.y, crossVal2.x, crossVal3.y);
//	float4 crossVal = float4(currentNormal.y * normal0.z - currentNormal.z * normal0.y,
//	currentNormal.z * normal1.x - currentNormal.x * normal1.z,
//	currentNormal.y * normal2.z - currentNormal.z * normal2.y,
//	currentNormal.z * normal3.x - currentNormal.x * normal3.z);
//	
	cliffDiff = dot(crossVal * (sign(crossVal) + mask) / 2.0, unit);
	
	
	
//	cliffDiff += (crossVal0.x) * (sign(crossVal0.x) - 1.0) / 2.0;
//	cliffDiff += (crossVal1.y) * (sign(crossVal1.y) + 1.0) / 2.0;;
//	cliffDiff += (crossVal2.x) * (sign(crossVal2.x) + 1.0) / 2.0;
//	cliffDiff += (crossVal3.y) * (sign(crossVal3.y) - 1.0) / 2.0;
	
	valleyDiff = dot(crossVal * (sign(crossVal) - mask) / 2.0, unit);
	
//	valleyDiff += (crossVal0.x) * (sign(crossVal0.x) + 1.0) / 2.0;
//	valleyDiff += (crossVal1.y) * (sign(crossVal1.y) - 1.0) / 2.0;
//	valleyDiff += (crossVal2.x) * (sign(crossVal2.x) - 1.0) / 2.0;
//	valleyDiff += (crossVal3.y) * (sign(crossVal3.y) + 1.0) / 2.0;
	//2
//	if (crossVal1.y > 0) {
		
//	} else {
//	}

	//3
//	if (crossVal2.x >= 0) {
		
//	} else {
//	}
	
	//4
//	if (crossVal3.y <= 0) {
//	} else {
		
//	}

//	return cliffDiff + valleyDiff;

//return diffDepth4;
//	return float4(abs(buffer0.xyz), 0.0);
//	float val = dot(float4(abs(crossVal0) + abs(crossVal1) + abs(crossVal2) + abs(crossVal3), 0.0) + diffDepth4 * 10.0, unit);
//	if (val < _CliffCutoff) {
//		val = 0.0;
//	}
//	return val;

	float finalColor = 0.0;
	
	if (currentDepth < 0.01) {
		finalColor += (valleyDiff > _ValleyCutoff) * valleyDiff * _ValleyMultiplier;
		finalColor += (cliffDiff > _CliffCutoff) * cliffDiff * _CliffMultiplier;
	}
//	finalColor += diffDepth * 150.0;
	
//	diffDepth *= 1.0 - diffDepth;
	
//	return (_ProjectionParams.z - (diffDepth * _ProjectionParams.z)) / 5000.0;
	
	//if (diffDepth > _DepthCutoff) {
	if (dot(diffDepth4, unit) > 0)
	{
		finalColor = _DepthMultiplier;//diffDepth * 2000.0 * _DepthMultiplier;
	} else {
//		finalColor = 0.0;
	}
	
	return finalColor;
}


ENDCG
    }
} 
}