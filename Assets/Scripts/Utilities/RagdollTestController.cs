﻿using UnityEngine;
using System.Collections;

public class RagdollTestController : MonoBehaviour 
{
    public bool isRagdoll = false;

	// Use this for initialization
	void Start ()
    {
	
	}

	public void SetRagdollState(bool _isRagdoll)
    {
        GetComponent<Animator>().enabled = !_isRagdoll;
        
        var rigidBodiesStateChange = GetComponentInChildren<RigidbodiesStateChange>();
        rigidBodiesStateChange.SetCollidersEnableState(_isRagdoll);
        rigidBodiesStateChange.SetRigidbodiesKinematicState(!_isRagdoll);
    }

	// Update is called once per frame
	void Update () 
    {
	    if (Input.GetKeyDown(KeyCode.R))
        {
            Debug.Log("uhuu!");
            isRagdoll = !isRagdoll;
            SetRagdollState(isRagdoll);
        }
	}
}
