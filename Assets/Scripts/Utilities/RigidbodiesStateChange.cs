﻿using UnityEngine;
using System.Collections;

public class RigidbodiesStateChange : MonoBehaviour 
{
    public bool executeOnAwake = true;
    public bool allChildRigidbodiesKinematic = false;
    public bool allChildCollidersEnabled = true;

    protected Rigidbody[] cachedRigidbodies;
    protected Collider[] cachedColliders;

    void Awake()
    {
        if (!enabled)
            return;

        cachedRigidbodies = GetComponentsInChildren<Rigidbody>(true);
        cachedColliders = GetComponentsInChildren<Collider>(true);

        if (executeOnAwake)
        {
            SetRigidbodiesKinematicState(allChildRigidbodiesKinematic);
            SetCollidersEnableState(allChildCollidersEnabled);
        }
    }

	// Use this for initialization
	void Start () 
    {
	
	}

	public void SetRigidbodiesKinematicState(bool enable)
    {
        foreach (var cachedRigidbody in cachedRigidbodies)
        {
            cachedRigidbody.isKinematic = enable;
            cachedRigidbody.velocity = Vector3.zero;
            cachedRigidbody.angularVelocity = Vector3.zero;
        }
    }

    public void SetCollidersEnableState(bool enable)
    {
        foreach (var cachedCollider in cachedColliders)
            cachedCollider.enabled = enable;
    }
}
