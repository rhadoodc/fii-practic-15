﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class ApplyShaderToAllMaterials : MonoBehaviour 
{
    public float matCapPower = 2f;
    public Texture matCapTex;
    public Shader srcShader;
    public bool isOn;

    void Start()
    {
        if (!Application.isEditor)
            Component.Destroy(this);

    }

	// Use this for initialization
	void Update () 
    {
        if (isOn)
        {
            var renderers = GetComponentsInChildren<Renderer>(true);

            foreach (var currentRenderer in renderers)
            {
                Texture tex = currentRenderer.sharedMaterial.mainTexture;
                currentRenderer.sharedMaterial.shader = srcShader;
                currentRenderer.sharedMaterial.mainTexture = tex;
                currentRenderer.sharedMaterial.SetTexture("_MatCap", matCapTex);
                currentRenderer.sharedMaterial.SetFloat("_MatCapPower", matCapPower);

                isOn = false;
            }
        }
	}
	
}
