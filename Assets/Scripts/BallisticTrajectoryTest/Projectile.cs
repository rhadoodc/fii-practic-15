﻿using UnityEngine;
using System.Collections;

public class Projectile : MonoBehaviour 
{
    Rigidbody cachedRigidbody;

    public Transform target;
    private float distanceToTarget;
    public float fireAngle;
    
    public float projectileSpeed;
    public float x;
    public float y;
    public float g;

    void Awake()
    {
        gameObject.CheckAndInitializeWithInterface(ref cachedRigidbody);
    }

	// Use this for initialization
	void Start () 
    {
        projectileSpeed = CalculateProjectileInitialSpeed();
        cachedRigidbody.velocity = transform.forward * projectileSpeed;
	}

    public float CalculateProjectileInitialSpeed()
    {
        // Look at target only in the XZ plane.
        Vector3 targetPos = target.transform.position;
        targetPos.y = transform.position.y;
        transform.LookAt(targetPos);

        // Set fire angle (around X axis)
        transform.localEulerAngles = new Vector3(-fireAngle, transform.localEulerAngles.y, transform.localEulerAngles.z);

        distanceToTarget = (target.position - transform.position).magnitude;
        Vector3 targetDirection = (target.position - transform.position).normalized;

        x = distanceToTarget;
        y = target.position.y - transform.position.y;
        g = -Physics.gravity.y;

        //v = (sqrt(g) * sqrt(x) * sqrt((tan(fireAngle) * tan(fireAngle)) + 1)) / sqrt(2 * tan(fireAngle) - (2 * g * y) / x); // velocity
        float num = Mathf.Sqrt(2 * Mathf.Tan(fireAngle) - (2 * g * y) / x);
        float initialSpeed = (Mathf.Sqrt(g * x * Mathf.Tan(fireAngle) * Mathf.Tan(fireAngle)) + 1) / num;

        return initialSpeed;
    }

	// Update is called once per frame
	void Update () 
    {
	}
}
