﻿using UnityEngine;

[DisallowMultipleComponent]
public class PlayerInputProvider : MonoBehaviour, ICharacterInputProvider
{
	#region Const
	private const float buttonThreshold = 0.5f;
	private const float lightAttackThreshold = 0.1f;
	private const float heavyAttackThreshold = 0.8f;
	#endregion //Const

	#region Events
	public event InputActionCallback OnJump;
	public event InputActionCallback OnLightAttack;
	public event InputActionCallback OnHeavyAttack;
	#endregion //Events

	public string ForwardAxis;
	public string RightAxis;

	public string JumpAxis;
	public string LightAttackAxis;
	public string HeavyAttackAxis;

	private Vector3 inputVelocity = Vector3.zero;

	private bool jump = false;
	private bool lightAttack = false;
	private bool heavyAttack = false;

	#region ICharacterInputProvider
	public Vector3 GetCharacterInputVelocity()
	{
		return inputVelocity;
	}

	public bool GetJumpState()
	{
		return jump;
	}

	public bool GetLightAttackState()
	{
		return lightAttack;
	}

	public bool GetHeavyAttackState()
	{
		return heavyAttack;
	}
	#endregion //ICharacterInputProvider

	#region MonoBehaviour
	public void Update()
	{
		inputVelocity.z = Input.GetAxis(ForwardAxis);
		inputVelocity.x = Input.GetAxis(RightAxis);

		float jumpAxisValue = Input.GetAxis(JumpAxis);

		//light and heavy attacks can be bound to the same axis with different thresholds
		float lightAttackAxisValue = Input.GetAxis(LightAttackAxis);
		float heavyAttackAxisValue = Input.GetAxis(HeavyAttackAxis);

		//the order in raising the events will determine priority when handled
		if (!heavyAttack && (heavyAttackAxisValue > heavyAttackThreshold))
		{
			RaiseHeavyAttackCallback();
		}

		if (!lightAttack && (lightAttackAxisValue > lightAttackThreshold))
		{
			RaiseLightAttackCallback();
		}

		if (!jump && (jumpAxisValue > buttonThreshold))
		{
			RaiseJumpCallback();
		}

		jump = (jumpAxisValue > buttonThreshold);
		lightAttack = (lightAttackAxisValue > lightAttackThreshold);
		heavyAttack = (heavyAttackAxisValue > heavyAttackThreshold);
	}
	#endregion //MonoBehaviour
	
	private void ResetInputState()
	{
		inputVelocity = Vector3.zero;

		jump = false;
		lightAttack = false;
		heavyAttack = false;
	}

	private void RaiseHeavyAttackCallback()
	{
		var eh = OnHeavyAttack;

		if (eh != null)
		{
			eh(this, null);
		}
	}

	private void RaiseLightAttackCallback()
	{
		var eh = OnLightAttack;

		if (eh != null)
		{
			eh(this, null);
		}
	}

	private void RaiseJumpCallback()
	{
		var eh = OnJump;

		if (eh != null)
		{
			eh(this, null);
		}
	}
}
