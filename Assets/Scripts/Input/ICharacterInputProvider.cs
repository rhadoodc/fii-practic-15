﻿using System;
using UnityEngine;

public interface ICharacterInputProvider
{
	Vector3 GetCharacterInputVelocity();

	bool GetJumpState();
	
	bool GetLightAttackState();
	
	bool GetHeavyAttackState();

	event InputActionCallback OnJump;
	event InputActionCallback OnLightAttack;
	event InputActionCallback OnHeavyAttack;
}
