﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public interface ICameraInputProvider
{
	/// <summary>
	/// Gets commanded translation in camera space. Relevant when the camera moves unrestrained, relative to itself (i.e. free-flight observer camera).
	/// </summary>
	/// <returns>A normalized Vector3 representing the camera local input direction.</returns>
	Vector3 GetCameraLocalTranslationVelocity();

	/// <summary>
	/// Gets commanded translation in world space. Relevant in scenarios where camera is restricted in its motions (i.e. 'top down' isometric perspective movement).
	/// </summary>
	/// <returns>A normalized Vector3 representing the world local input direction.</returns>
	Vector3 GetCameraWorldTranslationVelocity();
	
	/// <summary>
	/// Gets commanded orbit speed - which is to say movement on a sphere of constant radius relative to its target.
	/// </summary>
	/// <returns>A normalized Vector2 representing the input direction to be mapped onto the orbit.</returns>
	Vector2 GetCameraOrbitVelocity();
	
	/// <summary>
	/// Gets commanded rotation speed, useful for rolling, pitching or yawing relative to itself.
	/// </summary>
	/// <returns>A normalized Vector3 representing the input rotation speed around the corresponding axes.</returns>
	Vector3 GetCameraRotationVelocity();

	/// <summary>
	/// Gets commanded zoom speed. Useful for smooth increases/decreases of zoom.
	/// </summary>
	/// <returns>A float value, representing the input zoom speed.</returns>
	float GetCameraZoomVelocity();

	/// <summary>
	/// Gets commanded zoom increment. Useful for toggling zoom steps.
	/// </summary>
	/// <returns>An int value representing the input number of increments in zoom level.</returns>
	int GetCameraZoomIncrement();
}
