﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(Camera))]
public class MRTCamera : MonoBehaviour 
{
	public Material debugMaterial;
	public Material compositingMaterial;
	public Material uberShaderMaterial;

	private static List<Material> lightMaterials = new List<Material>();

	public Texture2D noiseTexture;

	public bool drawDebug = false;

	private RenderTexture[] mrtTex = new RenderTexture[4];
	private RenderBuffer[] mrtRB = new RenderBuffer[4];

	Camera cameraComponent;
	Camera lightPassCameraComponent;

	public void Awake()
	{
		lightMaterials.Clear();

		gameObject.CheckAndInitializeWithInterface(ref cameraComponent);

		cameraComponent.cullingMask = ~(1 << LayerMask.NameToLayer("DeferredLights"));

		var go = new GameObject("LightPassCamera");
		
		go.transform.parent = transform;

		lightPassCameraComponent = go.AddComponent<Camera>();

		lightPassCameraComponent.enabled = false;
	}

	public static void RegisterLightMaterialInstance(Material instance)
	{
		lightMaterials.Add(instance);
	}

	public void Start()
	{
		for (int i = 0; i < mrtTex.Length; i ++)
		{
			mrtTex[i] = new RenderTexture(Screen.width, Screen.height, 24, RenderTextureFormat.ARGBFloat, RenderTextureReadWrite.sRGB);
			mrtTex[i].Create();

			mrtRB[i] = mrtTex[i].colorBuffer;
		}
	}

	public void OnPreRender()
	{
		cameraComponent.SetTargetBuffers(mrtRB, mrtTex[0].depthBuffer);

		uberShaderMaterial.SetFloat("_ZNear", lightPassCameraComponent.nearClipPlane);
		uberShaderMaterial.SetFloat("_ZFar", lightPassCameraComponent.farClipPlane);

		var oldRT = RenderTexture.active;

		RenderTexture.active = mrtTex[0];
		GL.ClearWithSkybox(true, cameraComponent);

		RenderTexture.active = mrtTex[1];
		GL.Clear(false, true, Color.black);

		RenderTexture.active = mrtTex[2];
		GL.Clear(false, true, Color.white);

		RenderTexture.active = mrtTex[3];
		GL.Clear(false, true, Color.black);

		RenderTexture.active = oldRT;
	}

	public void OnPostRender()
	{
		lightPassCameraComponent.CopyFrom(cameraComponent);

		lightPassCameraComponent.cullingMask = 1 << LayerMask.NameToLayer("DeferredLights");
		lightPassCameraComponent.clearFlags = CameraClearFlags.SolidColor;
		lightPassCameraComponent.backgroundColor = new Color(0.0f, 0.0f, 0.0f, 0.0f);

		var vpInv = (lightPassCameraComponent.projectionMatrix * lightPassCameraComponent.worldToCameraMatrix).inverse;

		foreach (var lightMaterial in lightMaterials)
		{
			lightMaterial.SetTexture("_rt_in0", mrtTex[0]);
			lightMaterial.SetTexture("_rt_in1", mrtTex[1]);
			lightMaterial.SetTexture("_rt_in2", mrtTex[2]);
			lightMaterial.SetTexture("_rt_in_out", mrtTex[3]);

			lightMaterial.SetMatrix("_ViewProjectInverse", vpInv);

			lightMaterial.SetFloat("_ZNear", lightPassCameraComponent.nearClipPlane);
			lightMaterial.SetFloat("_ZFar", lightPassCameraComponent.farClipPlane);

			lightMaterial.SetVector("_CamPosition", transform.position);
		}

		lightPassCameraComponent.SetTargetBuffers(mrtRB[3], mrtTex[0].depthBuffer);

		lightPassCameraComponent.Render();

		if (drawDebug)
		{
			debugMaterial.SetTexture("_rt_out0", mrtTex[0]);
			debugMaterial.SetTexture("_rt_out1", mrtTex[1]);
			debugMaterial.SetTexture("_rt_out2", mrtTex[2]);
			debugMaterial.SetTexture("_rt_out3", mrtTex[3]);
			Graphics.Blit(null, null, debugMaterial, 0);
		}
		else
		{
			compositingMaterial.SetTexture("_rt_out0", mrtTex[0]);
			compositingMaterial.SetTexture("_rt_out1", mrtTex[1]);
			compositingMaterial.SetTexture("_rt_out2", mrtTex[2]);
			compositingMaterial.SetTexture("_rt_out3", mrtTex[3]);
			compositingMaterial.SetTexture("_Noise", noiseTexture);
			Graphics.Blit(null, null, compositingMaterial, 0);
		}
	}
}
