﻿using UnityEngine;
using SprocketTools.Base;

[RequireComponent(typeof(ObliqueFrustumCamera))]
public class SplitCam : MonoBehaviour
{
	public bool isMain = true;

	public Camera cameraComponent = null;
	public ObliqueFrustumCamera obliqueCamComponent = null;

	public float viewportMargin = 0.001f;

	protected Rect sharedViewport;
	protected SplitCam other = null;

	private float fullFov;

	public bool IsSplit
	{
		get
		{
			return other != null;
		}
	}

	public void Awake()
	{
		gameObject.CheckAndInitializeWithInterface(ref cameraComponent);
		gameObject.CheckAndInitializeWithInterface(ref obliqueCamComponent);
	}

	public void SetViewport(Rect target)
	{
		if (other != null)
		{
			float halfWidth = target.width * 0.5f;
			var myRect = new Rect(target.x, target.y, halfWidth - viewportMargin, target.height);
			var otherRect = new Rect(target.x + viewportMargin + halfWidth, target.y, halfWidth - viewportMargin, target.height);

			sharedViewport = target;
			other.sharedViewport = target;

			InternalSetViewport(myRect);
			other.InternalSetViewport(otherRect);
		}
		else
		{
			sharedViewport = target;

			InternalSetViewport(target);
		}

		obliqueCamComponent.isDirty = true;
	}

	public void Split()
	{
		if (other != null)
			return;

		//UGLY!
		KGameObjectSingleton<CameraDirector>.Instance.SuppressDirectedCameraRegistration = true;
		var go = (GameObject)GameObject.Instantiate(gameObject);
		KGameObjectSingleton<CameraDirector>.Instance.SuppressDirectedCameraRegistration = false;

		other = go.GetComponent<SplitCam>();
		other.other = this;

		other.isMain = false;

		SetViewport(cameraComponent.rect);

		obliqueCamComponent.HorizontalObliqueFactor = -1.0f;
		other.obliqueCamComponent.HorizontalObliqueFactor = 1.0f;
	}

	public void Join()
	{
		if (!isMain || (other == null))
			return;

		Destroy(other.gameObject);
		other = null;

		//obliqueComponent.fieldOfView = fullFov;

		obliqueCamComponent.HorizontalObliqueFactor = 0.0f;

		InternalSetViewport(sharedViewport);
	}

	public void Update()
	{
		if (Input.GetKeyDown(KeyCode.P) && isMain)
		{
			if (other == null)
			{
				Split();
			}
			else
			{
				Join();
			}
		}
	}
	protected void InternalSetViewport(Rect target)
	{
		cameraComponent.rect = target;
	}
}
