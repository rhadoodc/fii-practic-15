﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class SplittableCamera : DirectedCamera
{
	public ObliqueFrustumCamera obliqueCamComponent = null;
	public SplitCam splitCamComponent = null;

	public virtual void Awake()
	{
		base.Awake();

		gameObject.CheckAndInitializeWithInterface(ref splitCamComponent);
		gameObject.CheckAndInitializeWithInterface(ref obliqueCamComponent);
	}

	public void SetViewport(Rect target)
	{
		if (splitCamComponent != null)
		{
			splitCamComponent.SetViewport(target);
		}
		else if (obliqueCamComponent != null)
		{
			obliqueCamComponent.SetViewport(target);
		}
		else
		{
			cameraComponent.rect = target;
		}
	}
}
