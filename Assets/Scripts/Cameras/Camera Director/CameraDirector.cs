﻿using SprocketTools.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[DisallowMultipleComponent]
public class CameraDirector : KMonoBehaviour
{
	public enum EMainViewportPosition
	{
		Full = 0,
		Left = 1,
		Right = 2,
		Bottom = 3,
		Top = 4,
	}

	public GUISkin uiSkin;
	public bool renderCameraNames = true;

	private Dictionary<string, DirectedCamera> directedCameras = null;
	private Rect[] viewports = null;
	private string[] viewportNameMapping = null;

	private static AViewportLayoutAlgorithm[] layoutAlgorithms;

	[SerializeField]
	private DirectedCamera currentMainCamera = null;

	[SerializeField]
	private EMainViewportPosition mainViewportPosition = EMainViewportPosition.Right;

	[SerializeField]
	private float viewportVerticalMarginRatio = 0.01f;

	public EMainViewportPosition OffViewportsPosition
	{
		get
		{
			return mainViewportPosition;
		}

		set
		{
			if (mainViewportPosition != value)
			{
				mainViewportPosition = value;

				CalculateViewports();
				AssignViewports();
			}
		}
	}

	public float ViewportVerticalMarginRatio
	{
		get
		{
			return viewportVerticalMarginRatio;
		}

		set
		{
			if (viewportVerticalMarginRatio != value)
			{
				viewportVerticalMarginRatio = value;

				CalculateViewports();
				AssignViewports();
			}
		}
	}

	public DirectedCamera CurrentMainCamera
	{
		get
		{
			return currentMainCamera;
		}

		set
		{
			if (currentMainCamera != value)
			{
				currentMainCamera = value;

				CalculateViewports();
				AssignViewports();
			}
		}
	}

	public bool SuppressDirectedCameraRegistration
	{ get; set; }

	public string CurrentMainCameraName
	{
		get
		{
			return currentMainCamera.CameraName;
		}

		set
		{
			if (currentMainCamera.CameraName != value)
			{
				if (directedCameras.ContainsKey(value))
				{
					currentMainCamera = directedCameras[value];

					CalculateViewports();
					AssignViewports();
				}
			}
		}
	}

	private Rect MainViewport
	{
		get
		{
			return viewports.Last();
		}
	}

	static CameraDirector()
	{
		InitializeViewportLayoutAlgorithms();
	}

	public string[] GetCameraNames()
	{
		return directedCameras != null ? directedCameras.Keys.ToArray() : null;
	}

	public void RegisterDirectedCamera(DirectedCamera target, string name)
	{
		if (SuppressDirectedCameraRegistration)
			return;

		if (directedCameras == null)
		{
			directedCameras = new Dictionary<string, DirectedCamera>();
		}

		if (!directedCameras.ContainsKey(name) && (!(target is SplittableCamera) || ((target is SplittableCamera) && (((target as SplittableCamera).splitCamComponent == null) || (!(target as SplittableCamera).splitCamComponent.IsSplit)))))
			directedCameras.Add(name, target);
	}

	public void Start()
	{
		if (CurrentMainCamera == null)
		{
			CurrentMainCamera = directedCameras.First().Value;
		}
		else
		{
			CalculateViewports();
			AssignViewports();
		}
	}

	public void OnGUI()
	{
		GUI.skin = uiSkin;

		if (viewports == null)
		{
			return;
		}

		GUI.backgroundColor = Color.clear;

		for (int i = 0; i < viewports.Count(); i++)
		{
			Rect viewport = viewports[i];

			var buttonRect = new Rect(viewport.x * Screen.width, (1.0f - viewport.y - viewport.height) * Screen.height, viewport.width * Screen.width, viewport.height * Screen.height);

			if (GUI.Button(buttonRect, renderCameraNames ? viewportNameMapping[i] : ""))
			{
				CurrentMainCameraName = viewportNameMapping[i];
			}
		}

		GUI.skin = null;
	}

	private static void InitializeViewportLayoutAlgorithms()
	{
		layoutAlgorithms = new AViewportLayoutAlgorithm[Enum.GetValues(typeof(EMainViewportPosition)).Length];
		layoutAlgorithms[(int)EMainViewportPosition.Full] = new FullViewportLayoutAlgorithm();
		layoutAlgorithms[(int)EMainViewportPosition.Left] = new LeftViewportLayoutAlgorithm();
		layoutAlgorithms[(int)EMainViewportPosition.Right] = new RightViewportLayoutAlgorithm();
		layoutAlgorithms[(int)EMainViewportPosition.Top] = new TopViewportLayoutAlgorithm();
		layoutAlgorithms[(int)EMainViewportPosition.Bottom] = new BottomViewportLayoutAlgorithm();
	}

	private void CalculateViewports()
	{
		if (directedCameras == null)
			return;

		layoutAlgorithms[(int)mainViewportPosition].CalculateViewports(ref viewports, ref viewportNameMapping, directedCameras.Count, viewportVerticalMarginRatio);
	}

	private void AssignViewports()
	{
		if (directedCameras == null)
			return;

		int i = 0;

		foreach (var camera in directedCameras.Values)
		{
			if (camera == currentMainCamera)
			{
				camera.SetViewport(MainViewport);
				viewportNameMapping[viewports.Count() - 1] = camera.CameraName;
			}
			else if (mainViewportPosition != EMainViewportPosition.Full)
			{
				viewportNameMapping[i] = camera.CameraName;
				camera.SetViewport(viewports[i++]);
			}
		}
	}
}
