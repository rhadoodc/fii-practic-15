﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public abstract class AViewportLayoutAlgorithm
{
	#region Const
	public const float _4x3AspectRatio = 4.0f / 3.0f;
	public const float _16x9AspectRatio = 16.0f / 9.0f;
	public const float _16x10AspectRatio = 16.0f / 10.0f;
	public const float _21x9AspectRatio = 21.0f / 9.0f;
	#endregion //Const

	//TODO: Move this out to some utility class?
	public static Vector2 GetMainGameViewSize()
	{
		System.Type T = System.Type.GetType("UnityEditor.GameView,UnityEditor");
		if (T != null)
		{
			System.Reflection.MethodInfo GetSizeOfMainGameView = T.GetMethod("GetSizeOfMainGameView", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static);
			System.Object Res = GetSizeOfMainGameView.Invoke(null, null);
			return (Vector2)Res;
		}
		else
		{
			return new Vector2(Screen.width, Screen.height);
		}
	}

	protected Vector2 ScreenSize;

	protected float screenAspectRatio;
	protected float offViewportAspectRatio;
	protected float viewportHorizontalMarginRatio;

	protected float offViewportWidth;
	protected float offViewportHeight;

	protected int numOffViewports;

	protected const int minOffViewports = 3;

	public virtual void CalculateViewports(ref Rect[] viewports, ref string[] viewportNameMapping, int numViewports, float viewportVerticalMarginRatio)
	{
		ScreenSize = GetMainGameViewSize();

		screenAspectRatio = ScreenSize.x / ScreenSize.y;
		offViewportAspectRatio = _4x3AspectRatio;

		viewportHorizontalMarginRatio = viewportVerticalMarginRatio / screenAspectRatio;

		numOffViewports = numViewports - 1;

		viewports = new Rect[numViewports];
		viewportNameMapping = new string[numViewports];
	}
}
