﻿using UnityEngine;

public class LeftViewportLayoutAlgorithm : AHorizontalViewportLayoutAlgorithm
{
	public override void CalculateViewports(ref Rect[] viewports, ref string[] viewportNameMapping, int numViewports, float viewportVerticalMarginRatio)
	{
		base.CalculateViewports(ref viewports, ref viewportNameMapping, numViewports, viewportVerticalMarginRatio);

		for (int i = 0; i < numOffViewports; i++)
		{
			viewports[i] = new Rect(1.0f - offViewportWidth - viewportHorizontalMarginRatio, i * offViewportHeight + viewportVerticalMarginRatio * (i + 1), offViewportWidth, offViewportHeight);
		}

		//TODO: get closest matching aspect ratio for main viewport
		viewports[numOffViewports] = new Rect(0.0f, 0.0f, 1.0f - (offViewportWidth + 2 * viewportHorizontalMarginRatio), 1.0f);
	}
}
