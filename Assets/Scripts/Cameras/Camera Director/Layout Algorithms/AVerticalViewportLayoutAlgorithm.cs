﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public abstract class AVerticalViewportLayoutAlgorithm : AViewportLayoutAlgorithm
{
	public override void CalculateViewports(ref Rect[] viewports, ref string[] viewportNameMapping, int numViewports, float viewportVerticalMarginRatio)
	{
		base.CalculateViewports(ref viewports, ref viewportNameMapping, numViewports, viewportVerticalMarginRatio);

		offViewportWidth = (numOffViewports != 0) ? (1.0f - viewportHorizontalMarginRatio) / Mathf.Max(minOffViewports, numOffViewports) - viewportHorizontalMarginRatio : 0.0f;
		offViewportHeight = offViewportWidth * offViewportAspectRatio;
	}
}
