﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class FullViewportLayoutAlgorithm : AViewportLayoutAlgorithm
{
	public override void CalculateViewports(ref Rect[] viewports, ref string[] viewportNameMapping, int numViewports, float viewportVerticalMarginRatio)
	{
		viewports = new Rect[1];
		viewportNameMapping = new string[1];
		viewports[0] = new Rect(0.0f, 0.0f, 1.0f, 1.0f);
	}
}
