﻿using UnityEngine;

public class TopViewportLayoutAlgorithm : AVerticalViewportLayoutAlgorithm
{
	public override void CalculateViewports(ref Rect[] viewports, ref string[] viewportNameMapping, int numViewports, float viewportVerticalMarginRatio)
	{
		base.CalculateViewports(ref viewports, ref viewportNameMapping, numViewports, viewportVerticalMarginRatio);

		for (int i = 0; i < numOffViewports; i++)
		{
			viewports[i] = new Rect(i * offViewportWidth + viewportHorizontalMarginRatio * (i + 1), 0.0f + viewportVerticalMarginRatio, offViewportWidth, offViewportHeight);
		}

		//TODO: get closest matching aspect ratio for main viewport
		viewports[numOffViewports] = new Rect(0.0f, offViewportHeight + 2 * viewportVerticalMarginRatio, 1.0f, 1.0f - (offViewportHeight + 2 * viewportVerticalMarginRatio));
	}
}