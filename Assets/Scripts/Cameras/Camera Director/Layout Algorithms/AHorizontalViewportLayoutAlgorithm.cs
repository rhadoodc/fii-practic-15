﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class AHorizontalViewportLayoutAlgorithm : AViewportLayoutAlgorithm
{
	public override void CalculateViewports(ref Rect[] viewports, ref string[] viewportNameMapping, int numViewports, float viewportVerticalMarginRatio)
	{
		base.CalculateViewports(ref viewports, ref viewportNameMapping, numViewports, viewportVerticalMarginRatio);

		offViewportHeight = (numOffViewports != 0) ? (1.0f - viewportVerticalMarginRatio) / Mathf.Max(minOffViewports, numOffViewports) - viewportVerticalMarginRatio : 0.0f;
		offViewportWidth = offViewportHeight / offViewportAspectRatio;
	}
}
