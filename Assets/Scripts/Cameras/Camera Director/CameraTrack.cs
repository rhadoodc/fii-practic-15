﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class CameraTrack : MonoBehaviour
{
	public Vector3 endPointA;
	public Vector3 EndPointA
	{
		get
		{
			return transform.TransformPoint(endPointA);
		}

		set
		{
			endPointA = transform.InverseTransformPoint(value);
		}
	}

	public Vector3 controlPointA;
	public Vector3 ControlPointA
	{	
		get
		{
			return transform.TransformPoint(controlPointA);
		}

		set
		{
			controlPointA = transform.InverseTransformPoint(value);
		}
	}

	public Vector3 endPointB;
	public Vector3 EndPointB
	{
		get
		{
			return transform.TransformPoint(endPointB);
		}

		set
		{
			endPointB = transform.InverseTransformPoint(value);
		}
	}

	public Vector3 controlPointB;
	public Vector3 ControlPointB
	{
		get
		{
			return transform.TransformPoint(controlPointB);
		}

		set
		{
			controlPointB = transform.InverseTransformPoint(value);
		}
	}
}
