﻿using SprocketTools.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[DisallowMultipleComponent]
[RequireComponent(typeof(Camera))]
public class DirectedCamera : MonoBehaviour
{
	public Camera cameraComponent = null;
	public string cameraName = "";
	
	public string CameraName
	{
		get
		{
			return cameraName == "" ? gameObject.name : cameraName;
		}
		set
		{
			cameraName = value;
		}
	}

	public virtual void Awake()
	{
		gameObject.CheckAndInitializeWithInterface(ref cameraComponent);

		KGameObjectSingleton<CameraDirector>.Instance.RegisterDirectedCamera(this, CameraName);
	}

	public virtual void SetViewport(Rect target)
	{
		cameraComponent.rect = target;
	}
}
