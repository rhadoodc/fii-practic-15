﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[RequireComponent(typeof(ObliqueFrustumCamera))]
public class MultiTargetCamera : MonoBehaviour
{
	public ObliqueFrustumCamera obliqueComponent = null;
	public SplitCam splitComponent = null;
	public Camera cameraComponent = null;

	public CameraTarget[] targets;

	public float[] weights;

	public Vector3 offsetFromCentroid;

	public float panSpeed;

	public float minFov = 15.0f;
	public float minFovBoundsSize = 5.0f;
	public float maxFov = 90.0f;
	public float maxFovBoundsSize = 15.0f;

	public float joinDistance = 7.5f;
	public float preJoinDistance = 10.0f;

	public float splitDistance = 12.5f;
	public float postSplitDistance = 15.0f;

	public float maximumTolerance;

	public float maxFovTolerance;
	public float fovSpeed;

	private Vector3 targetLocation;

	private Vector3 startingLocation;

	private float panTransitionStartTime;

	private float panTransitionDuration;

	private bool hasPanTarget = false;
	private bool hasFOVTarget = false;

	private float fovTransitionStartTime;
	private float fovTransitionDuration;

	private float startingFOV;
	private float targetFOV;

	private double fdist;

	[SerializeField]
	private float sqrMaximumTolerance;

	private Bounds lastFrameBounds = new Bounds();

	public float MaximumTolerance
	{
		get
		{
			return maximumTolerance;
		}

		set
		{
			if (maximumTolerance != value)
			{
				maximumTolerance = value;
				sqrMaximumTolerance = maximumTolerance * maximumTolerance;
			}
		}
	}

	private Vector3 TargetLocation
	{
		get
		{
			return targetLocation;
		}

		set
		{
			if ((targetLocation - value).sqrMagnitude > sqrMaximumTolerance)
			{
				hasPanTarget = true;

				targetLocation = value;

				startingLocation = transform.position;

				panTransitionDuration = CalculatePanTransitionTime(startingLocation, targetLocation, panSpeed);

				panTransitionStartTime = Time.timeSinceLevelLoad;
			}
		}
	}

	private float TargetFOV
	{
		get
		{
			return targetFOV;
		}
		set
		{
			if (Mathf.Abs(targetFOV - value) > maxFovTolerance)
			{
				hasFOVTarget = true;

				targetFOV = value;

				startingFOV = cameraComponent.fieldOfView;

				fovTransitionDuration = CalculateFOVTransitionTime(startingFOV, targetFOV, fovSpeed);

				fovTransitionStartTime = Time.timeSinceLevelLoad;
			}
		}
	}

	public void Awake()
	{
		gameObject.CheckAndInitializeWithInterface(ref cameraComponent);
		gameObject.CheckAndInitializeWithInterface(ref obliqueComponent);
		gameObject.CheckAndInitializeWithInterface(ref splitComponent);
	}

	public Vector3 CalculateCameraPositionForAABB(Bounds aabb)
	{
		var targetDirection = aabb.extents.normalized;

		return targetDirection * 10.0f;
	}

	public void Update()
	{
		var bounds = CalculateCameraRelativeTargetAABB();

		//TargetLocation = bounds.center + offsetFromCentroid;

		TargetLocation = CalculateCameraPositionForAABB(bounds);

		if (hasPanTarget)
		{
			var factor = Mathf.Clamp01((Time.timeSinceLevelLoad - panTransitionStartTime) / panTransitionDuration);

			transform.position = targetLocation;//Vector3.Lerp(startingLocation, targetLocation, /*Mathf.SmoothStep(0.0f, 1.0f, factor)*/ factor);

			if (factor > 1.0f - 1e-5f)
			{
				hasPanTarget = false;
			}
		}

		TargetFOV = Mathf.LerpAngle(minFov, maxFov, Mathf.Clamp01(Mathf.InverseLerp(minFovBoundsSize, maxFovBoundsSize, bounds.size.magnitude)));

		if (hasFOVTarget)
		{
			var factor = Mathf.Clamp01((Time.timeSinceLevelLoad - fovTransitionStartTime) / fovTransitionDuration);

			cameraComponent.fieldOfView = targetFOV;//Mathf.Lerp(startingFOV, targetFOV, /*Mathf.SmoothStep(0.0f, 1.0f, factor)*/ factor);

			obliqueComponent.isDirty = true;

			if (factor > 1.0f - 1e-5f)
			{
				hasFOVTarget = false;
			}
		}

		lastFrameBounds = bounds;

		if (bounds.size.magnitude < joinDistance)
		{
			splitComponent.Join();
		}

		if (bounds.size.magnitude > splitDistance)
		{
			splitComponent.Split();
		}
	}

	public void OnDrawGizmos()
	{
		Color col = Gizmos.color;

		Gizmos.color = Color.magenta;

		Gizmos.DrawCube(transform.TransformPoint(lastFrameBounds.center), Vector3.one * 0.5f);

		Vector3 corner0 = transform.TransformPoint(lastFrameBounds.min);
		Vector3 corner1 = transform.TransformPoint(new Vector3(lastFrameBounds.min.x, lastFrameBounds.min.y, lastFrameBounds.max.z));
		Vector3 corner2 = transform.TransformPoint(new Vector3(lastFrameBounds.min.x, lastFrameBounds.max.y, lastFrameBounds.min.z));
		Vector3 corner3 = transform.TransformPoint(new Vector3(lastFrameBounds.max.x, lastFrameBounds.min.y, lastFrameBounds.min.z));
		Vector3 corner4 = transform.TransformPoint(lastFrameBounds.max);
		Vector3 corner5 = transform.TransformPoint(new Vector3(lastFrameBounds.max.x, lastFrameBounds.max.y, lastFrameBounds.min.z));
		Vector3 corner6 = transform.TransformPoint(new Vector3(lastFrameBounds.max.x, lastFrameBounds.min.y, lastFrameBounds.max.z));
		Vector3 corner7 = transform.TransformPoint(new Vector3(lastFrameBounds.min.x, lastFrameBounds.max.y, lastFrameBounds.max.z));

		Gizmos.DrawLine(corner0, corner1);
		Gizmos.DrawLine(corner0, corner2);
		Gizmos.DrawLine(corner0, corner3);
		
		Gizmos.DrawLine(corner2, corner7);
		Gizmos.DrawLine(corner2, corner5);

		Gizmos.DrawLine(corner4, corner7);
		Gizmos.DrawLine(corner4, corner5);
		Gizmos.DrawLine(corner4, corner6);

		Gizmos.DrawLine(corner7, corner1);
		
		Gizmos.DrawLine(corner6, corner3);
		Gizmos.DrawLine(corner5, corner3);
		Gizmos.DrawLine(corner1, corner6);

		Gizmos.color = col;
	}

	private static float CalculateFOVTransitionTime(float startingFOV, float targetFOV, float fovSpeed)
	{
		return Mathf.Abs(targetFOV - startingFOV) / fovSpeed;
	}

	private static float CalculatePanTransitionTime(Vector3 origin, Vector3 target, float panSpeed)
	{
		return (target - origin).magnitude / panSpeed;
	}

	private Bounds CalculateCameraRelativeTargetAABB()
	{
		Vector3 max = new Vector3(float.MinValue, float.MinValue, float.MinValue);
		Vector3 min = new Vector3(float.MaxValue, float.MaxValue, float.MaxValue);

		foreach (var target in targets)
		{
			var relativePosition = transform.InverseTransformPoint(target.transform.position);

			if (relativePosition.x > max.x)
				max.x = relativePosition.x;

			if (relativePosition.y > max.y)
				max.y = relativePosition.y;

			if (relativePosition.z > max.z)
				max.z = relativePosition.z;


			if (relativePosition.x < min.x)
				min.x = relativePosition.x;

			if (relativePosition.y < min.y)
				min.y = relativePosition.y;

			if (relativePosition.z < min.z)
				min.z = relativePosition.z;
		}

		Bounds resultBounds = new Bounds();
		resultBounds.SetMinMax(min, max);

		return resultBounds;
	}
}
