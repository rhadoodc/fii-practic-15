﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[DisallowMultipleComponent]
[RequireComponent(typeof(Camera))]
public class ObliqueFrustumCamera : MonoBehaviour
{
	public Camera cameraComponent = null;

	//public float aspectRatio = 1.0f;

	[SerializeField]
	private float horizontalObliqueFactor = 0.0f;
	
	[SerializeField]
	private float verticalObliqueFactor = 0.0f;

	public bool isDirty = true;

	public float HorizontalObliqueFactor
	{
		get
		{
			return horizontalObliqueFactor;
		}
		set
		{
			if (horizontalObliqueFactor != value)
			{
				horizontalObliqueFactor = value;

				gameObject.CheckAndInitializeWithInterface(ref cameraComponent);

				//var mat = cameraComponent.projectionMatrix;
				//mat[0, 2] = horizontalObliqueFactor;
				//cameraComponent.projectionMatrix = mat;

				//UpdateProjectionMatrix();
				isDirty = true;
			}
		}
	}

	public float VerticalObliqueFactor
	{
		get
		{
			return verticalObliqueFactor;
		}
		set
		{
			if (verticalObliqueFactor != value)
			{
				verticalObliqueFactor = value;

				gameObject.CheckAndInitializeWithInterface(ref cameraComponent);

				//var mat = cameraComponent.projectionMatrix;
				//mat[1, 2] = verticalObliqueFactor;
				//cameraComponent.projectionMatrix = mat;

				//UpdateProjectionMatrix();
				isDirty = true;
			}
		}
	}

	public void Awake()
	{
		gameObject.CheckAndInitializeWithInterface(ref cameraComponent);

		//var mat = cameraComponent.projectionMatrix;
		//mat[0, 2] = horizontalObliqueFactor;
		//mat[1, 2] = verticalObliqueFactor;
		//cameraComponent.projectionMatrix = mat;

		//UpdateProjectionMatrix();
	}

	public void LateUpdate()
	{
		if (isDirty)
		{
			isDirty = false;
			UpdateProjectionMatrix();
		}
	}

	public void UpdateProjectionMatrix()
	{
		cameraComponent.projectionMatrix = ComputeProjectionMatrix(cameraComponent.fieldOfView, cameraComponent.aspect, cameraComponent.nearClipPlane, cameraComponent.farClipPlane, horizontalObliqueFactor, verticalObliqueFactor, true);
	}

	private Matrix4x4 ComputeProjectionMatrix(float fov, float aspect, float nearDist, float farDist, float hOblique, float vOblique, bool leftHanded = true)
	{
		Matrix4x4 result = Matrix4x4.identity;
		//
		// General form of the Projection Matrix
		//
		// uh = Cot( fov/2 ) == 1/Tan(fov/2)
		// uw / uh = 1/aspect
		// 
		//   uw         0       0       0
		//    0        uh       0       0
		//    0         0      f/(f-n)  1
		//    0         0    -fn/(f-n)  0
		//
		// Make result to be identity first

		// check for bad parameters to avoid divide by zero:
		// if found, assert and return an identity matrix.
		if ( fov <= 0 || aspect == 0 )
		{
			return result;
		}

		float frustumDepth = farDist - nearDist;

		result[1, 1] = 1.0f / Mathf.Tan(0.5f * fov * Mathf.Deg2Rad);
		result[0, 0] = (leftHanded ? 1.0f : -1.0f ) * result[1, 1] / aspect;
		result[0, 2] = hOblique;
		result[1, 2] = vOblique;
		result[2, 2] = -(farDist + nearDist) / frustumDepth;
		result[2, 3] = -2.0f * farDist * nearDist / frustumDepth;
		result[3, 2] = -1.0f;
		result[3, 3] = 0.0f;

		return result;
	}

	public void SetViewport(Rect target)
	{
		cameraComponent.rect = target;

		isDirty = true;
	}
}
