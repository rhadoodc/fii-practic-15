﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[DisallowMultipleComponent]
public class CameraMouseInputProvider : MonoBehaviour, ICameraInputProvider
{

	#region ICameraInputProvider Members

	public Vector3 GetCameraLocalTranslationVelocity()
	{
		return Vector3.zero;
	}

	public Vector3 GetCameraWorldTranslationVelocity()
	{
		return Vector3.zero;
	}

	public Vector2 GetCameraOrbitVelocity()
	{
		return orbitVelocity;
	}

	public Vector3 GetCameraRotationVelocity()
	{
		return Vector3.zero;
	}

	public float GetCameraZoomVelocity()
	{
		return 0.0f;
	}

	public int GetCameraZoomIncrement()
	{
		return zoomIncrement;
	}

	#endregion

	private Vector2 orbitVelocity;

	private int zoomIncrement;

	public string orbitHAxis;
	public string orbitVAxis;
	public string zoomAxis;

	public void Update()
	{
		zoomIncrement = (zoomAxis != "") ? (int)Input.GetAxis(zoomAxis) : 0;
		orbitVelocity = new Vector2((orbitHAxis != "") ? Input.GetAxis(orbitHAxis) : 0.0f, (orbitVAxis != "") ? Input.GetAxis(orbitVAxis) : 0.0f);
	}
}
