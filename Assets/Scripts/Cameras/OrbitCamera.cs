﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[DisallowMultipleComponent]
[RequireComponent(typeof(ICameraInputProvider))]
[RequireComponent(typeof(VolumeBoundedFollowCamera))]
class OrbitCamera : MonoBehaviour
{
	public ICameraInputProvider inputProvider = null;
	public ITargetSmoothingAlgorithm targetSmoothingAlgorithm = null;

	public VolumeBoundedFollowCamera volumeBoundedCameraComponent;

	[Range(1.0f, 10.0f)]
	public float sensitivity = 0.0f;

	public void Awake()
	{
		gameObject.CheckAndInitializeWithInterface<ICameraInputProvider>(ref inputProvider);

		gameObject.CheckAndInitializeWithInterface<VolumeBoundedFollowCamera>(ref volumeBoundedCameraComponent);
	}

	public void Update()
	{
		var orbitVelocity = inputProvider.GetCameraOrbitVelocity();
		var offset = volumeBoundedCameraComponent.Offset;
		
		var radius = offset.magnitude;
		var theta = Mathf.Acos(offset.y / radius);
		var phi = Mathf.Atan2(offset.z, offset.x);

		theta += orbitVelocity.y * Mathf.PI * sensitivity * 1e-2f;
		phi += -orbitVelocity.x * Mathf.PI * sensitivity * 1e-2f;

		theta = Mathf.Clamp(theta, Mathf.PI / 12.0f, Mathf.PI * 11.0f/12.0f);

		var x = radius * Mathf.Sin(theta) * Mathf.Cos(phi);
		var z = radius * Mathf.Sin(theta) * Mathf.Sin(phi);
		var y = radius * Mathf.Cos(theta);

		volumeBoundedCameraComponent.Offset = new Vector3(x, y, z);
	}
}
