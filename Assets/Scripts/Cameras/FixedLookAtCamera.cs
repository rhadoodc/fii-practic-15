﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

[DisallowMultipleComponent]
[RequireComponent(typeof(ITargetSmoothingAlgorithm))]
public class FixedLookAtCamera : MonoBehaviour
{
	public ITargetSmoothingAlgorithm targetSmoothingAlgorithm = null;
	public float minXAngle;
	public float maxXAngle;
	public float minYAngle;
	public float maxYAngle;

	private Vector3 initialAngles;

	public bool applyAngleConstraints = false;

	public Vector3 InitialAngles
	{
		get { return !initialized ? transform.eulerAngles : initialAngles; }
	}

	public float minX
	{
		get { return (!initialized ? transform.eulerAngles.x : initialAngles.x) + minXAngle; }
	}

	public float maxX
	{
		get { return (!initialized ? transform.eulerAngles.x : initialAngles.x) + maxXAngle; }
	}

	public float minY
	{
		get { return (!initialized ? transform.eulerAngles.y : initialAngles.y) + minYAngle; }
	}

	public float maxY
	{
		get { return (!initialized ? transform.eulerAngles.y : initialAngles.y) + maxYAngle; }
	}

	[HideInInspector]
	public bool initialized = false;

	public void Awake()
	{
		gameObject.CheckAndInitializeWithInterface<ITargetSmoothingAlgorithm>(ref targetSmoothingAlgorithm);

		initialAngles = transform.eulerAngles;

		initialized = true;
	}

	public void Update()
	{
		transform.LookAt(targetSmoothingAlgorithm.SmoothedTargetPosition);

		if (applyAngleConstraints)
		{
			var angleX = transform.eulerAngles.x > 180.0f ? transform.eulerAngles.x - 360.0f : transform.eulerAngles.x;

			var angleY = transform.eulerAngles.y > 180.0f ? transform.eulerAngles.y - 360.0f : transform.eulerAngles.y;

			transform.eulerAngles = new Vector3(Mathf.Clamp(angleX, minX, maxX), Mathf.Clamp(angleY, minY, maxY), transform.eulerAngles.z);
		}
	}
}
