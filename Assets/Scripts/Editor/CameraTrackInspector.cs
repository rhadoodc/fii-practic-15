﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(CameraTrack))]
public class CameraTrackInspector : TypedTargetInspector<CameraTrack>
{
	public void OnSceneGUI()
	{
		Undo.RecordObject(typedTarget, "Modify Bezier");

		var col = Handles.color;

		Handles.color = Color.green;

		Handles.DrawBezier(typedTarget.EndPointA, typedTarget.EndPointB, typedTarget.ControlPointA, typedTarget.ControlPointB, Color.green, null, 1.0f);

		Handles.DrawLine(typedTarget.EndPointA, typedTarget.ControlPointA);
		Handles.DrawLine(typedTarget.EndPointB, typedTarget.ControlPointB);

		Handles.CubeCap(0, typedTarget.EndPointA, Quaternion.identity, 0.25f);
		Handles.CubeCap(0, typedTarget.EndPointB, Quaternion.identity, 0.25f);

		Handles.SphereCap(0, typedTarget.ControlPointA, Quaternion.identity, 0.25f);
		Handles.SphereCap(0, typedTarget.ControlPointB, Quaternion.identity, 0.25f);

		Vector3 newEndPointA = Handles.PositionHandle(typedTarget.EndPointA, Quaternion.identity);

		Vector3 deltaA = (newEndPointA - typedTarget.EndPointA);

		typedTarget.ControlPointA += deltaA;
		typedTarget.EndPointA += deltaA;

		typedTarget.ControlPointA = Handles.PositionHandle(typedTarget.ControlPointA, Quaternion.identity);

		Vector3 newEndPointB = Handles.PositionHandle(typedTarget.EndPointB, Quaternion.identity);


		Vector3 deltaB = (newEndPointB - typedTarget.EndPointB);

		typedTarget.ControlPointB += deltaB;
		typedTarget.EndPointB += deltaB;

		typedTarget.ControlPointB = Handles.PositionHandle(typedTarget.ControlPointB, Quaternion.identity);

		Handles.color = col;

		if (GUI.changed)
		{
			EditorUtility.SetDirty(typedTarget);
		}
	}

	protected override void OnHeaderGUI()
	{
		base.OnHeaderGUI();
	}

	public override void OnInspectorGUI()
	{
		//base.OnInspectorGUI();
		DrawDefaultInspector();
	}
}
