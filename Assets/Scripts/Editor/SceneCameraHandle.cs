﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[InitializeOnLoad]
public class SceneCameraHandle : Editor
{
	#region Constants

	private static readonly Vector3 defaultRotationVector = new Vector3(30.0f, 45.0f, 0.0f);

	private static readonly Quaternion handleRotation = Quaternion.Euler(90.0f, 0.0f, 0.0f);

	private static readonly Plane worldPlane = new Plane(Vector3.up, Vector3.zero);

	private const float minCameraZoom = 1.0f;
	private const float maxCameraZoom = 50.0f;
	private const float previewGizmoChangeLifetime = 1.5f;
	private const float mouseBoxScale = 0.1f;
	private const float gizmoBoxScale = 0.3f;
	private const float focusBoxScale = 0.4f;
	private const float safeBorder = 0.01f;
	private const float notFocusedTransparency = 0.5f;

	#endregion

	#region Fields

	private static double previewMaxMagnitudeGizmoChangesTimer = 0.0;
	private static double previewDeadZoneGizmoChangesTimer = 0.0;

	private static int controlID;

	private static bool draggingHandle = false;

	#endregion

	#region Properties

	private static Vector3 DefaultCameraPosition
	{
		// = Vector3.zero;
		get
		{
			Vector3 ret = Vector3.zero;

			if(EditorPrefs.HasKey(defaultPositionXPrefKey))
			{
				ret.x = EditorPrefs.GetFloat(defaultPositionXPrefKey);
				ret.y = EditorPrefs.GetFloat(defaultPositionYPrefKey);
				ret.z = EditorPrefs.GetFloat(defaultPositionZPrefKey);
			}

			return ret;
		}

		set
		{
			EditorPrefs.SetFloat(defaultPositionXPrefKey, value.x);
			EditorPrefs.SetFloat(defaultPositionYPrefKey, value.y);
			EditorPrefs.SetFloat(defaultPositionZPrefKey, value.z);
		}
	}

	private static Quaternion DefaultCameraRotation
	{
		//= Quaternion.Euler (30.0f, 45.0f, 0.0f);
		get
		{
			Vector3 ret = defaultRotationVector;

			if(EditorPrefs.HasKey(defaultRotationXAnglePrefKey))
			{
				ret.x = EditorPrefs.GetFloat(defaultRotationXAnglePrefKey);
				ret.y = EditorPrefs.GetFloat(defaultRotationYAnglePrefKey);
				ret.z = EditorPrefs.GetFloat(defaultRotationZAnglePrefKey);
			}

			return Quaternion.Euler(ret);
		}

		set
		{
			var ret = value.eulerAngles;
			EditorPrefs.SetFloat(defaultRotationXAnglePrefKey, ret.x);
			EditorPrefs.SetFloat(defaultRotationYAnglePrefKey, ret.y);
			EditorPrefs.SetFloat(defaultRotationZAnglePrefKey, ret.z);
		}
	}

	private static float DefaultCameraZoomSize
	{
		//= 15.0f;
		get
		{
			if(EditorPrefs.HasKey(defaultZoomSizePrefKey))
			{
				return EditorPrefs.GetFloat(defaultZoomSizePrefKey);
			}

			return 15.0f;
		}

		set
		{
			EditorPrefs.SetFloat(defaultZoomSizePrefKey, value);
		}
	}
	
	private static float MoveSpeed
	{
		//= 0.02f;
		get
		{
			if(EditorPrefs.HasKey(moveSpeedPrefKey))
			{
				return EditorPrefs.GetFloat(moveSpeedPrefKey);
			}

			return 0.02f;
		}

		set
		{
			EditorPrefs.SetFloat(moveSpeedPrefKey, value);
		}
	}

	private static float MaxAllowedMagnitude
	{
		//= 2.5f;
		get
		{
			if(EditorPrefs.HasKey(maxMagnitudePrefKey))
			{
				return EditorPrefs.GetFloat(maxMagnitudePrefKey);
			}

			return 2.5f;
		}

		set
		{
			EditorPrefs.SetFloat(maxMagnitudePrefKey, value);
		}
	}

	private static float DeadZone
	{
		//= 0.25f;
		get
		{
			if(EditorPrefs.HasKey(deadZonePrefKey))
			{
				return EditorPrefs.GetFloat(deadZonePrefKey);
			}

			return 0.25f;
		}

		set
		{
			EditorPrefs.SetFloat(deadZonePrefKey, value);
		}
	}

	private static bool ClipOnMouseOut
	{
		//= false;
		get
		{
			if(EditorPrefs.HasKey(clipOnMouseOutPrefKey))
			{
				return EditorPrefs.GetBool(clipOnMouseOutPrefKey);
			}

			return false;
		}

		set
		{
			EditorPrefs.SetBool(clipOnMouseOutPrefKey, value);
		}
	}

	private static bool PreviewCameraDefaultsChanges
	{
		//= true;
		get
		{
			if(EditorPrefs.HasKey(previewCamearDefaultsPrefKey))
			{
				return EditorPrefs.GetBool(previewCamearDefaultsPrefKey);
			}

			return true;
		}

		set
		{
			EditorPrefs.SetBool(previewCamearDefaultsPrefKey, value);
		}
	}

	private static bool IsAttached
	{
		get
		{
			if(EditorPrefs.HasKey(isEnabledPrefKey))
				return EditorPrefs.GetBool(isEnabledPrefKey);

			return false;
		}

		set
		{
			if(value == IsAttached)
				return;

			EditorPrefs.SetBool(isEnabledPrefKey, value);

			if(value)
				AttachHandlers();
			else
				DetachHandlers();
		}
	}

	#endregion

	#region Strings

	private const string editorPrefsKeyPrefix = "SceneCameraHandle_";
	private const string isEnabledPrefKey = editorPrefsKeyPrefix + "Enabled";

	private const string defaultRotationXAnglePrefKey = editorPrefsKeyPrefix + "defaultRotationXAngle";
	private const string defaultRotationYAnglePrefKey = editorPrefsKeyPrefix + "defaultRotationYAngle";
	private const string defaultRotationZAnglePrefKey = editorPrefsKeyPrefix + "defaultRotationZAngle";

	private const string defaultPositionXPrefKey = editorPrefsKeyPrefix + "defaultPositionX";
	private const string defaultPositionYPrefKey = editorPrefsKeyPrefix + "defaultPositionY";
	private const string defaultPositionZPrefKey = editorPrefsKeyPrefix + "defaultPositionZ";

	private const string deadZonePrefKey = editorPrefsKeyPrefix + "deadZone";
	private const string defaultZoomSizePrefKey = editorPrefsKeyPrefix + "defaultZoomSize";
	private const string moveSpeedPrefKey = editorPrefsKeyPrefix + "moveSpeed";

	private const string maxMagnitudePrefKey = editorPrefsKeyPrefix + "maxMagnitude";
	private const string clipOnMouseOutPrefKey = editorPrefsKeyPrefix + "clipOnMouseOut";
	private const string previewCamearDefaultsPrefKey = editorPrefsKeyPrefix + "previewCameraDefaults";

	private const string resetSceneViewCameraKey = "Reset Scene View Camera";
	private const string resetSceneViewCameraShortcut = " %#r";

	private const string resetSceneViewCameraRotationKey = "Reset Scene View Camera Rotation";
	private const string resetSceneViewCameraRotationShortcut = " %&#r";

	private const string toggleCameraHandleKey = "Toggle Camera Move Handle";
	private const string toggleCameraHandleShortcut = " %&#c";

	private const string menuPathPrefix = "Window/SprocketTools/Camera/";

	private const string preferencesMenuKey = "KCamHandle";
	private const string showCameraMoveHandleKey = "Show Camera Move Handle";
	private const string previewCameraDefaultsChangesKey = "Preview Camera Defaults Changes";
	private const string defaultCameraRotationKey = "Default Camera Rotation";
	private const string defaultCameraPositionKey = "Default Camera Position";
	private const string defaultCameraZoomKey = "Default Camera Zoom";
	private const string cameraMoveSpeedKey = "Camera Move Speed";
	private const string maximumMagnitudeKey = "Maximum Magnitude";
	private const string deadZoneMagnitudeKey = "Dead Zone Magnitude";
	private const string clipOnMouseOutKey = "Clip On Mouse Out";
	
	#endregion

	#region Preference Menu

	[PreferenceItem(preferencesMenuKey)]
	public static void PreferencesGUI()
	{
		// Preferences GUI
		SceneCameraHandle.DrawGUI();
	}

	private static void DrawGUI()
	{
		IsAttached = EditorGUILayout.ToggleLeft(showCameraMoveHandleKey, IsAttached);

		if(!IsAttached)
			GUI.enabled = false;

		EditorGUILayout.Space();
		
		PreviewCameraDefaultsChanges = EditorGUILayout.ToggleLeft(previewCameraDefaultsChangesKey, PreviewCameraDefaultsChanges);

		EditorGUILayout.Space();

		DefaultCameraPosition = EditorGUILayout.Vector3Field(defaultCameraPositionKey, DefaultCameraPosition);

		DefaultCameraRotation = Quaternion.Euler(EditorGUILayout.Vector3Field(defaultCameraRotationKey, DefaultCameraRotation.eulerAngles));
		
		EditorGUILayout.Space();

		DefaultCameraZoomSize = EditorGUILayout.Slider(defaultCameraZoomKey, DefaultCameraZoomSize, minCameraZoom, maxCameraZoom);
		
		EditorGUILayout.Space();

		if(PreviewCameraDefaultsChanges && GUI.changed)
		{
			ResetSceneViewCamera();
			SceneView.lastActiveSceneView.Repaint();
		}
		
		MoveSpeed = EditorGUILayout.FloatField(cameraMoveSpeedKey, MoveSpeed);

		GUI.changed = false;

		MaxAllowedMagnitude = EditorGUILayout.FloatField(maximumMagnitudeKey, MaxAllowedMagnitude);

		if(GUI.changed)
			previewMaxMagnitudeGizmoChangesTimer = EditorApplication.timeSinceStartup;

		GUI.changed = false;

		DeadZone = EditorGUILayout.FloatField(deadZoneMagnitudeKey, DeadZone);

		if(GUI.changed)
			previewDeadZoneGizmoChangesTimer = EditorApplication.timeSinceStartup;
	
		EditorGUILayout.Space();

		ClipOnMouseOut = EditorGUILayout.ToggleLeft(clipOnMouseOutKey, ClipOnMouseOut);

		GUI.enabled = true;
	}

	#endregion

	#region PublicInterface

	[MenuItem(menuPathPrefix + resetSceneViewCameraKey + resetSceneViewCameraShortcut, priority = 0)]
	public static void ResetSceneViewCamera ( )
	{
		SceneView.lastActiveSceneView.pivot = DefaultCameraPosition;
		SceneView.lastActiveSceneView.rotation = DefaultCameraRotation;
		SceneView.lastActiveSceneView.size = DefaultCameraZoomSize;

		SceneView.lastActiveSceneView.Repaint ();
	}

	[MenuItem(menuPathPrefix + resetSceneViewCameraRotationKey + resetSceneViewCameraRotationShortcut, priority = 1)]
	public static void ResetSceneViewCameraRotation ( )
	{		
		SceneView.lastActiveSceneView.rotation = DefaultCameraRotation;

		SceneView.lastActiveSceneView.Repaint ();
	}

	[MenuItem(menuPathPrefix + toggleCameraHandleKey + toggleCameraHandleShortcut, priority = 50)]
	public static void ToggleCameraHandlers ( )
	{
		IsAttached = !IsAttached;
	}

	#endregion

	static SceneCameraHandle ( )
	{
		if (IsAttached)
			AttachHandlers ();
	}

	#region Privates
	
	private static void AttachHandlers()
	{
		//prevent double subscription
		SceneView.onSceneGUIDelegate -= OnSceneGUI;

		SceneView.onSceneGUIDelegate += OnSceneGUI;

		controlID = GUIUtility.GetControlID (FocusType.Passive);

		if (SceneView.lastActiveSceneView != null)
			SceneView.lastActiveSceneView.Repaint ();
	}

	private static void DetachHandlers ( )
	{
		SceneView.onSceneGUIDelegate -= OnSceneGUI;

		if (SceneView.lastActiveSceneView != null)
			SceneView.lastActiveSceneView.Repaint ();
	}

	private static void OnSceneGUI(SceneView sceneView)
	{
		var color = Handles.color;

		var screenRect = sceneView.camera.pixelRect;

		Vector2 guiPoint = new Vector2 (screenRect.width * 0.5f, screenRect.height - 50.0f);

		var guiRay = HandleUtility.GUIPointToWorldRay (guiPoint);
		var mouseRay = HandleUtility.GUIPointToWorldRay (Event.current.mousePosition);

		float guiHitDistance = float.NegativeInfinity;
		float mouseHitDistance = float.NegativeInfinity;

		Vector3 guiLocation = Vector3.zero;
		Vector3 mouseLocation = Vector3.zero;
		float handleSizeAtGUILocation = float.PositiveInfinity;
		float handleSizeAtMouseLocation = float.PositiveInfinity;

		var guiHit = worldPlane.Raycast (guiRay, out guiHitDistance);
		var mouseHit = worldPlane.Raycast (mouseRay, out mouseHitDistance);

		if (guiHit)
		{
			guiLocation = guiRay.GetPoint (guiHitDistance);
			handleSizeAtGUILocation = HandleUtility.GetHandleSize (guiLocation);
		}

		if (mouseHit)
		{
			mouseLocation = mouseRay.GetPoint (mouseHitDistance);
			handleSizeAtMouseLocation = HandleUtility.GetHandleSize (mouseLocation);
		}

		if (Event.current.type == EventType.Layout)
		{
			if ((mouseHit)
			&& ((guiLocation - mouseLocation).magnitude < handleSizeAtGUILocation * focusBoxScale))
			{
				HandleUtility.AddControl (controlID, (mouseLocation - guiLocation).magnitude);
			}
		}

		if(ClipOnMouseOut
		&& ((Event.current.mousePosition.x < safeBorder * screenRect.width)
		|| (Event.current.mousePosition.x > (1.0f - safeBorder) * screenRect.width)
		|| (Event.current.mousePosition.y < safeBorder * screenRect.height)
		|| (Event.current.mousePosition.y > (1.0f - safeBorder) * screenRect.height)))
		{
			draggingHandle = false;
		}

		if(((GUIUtility.hotControl == 0) 
		|| (GUIUtility.hotControl == controlID))
		&& (Event.current.type == EventType.MouseUp)
		&& ((Event.current.button == 0)  //LMB
		|| (Event.current.button == 2))) //MMB
		{
			draggingHandle = false;

			Event.current.Use();
		}

		if (guiHit)
		{
			var col = Color.yellow;
			col.a = notFocusedTransparency;
			Handles.color = col;

			if (mouseHit)
			{
				var delta = (mouseLocation - guiLocation);
				var deltaMagnitude = delta.magnitude;
				var deltaNormalized = delta.normalized;

				if ((deltaMagnitude < handleSizeAtGUILocation * focusBoxScale) && (HandleUtility.nearestControl == controlID))
				{
					Handles.color = Color.yellow;

					if((Event.current.type == EventType.MouseDown)
					&& (Event.current.button == 1)) //RMB
					{
						Event.current.Use();
					}

					if(((GUIUtility.hotControl == 0)
					|| (GUIUtility.hotControl == controlID)) 
					&& (Event.current.type == EventType.MouseUp)
					&& (Event.current.button == 1)) //RMB
					{
						var menu = new GenericMenu();

						menu.AddItem(new GUIContent(resetSceneViewCameraKey), false, ResetSceneViewCamera);
						menu.AddItem(new GUIContent(resetSceneViewCameraRotationKey), false, ResetSceneViewCameraRotation);

						menu.ShowAsContext();

						draggingHandle = false;

						Event.current.Use();
					}

					if ((Event.current.type == EventType.MouseDown)
					&& ((Event.current.button == 0)  //LMB
					|| (Event.current.button == 2))) //MMB
					{
						draggingHandle = true;

						Event.current.Use();
					}
				}

				if (draggingHandle)
				{
					Handles.color = Color.green;

					if (deltaMagnitude > DeadZone)
						sceneView.pivot += deltaNormalized * Mathf.Min (deltaMagnitude, MaxAllowedMagnitude) * MoveSpeed;

					Handles.ArrowCap (controlID, guiLocation, Quaternion.LookRotation (delta), Mathf.Min (deltaMagnitude, MaxAllowedMagnitude));

					Handles.CircleCap (controlID, guiLocation, handleRotation, MaxAllowedMagnitude);

					Handles.RectangleCap (controlID, mouseLocation, handleRotation, handleSizeAtMouseLocation * mouseBoxScale);
				}
			}
			else
			{
				draggingHandle = false;
			}

			Handles.RectangleCap (controlID, guiLocation, handleRotation, handleSizeAtGUILocation * gizmoBoxScale);
		}
		else
		{
			draggingHandle = false;
		}

		float gizmoMaxMagnitudePreviewFactor = (float)(1.0f - (EditorApplication.timeSinceStartup - previewMaxMagnitudeGizmoChangesTimer) / previewGizmoChangeLifetime);

		if(gizmoMaxMagnitudePreviewFactor > 0.0f)
		{
			var c = Color.green;
			c.a = gizmoMaxMagnitudePreviewFactor;

			Handles.color = c;

			Handles.CircleCap(controlID, guiLocation, handleRotation, MaxAllowedMagnitude);
		}

		float gizmoDeadzonePreviewFactor = (float)(1.0f - (EditorApplication.timeSinceStartup - previewDeadZoneGizmoChangesTimer) / previewGizmoChangeLifetime);

		if(gizmoDeadzonePreviewFactor > 0.0f)
		{
			var c = Color.yellow;
			c.a = gizmoDeadzonePreviewFactor;

			Handles.color = c;

			Handles.CircleCap(controlID, guiLocation, handleRotation, DeadZone);
		}

		Handles.color = color;

		//Force repaint
		sceneView.Repaint ();
	}

	#endregion
}
