﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using UnityEditor;
using UnityEngine;

public class TypedTargetInspector<TObject> : Editor where TObject : MonoBehaviour
{
	protected TObject typedTarget = null;

	public void Awake()
	{
		typedTarget = (TObject)target;
	}

	public override void OnInspectorGUI()
	{
		//DrawHeader();
	}
}
