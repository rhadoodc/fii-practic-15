﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(CameraDirector))]
public class CameraDirectorInspector : TypedTargetInspector<CameraDirector>
{
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();

		typedTarget.uiSkin = (GUISkin)EditorGUILayout.ObjectField("UI Skin", typedTarget.uiSkin, typeof(GUISkin), false);

		typedTarget.renderCameraNames = EditorGUILayout.ToggleLeft("Render Camera Names", typedTarget.renderCameraNames);

		var cameras = typedTarget.GetCameraNames();

		if (cameras == null) //probably in Editor mode, or no cameras were yet registered (maybe none exist on the scene)
		{
			typedTarget.CurrentMainCamera = (DirectedCamera)EditorGUILayout.ObjectField("Current Main Camera", typedTarget.CurrentMainCamera, typeof(DirectedCamera), true);
		}
		else
		{
			var currentIndex = Array.IndexOf(cameras, typedTarget.CurrentMainCameraName);
		
			if (currentIndex >= 0)
			{
				var targetIndex = EditorGUILayout.Popup("Current Main Camera", currentIndex, cameras);
				typedTarget.CurrentMainCameraName = cameras[targetIndex];
			}
			else
			{
				Debug.LogWarning("Something went horribly wrong.");
			}
		}

		typedTarget.ViewportVerticalMarginRatio = EditorGUILayout.Slider("Viewport Margin Ratio", typedTarget.ViewportVerticalMarginRatio, 0.0f, 0.05f);

		typedTarget.OffViewportsPosition = (CameraDirector.EMainViewportPosition)EditorGUILayout.EnumPopup("Main Viewport Position", typedTarget.OffViewportsPosition);
	}
}
