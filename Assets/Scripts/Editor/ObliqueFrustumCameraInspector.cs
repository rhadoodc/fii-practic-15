﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;

[CustomEditor(typeof(ObliqueFrustumCamera))]
public class ObliqueFrustumCameraInspector : TypedTargetInspector<ObliqueFrustumCamera>
{
	public override void OnInspectorGUI()
	{
		base.OnInspectorGUI();

		typedTarget.HorizontalObliqueFactor = EditorGUILayout.Slider("Horizontal Obliqueness", typedTarget.HorizontalObliqueFactor, -1.0f, 1.0f);
		typedTarget.VerticalObliqueFactor = EditorGUILayout.Slider("Vertical Obliqueness", typedTarget.VerticalObliqueFactor, -1.0f, 1.0f);
	}
}
