﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(FixedLookAtCamera))]
public class FixedLookAtCameraInspector : TypedTargetInspector<FixedLookAtCamera>
{
	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();
	}

	private static readonly Color xzHandleFillColor = new Color(1.0f, 0.0f, 1.0f, 0.25f);
	private static readonly Color xzHandleArcColor = new Color(1.0f, 0.0f, 1.0f, 1.0f);

	private static readonly Color yzHandleFillColor = new Color(0.0f, 1.0f, 1.0f, 0.25f);
	private static readonly Color yzHandleArcColor = new Color(0.0f, 1.0f, 1.0f, 1.0f);

	private static readonly Color initialOrientationHandleColor = new Color(0.0f, 1.0f, 0.0f, 1.0f);
	private static readonly Color currentOrientationHandleColor = new Color(1.0f, 1.0f, 0.0f, 1.0f);

	[DrawGizmo(GizmoType.NotInSelectionHierarchy | GizmoType.InSelectionHierarchy | GizmoType.Active)]
	static void DrawGizmo(FixedLookAtCamera target, GizmoType type)
	{
		if (!target.enabled || !target.gameObject.activeSelf || !target.applyAngleConstraints)
			return;

		var col = Handles.color;

		Handles.color = xzHandleFillColor;

		Quaternion q = Quaternion.Euler(new Vector3(0.0f, target.minY, 0.0f));

		Handles.DrawSolidArc(target.transform.position, Vector3.up, q * Vector3.forward, target.maxY - target.minY, 5.0f);

		Handles.color = xzHandleArcColor;

		Handles.DrawWireArc(target.transform.position, Vector3.up, q * Vector3.forward, target.maxY - target.minY, 5.0f);

		Handles.color = yzHandleFillColor;

		q = Quaternion.Euler(new Vector3(target.minX, 0.0f, 0.0f));

		Handles.DrawSolidArc(target.transform.position, Vector3.right, q * Vector3.forward, target.maxX - target.minX, 5.0f);

		Handles.color = yzHandleArcColor;

		Handles.DrawWireArc(target.transform.position, Vector3.right, q * Vector3.forward, target.maxX - target.minX, 5.0f);

		Handles.color = initialOrientationHandleColor;

		q = Quaternion.Euler(target.InitialAngles);

		Handles.DrawLine(target.transform.position, target.transform.position + q * Vector3.forward * 4.0f);

		Handles.ArrowCap(0, target.transform.position + q * Vector3.forward * 4.0f, q, 1.0f);

		Handles.color = currentOrientationHandleColor;

		q = Quaternion.Euler(target.transform.eulerAngles);

		Handles.DrawLine(target.transform.position, target.transform.position + q * Vector3.forward * 3.0f);

		Handles.ArrowCap(0, target.transform.position + q * Vector3.forward * 3.0f, q, 1.0f);

		Handles.color = col;
	}
}
