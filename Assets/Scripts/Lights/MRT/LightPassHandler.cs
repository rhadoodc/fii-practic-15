﻿using UnityEngine;
using System.Collections;

public class LightPassHandler : MonoBehaviour 
{
	public Light lightComponent;

	public MeshFilter meshFilterComponent;
	public MeshRenderer meshRendererComponent;

	public Mesh sphereMesh;
	public Material pointLightMaterial;

	public virtual void Start()
	{
		gameObject.CheckAndInitializeWithInterface(ref lightComponent);

		lightComponent.enabled = false;

		var go = new GameObject("DeferredPointLightVolume");

		go.layer = LayerMask.NameToLayer("DeferredLights");

		go.transform.parent = transform;
		go.transform.localScale = Vector3.one * lightComponent.range * 2.0f;
		go.transform.localPosition = Vector3.zero;

		meshFilterComponent = go.AddComponent<MeshFilter>();

		meshFilterComponent.mesh = sphereMesh;

		meshRendererComponent = go.AddComponent<MeshRenderer>();

        Material newLightMaterial = new Material(pointLightMaterial);
		newLightMaterial.name = gameObject.name + "_LightMaterial";
        newLightMaterial.CopyPropertiesFromMaterial(pointLightMaterial);

		MRTCamera.RegisterLightMaterialInstance(newLightMaterial);

        meshRendererComponent.material = newLightMaterial;
		meshRendererComponent.shadowCastingMode = UnityEngine.Rendering.ShadowCastingMode.Off;
		meshRendererComponent.receiveShadows = false;
		meshRendererComponent.useLightProbes = false;
		meshRendererComponent.reflectionProbeUsage = UnityEngine.Rendering.ReflectionProbeUsage.Off;

		var lmu = go.AddComponent<LightMaterialUpdater>();
		lmu.lightComponent = lightComponent;
        lmu.lightMaterial = newLightMaterial;
	}
}
