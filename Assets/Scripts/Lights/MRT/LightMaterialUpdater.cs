﻿using UnityEngine;
using System.Collections;

public class LightMaterialUpdater : MonoBehaviour 
{
	public Light lightComponent;
	public Material lightMaterial;

    public void Start() { }

	public void Update()
	{
        //Debug.Log(string.Format("Will Render: {0} in frame {1} ", transform.parent.name, Time.frameCount));
        lightMaterial.SetFloat("_Range", lightComponent.range * transform.parent.localScale.x);
		lightMaterial.SetVector("_Position", transform.position);
        lightMaterial.SetFloat("_LightIntensity", lightComponent.intensity);
        lightMaterial.SetColor("_LightColor", lightComponent.color);
	}
}
