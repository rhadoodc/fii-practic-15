﻿using UnityEngine;
using System.Collections;

public class AILookAtController : MonoBehaviour 
{
    public AIBlackboard aiBlackboard;
    public string blackboardKey;
    public Animator animatorComponent;
    public float smoothAmount = 5f;
	public Vector3 lookAtOffset = Vector3.up;

    private Vector3? lastTargetPos;
    private Vector3 finalTargetPos;
	private float currentLookAtWeight;
	private Transform headTransform;

    protected void Awake()
    {
        gameObject.CheckAndInitializeWithInterface(ref animatorComponent);
		headTransform = animatorComponent.GetBoneTransform(HumanBodyBones.Head);
    }

    public void OnAnimatorIK()
    {
        object keyValue;
        if (aiBlackboard.Data.TryGetValue(blackboardKey, out keyValue))
            lastTargetPos = (Vector3?)keyValue;
        else
            lastTargetPos = null;

        if (lastTargetPos != null)
        {
            finalTargetPos = Vector3.Lerp(finalTargetPos, lastTargetPos.Value, smoothAmount * Time.deltaTime);
			currentLookAtWeight = Mathf.Lerp(currentLookAtWeight, 1f, smoothAmount * Time.deltaTime);

            animatorComponent.SetLookAtWeight(currentLookAtWeight);
            animatorComponent.SetLookAtPosition(finalTargetPos + Vector3.up);
        }
		else if (currentLookAtWeight > 0.01f)
		{
			Vector3 headForwardPosition = headTransform.position + transform.forward;
			finalTargetPos = Vector3.Lerp(finalTargetPos, headForwardPosition, smoothAmount * Time.deltaTime);
			currentLookAtWeight = Mathf.Lerp(currentLookAtWeight, 0f, smoothAmount * Time.deltaTime);

			animatorComponent.SetLookAtWeight(currentLookAtWeight);
			animatorComponent.SetLookAtPosition(finalTargetPos + lookAtOffset);
		}
    }
}
