﻿using UnityEngine;
using System.Collections;

public class FollowTargetStateBehaviour : AAIStateBehaviour
{
    public string lastAITargetPositionKey = "lastAITargetPosition";
    public float followUpdateThreshold = 0.2f;
    public string walkMode = "Run";

    protected AINavController navController;
    
	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        // Init behaviour data.
        aiBlackboard.CheckAndInitializeFrom(animator.gameObject, AINavController.navController_key, ref navController);
    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (CanUpdateBehaviour())
        {
            Vector3? lastAITargetPosition = aiBlackboard.Data[lastAITargetPositionKey] as Vector3?;
            
            if (lastAITargetPosition != null)
            {
                navController.CurrentMoveMode = walkMode;
                navController.navMeshAgent.SetDestination(lastAITargetPosition.Value);
            }

            navController.navMeshAgent.SetDestination(lastAITargetPosition.Value);
        }
    }
}
