﻿using UnityEngine;
using System.Collections;

public class WanderStateBehaviour : AAIStateBehaviour 
{
    public float wanderPositionPickMinTime = 2f;
    public float wanderPositionPickMaxTime = 5f;
    public float randomPosPickRadius = 10f;
    public string walkMode = "Walk";

    private float nextTimePickNewWanderPos;
    private float lastTimeWanderPosWasSet;

    protected AINavController navController;

    #region StateMachineBehaviour methods
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        // Init behaviour data.
        aiBlackboard.CheckAndInitializeFrom(animator.gameObject, AINavController.navController_key, ref navController);
    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        UpdateRandomTargetPosition();
    }

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	//override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
    //}
    #endregion StateMachineBehaviour methods

    public void UpdateRandomTargetPosition()
    {
        // Check if it's time to update our random target position.
        if (Time.timeSinceLevelLoad - lastTimeWanderPosWasSet >= nextTimePickNewWanderPos)
        {
            navController.CurrentMoveMode = walkMode;
            navController.navMeshAgent.SetDestination(GetRandomTargetPosition());
            
            lastTimeWanderPosWasSet = Time.timeSinceLevelLoad;
            nextTimePickNewWanderPos = Random.RandomRange(wanderPositionPickMinTime, wanderPositionPickMaxTime);
        }
    }

    public Vector3 GetRandomTargetPosition()
    {
        Vector3 randomHeading = Random.insideUnitSphere * randomPosPickRadius;
        return navController.transform.position + randomHeading;
    }
}
