﻿using UnityEngine;
using System.Collections;

public class FindTargetStateBehaviour : AAIStateBehaviour
{
    public const string visibilityController_key = "visibilityController";
    public const string lastAITargetPosition_key = "lastAITargetPosition";

    public string targetFoundBoolAnimatorParamName = "TargetFound";
    public float timeToRememberLastPosition = 3f;

    private int targetFoundBoolParamId = -1;
    private float lastTimeRemeberUpdated;

    protected AIVisibilityController visibilityController;
    protected GameObject currentAITarget = null;

	// OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        aiBlackboard.CheckAndInitializeFrom(animator.gameObject, visibilityController_key, ref visibilityController);
        if (targetFoundBoolParamId < 0)
            targetFoundBoolParamId = Animator.StringToHash(targetFoundBoolAnimatorParamName);
    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (CanUpdateBehaviour())
        {
            currentAITarget = visibilityController.Target;

            if (currentAITarget != null)
            {
                aiBlackboard.Data[lastAITargetPosition_key] = (Vector3?)currentAITarget.transform.position;
                animator.SetBool(targetFoundBoolParamId, true);
                lastTimeRemeberUpdated = Time.timeSinceLevelLoad;
            }
            else
            {
                if (Time.timeSinceLevelLoad - lastTimeRemeberUpdated >= timeToRememberLastPosition)
                {
                    animator.SetBool(targetFoundBoolParamId, false);
                    aiBlackboard.Data[lastAITargetPosition_key] = null;
                }
            }
        }
    }
}
