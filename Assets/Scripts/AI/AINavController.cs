﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class AIMoveMode
{
    public string name;
    public float moveSpeedFactor;
}

public class AINavController : MonoBehaviour, ICharacterInputProvider
{
    public const string navController_key = "navController";

    public event InputActionCallback OnJump;
    public event InputActionCallback OnLightAttack;
    public event InputActionCallback OnHeavyAttack;

    public NavMeshAgent navMeshAgent;
    public Animator logicAnimator;
    public AIMoveMode[] aiMoveModesSetup;
    [System.NonSerialized]
    public Dictionary<string, AIMoveMode> aiMoveModes;
    private AIMoveMode currentMoveMode;

    public Transform targetTest;


    public string CurrentMoveMode
    {
        get { return currentMoveMode.name; }
        set { currentMoveMode = aiMoveModes[value]; }
    }

    void Awake()
    {
        gameObject.CheckAndInitializeWithInterface(ref navMeshAgent);
        gameObject.CheckAndInitializeWithInterface(ref logicAnimator);

        // Don't let the nav agent update our character position or rotation.
        navMeshAgent.updatePosition = false;
        navMeshAgent.updateRotation = false;        
    }

	// Use this for initialization
	void Start () 
    {
        // Init move modes dictionary for faster and easier access to the configured AI move modes.
        aiMoveModes = new Dictionary<string, AIMoveMode>(aiMoveModesSetup.Length);
        
        for(int i = 0; i < aiMoveModesSetup.Length; i++)
            aiMoveModes[aiMoveModesSetup[i].name] = aiMoveModesSetup[i];
	}
	
	// Update is called once per frame
	void Update()
    {
        //if (targetTest != null)
        {
            // Set the agent current destination (not wise to set it repeatedlly as it will trigger repeated path calculations).
            //navMeshAgent.SetDestination(targetTest.position);
            // Manually control the agent and set him to the position our character is currently at.
            navMeshAgent.nextPosition = transform.position;
        }
	}

    protected void OnDrawGizmos()
    {
        Color defaultColor = Gizmos.color;
        if (navMeshAgent != null)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(navMeshAgent.destination, 0.25f);
            Gizmos.DrawWireSphere(navMeshAgent.destination, navMeshAgent.stoppingDistance);
        }

        Gizmos.color = defaultColor;
    }

    #region ICharacterInputProvider interface
    public Vector3 GetCharacterInputVelocity()
    {
        // Use the navigation agent current velocity vector as the input for anyone using this ICharacterInputProvider (like RootMotionLocomotion).
        return Vector3.ClampMagnitude(navMeshAgent.desiredVelocity, currentMoveMode != null ? currentMoveMode.moveSpeedFactor : Mathf.Infinity);
    }

    public bool GetJumpState()
    {
        //TODO:
        return false;
    }

    public bool GetLightAttackState()
    {
        //TODO:
        return false;
    }

    public bool GetHeavyAttackState()
    {
        //TODO:
        return false;
    }
    #endregion ICharacterInputProvider interface
}