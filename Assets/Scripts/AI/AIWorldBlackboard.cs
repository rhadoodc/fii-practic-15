﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AIWorldBlackboard : AIBlackboard
{
	public GameObject ObjectFromID(int targetID)
	{
		GameObject target = null;

		aiTargets.TryGetValue(targetID, out target);

		return target;
	}
}
