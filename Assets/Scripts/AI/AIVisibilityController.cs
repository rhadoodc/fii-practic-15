﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
#if UNITY_EDITOR
using UnityEditor;
using SprocketTools.Base;
#endif

public class AIVisibilityController : MonoBehaviour 
{
	public int SightTextureWidth = 32;
	public int SightTextureHeight = 8;
	public Camera AISightCameraObject;
	public RenderTexture sightTexture;
	public Texture2D tex;

	private Rect readRect;

	public GameObject Target
	{
		get
		{
			return target;
		}
	}

	private GameObject target = null;

	public void Awake()
	{
		sightTexture = new RenderTexture(SightTextureWidth, SightTextureHeight, 0, RenderTextureFormat.ARGB32, RenderTextureReadWrite.Default);
		sightTexture.Create();

		tex = new Texture2D(SightTextureWidth, SightTextureHeight, TextureFormat.ARGB32, false);

		readRect = new Rect(0, 0, SightTextureWidth, SightTextureHeight);

		AISightCameraObject.targetTexture = sightTexture;
	}

	public void Update()
	{
		IEnumerable<int> targetIDs = FindTargetIDs(sightTexture);

		target = null;

		foreach (var targetID in targetIDs)
		{
			target = IdentifyTarget(targetID);
		}
	}

	private GameObject IdentifyTarget(int targetID)
	{
		return KGameObjectSingleton<AIWorldBlackboard>.Instance.ObjectFromID(targetID);
	}

	private IEnumerable<int> FindTargetIDs(RenderTexture sightTexture)
	{
		var oldRT = RenderTexture.active;

		RenderTexture.active = sightTexture;

		tex.ReadPixels(readRect, 0, 0);

		for (int i = 0; i < SightTextureHeight; i ++)
		for (int j = 0; j < SightTextureWidth; j ++)
		{
			var pix = tex.GetPixel(j, i);
			if (pix.r != 0.0f)
			{
				yield return (int)(pix.r * 255);

				//this is only for single targets at the moment
				RenderTexture.active = oldRT;
				
				yield break;
			}
		}

		RenderTexture.active = oldRT;

		yield break;
	}
}
