﻿using UnityEngine;
using System.Collections;

/// <summary>
/// CharacterPhysicsController.
/// 
/// Controls the physics behavior for a character that should be able to move, jump, 
/// have a slight controll in mid-air, respond to gravity.
/// </summary>
[DisallowMultipleComponent]
[RequireComponent(typeof(Animator))]
public class CharacterPhysicsController : MonoBehaviour
{
	public LayerMask walkableLayers;
	public string animatorJumpBoolName = "Jump";
	public string animatorIsGroundedBoolName = "IsGrounded";
	public string animatorJumpLandState = "Jump.LandJump";
    protected int animatorJumpBoolId;
    protected int animatorIsGroundedBoolId;
    protected int animatorJumpLandStateId;

	/// <summary>
	/// This must be set by previewing the land animation and getting the delay(in seconds) until the feet touches the ground.
	/// </summary>
	public float landAnimDelayUntilGrounded = 0.09f;
	public float landAnimTransitionTime = 0.2f;
	/// <summary>
	/// From which height should we begin predicting the land animation start before reaching the ground.
	/// It uses sphere casting with the radius <see cref="feetColliderRadius"/>.
	/// </summary>
	public float landAnimPredictionHeight = 2f;
	private float fallHeight = 0f;
	private bool landAnimationStarted = false;

	public float feetColliderRadius = 0.25f;

	public float jumpVelocityAmount = 10f;
	public float jumpForwardForceAmount = 30f;
	public float inAirControlForceAmount = 80f;
	public float inAirTurnSpeed = 3f;
	public float inAirControlDelay = 1.5f;
	public float inAirRigidbodyDrag = 0.25f;

	protected float inAirControlTimer = 0f;
	/// <summary>
	/// The direction can be temporarily changed at certain times to obtain different jump directions (think of wall jumping).
	/// TODO: not thouroughly tested with other values than Vector3.up.
	/// </summary>
	public Vector3 jumpDirection = Vector3.up;

	public float extraGravityFactor = 1.5f;

    protected Rigidbody currentMovingPlatform;

	/// <summary>
	/// The input jump power factor can be used to scale the jump power based on pressure sensitive controls.
	/// </summary>
	protected float inputJumpPowerFactor;
	protected float forwardSpeedBeforeJump = 0f;
	protected float defaultTurnSpeed;

    public System.Func<bool> canJumpEvaluateFunc;
	protected bool isJumping;
	protected bool doJump;

	[System.NonSerialized]
	public RootMotionLocomotion locomotion;

	[System.NonSerialized]
	public Animator animator;

	[System.NonSerialized]
	public Rigidbody cachedRigidbody;

	protected GroundSensorListener groundSensor;

	public void Awake()
	{
		gameObject.CheckAndInitializeWithInterface(ref cachedRigidbody);
		gameObject.CheckAndInitializeWithInterface(ref animator);
		gameObject.CheckAndInitializeWithInterface(ref locomotion);

		groundSensor = GetComponentInChildren<GroundSensorListener>();
		if (groundSensor != null)
		{
			if (!groundSensor.GetComponent<Collider>().isTrigger)
			{
				Debug.LogError("CharacterPhysicsController: GroundSensorListener found is not a trigger: " + gameObject.name);
			}

			groundSensor.OnGroundSensorStateChange += HandleOnGroundSensorStateChange;
            groundSensor.OnGroundSensorEnter += HandleOnGroundSensorEnter;
            groundSensor.OnGroundSensorExit += HandleOnGroundSensorExit;
		}
		else
		{
			Debug.LogError("CharacterPhysicsController: GroundSensorListener was " +
							"not found in the children of " + gameObject.name, gameObject);
		}

        if (canJumpEvaluateFunc == null)
            canJumpEvaluateFunc = CanJumpEvaluator;

        // Get optimized hash ids for the animator parameters we need to access often.
        animatorJumpBoolId = Animator.StringToHash(animatorJumpBoolName);
        animatorIsGroundedBoolId = Animator.StringToHash(animatorIsGroundedBoolName);
        animatorJumpLandStateId = Animator.StringToHash(animatorJumpLandState);
	}

	public void Start()
	{
		defaultTurnSpeed = locomotion.turnSpeedFactorTowardTarget;
	}

	// Get the rigidbody forward velocity
	public float ForwardSpeed
	{
		get { return Vector3.Dot(cachedRigidbody.velocity, cachedRigidbody.rotation * Vector3.forward); }
	}
	
	public Vector3 CurrentVelocity
	{
		get { return cachedRigidbody.velocity; }
	}

	public bool IsJumping
	{
		get { return isJumping; }
		protected set 
		{ 
			isJumping = value;
		}
	}

	public bool IsGrounded
	{
		get { return groundSensor.IsGroundSensorTriggered; }
	}

	public bool CanJump
	{
		get 
        {
            var evaluator = canJumpEvaluateFunc;
            if (evaluator == null)
            {
                evaluator = canJumpEvaluateFunc = CanJumpEvaluator;
            }

            return evaluator();
        }
	}

    /// <summary>
    /// Default CanJump state evaluator method.
    /// This method can be overriden through the <see cref="canJumpEvaluateFunc"/> field to 
    /// customize the jump condition and obtain double jumping behavior for example.
    /// </summary>
    /// <returns></returns>
    protected bool CanJumpEvaluator()
    {
        return !IsJumping && IsGrounded;
    }

    protected void AttachToMovingPlatform(Rigidbody movingPlatform)
    {
        currentMovingPlatform = movingPlatform;

        Vector3 backupLocalScale = transform.localScale;
        transform.parent = currentMovingPlatform.transform;
        transform.localScale = backupLocalScale;
    }

    protected void DetachFromCurrentMovingPlatform()
    {
        if (currentMovingPlatform != null)
        {
            currentMovingPlatform = null;

            Vector3 backupLocalScale = transform.localScale;
            transform.parent = null;
            transform.localScale = backupLocalScale;
        }
    }

	void HandleOnGroundSensorStateChange(GroundSensorListener sender, bool _isGrounded)
	{
		animator.SetBool(animatorIsGroundedBoolId, _isGrounded);

        if (_isGrounded)
        {
            locomotion.turnSpeedFactorTowardTarget = defaultTurnSpeed;
            cachedRigidbody.drag = 0f;

            landAnimationStarted = false;
        }
	}

    void HandleOnGroundSensorEnter(GroundSensorListener sender, Collider collider)
    {
        if ( ((1 << collider.gameObject.layer) & walkableLayers.value) != 0 )
        {
            Rigidbody physicsBody = collider.GetComponent<Rigidbody>();
            if (physicsBody != null && physicsBody.isKinematic)
                AttachToMovingPlatform(collider.GetComponent<Rigidbody>());
        }
    }

    void HandleOnGroundSensorExit(GroundSensorListener sender, Collider collider)
    {
        if (currentMovingPlatform != null && collider.GetComponent<Rigidbody>() == currentMovingPlatform)
        {
            DetachFromCurrentMovingPlatform();
        }
    }

	public void Jump(float jumpPowerFactor)
	{
		doJump = true;
		inputJumpPowerFactor = jumpPowerFactor;
		animator.SetBool(animatorJumpBoolId, true);

        // Detach from current moving platform (if there's any) here because the ground sensor is disabled
        // when jumping and we won't receive any more events from it until we start to fall again.
        DetachFromCurrentMovingPlatform();
	}

    protected void PredictLandAnimationTransition()
    {
        RaycastHit hitInfo;
        if (Physics.SphereCast(transform.position, feetColliderRadius, -transform.up, out hitInfo,
                               landAnimPredictionHeight, walkableLayers.value))
        {
            fallHeight = hitInfo.distance;
            //Debug.DrawRay(transform.position, -Vector3.up * fallHeight, Color.blue, 0f, false);

            // The predicted height (based on the character fall velocity) at which the landing animation should start.
            float predictedLandAnimStartHeight = landAnimDelayUntilGrounded * -CurrentVelocity.y;

            if (!landAnimationStarted && fallHeight <= Mathf.Max(0f, predictedLandAnimStartHeight))
            {
                landAnimationStarted = true;
                if (predictedLandAnimStartHeight > 1e-5f)
                {
                    // Cross fade to the jump landing animation state and start playing it from the calculated normalized time below.
                    animator.CrossFade(animatorJumpLandStateId, landAnimTransitionTime, 0, 1f - fallHeight / predictedLandAnimStartHeight);
                }
                else
                    animator.CrossFade(animatorJumpLandStateId, landAnimTransitionTime, 0, 1f);
            }
        }
    }
	protected void UpdateGroundedState()
	{
		// Check if the character is about to fall. (not yet started to fall but he decelerated enough)
		if (IsJumping && CurrentVelocity.y <= 0.5f * jumpVelocityAmount)
		{
			groundSensor.SetSensorActiveState(true);

			animator.SetBool(animatorJumpBoolId, false);

			// If during a fall we become grounded then reset the IsJumping flag.
			if (IsGrounded)
				IsJumping = false;
		}
	}

	protected void UpdateJumpState()
	{
		if (doJump && CanJump)
		{
			forwardSpeedBeforeJump = ForwardSpeed;

			cachedRigidbody.AddForce(jumpDirection * jumpVelocityAmount * inputJumpPowerFactor, ForceMode.VelocityChange);

			IsJumping = true;
			doJump = false;
			groundSensor.SetSensorActiveState(false);
			inAirControlTimer = inAirControlDelay;
		}
		
		if (!IsGrounded)
		{
            // Change character physics and control factors while in-air.
			cachedRigidbody.drag = inAirRigidbodyDrag;
			locomotion.turnSpeedFactorTowardTarget = inAirTurnSpeed;
			float inAirControlFactor = inAirControlTimer / inAirControlDelay;

			if (IsJumping)
			{
				// In a certain amount of time the initial jump forward force should go down to zero and leave only the in-air control force.
				Vector3 forwardJumpForce = transform.forward * forwardSpeedBeforeJump * jumpForwardForceAmount * inAirControlFactor;
				cachedRigidbody.AddForce(forwardJumpForce, ForceMode.Acceleration);
			}

			Vector3 inAirControlForce = Vector3.ClampMagnitude(locomotion.TargetVelocity, 1f) * inAirControlForceAmount * inAirControlFactor;
			// Apply in air extra control force.
			cachedRigidbody.AddForce(inAirControlForce , ForceMode.Acceleration);

			inAirControlTimer = Mathf.Max(0f, inAirControlTimer - Time.deltaTime);

            // If the character is falling predict when we should cross-fade to the land animation.
            if (CurrentVelocity.y <= 0f && !landAnimationStarted)
                PredictLandAnimationTransition();
		}	
	}

	protected void UpdateGravity()
	{
		if (!IsGrounded)
			cachedRigidbody.AddForce(Physics.gravity * extraGravityFactor, ForceMode.Acceleration);
	}

    /// <summary>
    /// Unity event.
    /// </summary>
    protected void OnAnimatorMove()
    {
        Vector3 moveVelocity = animator.velocity;

        moveVelocity.y = cachedRigidbody.velocity.y;
        cachedRigidbody.velocity = moveVelocity;
    }

    protected void FixedUpdate()
    {
        UpdateGroundedState();
        UpdateJumpState();
        UpdateGravity();
    }

	#region Temporary debug area
	private bool isSlowMotion = false;
	private float defaultFixedTime;
	private float defaultMaxDeltaTime;
	protected void OnGUI()
	{
		GUILayout.BeginVertical();
		{
			GUILayout.Box("inAirControlTimer = " + inAirControlTimer, GUILayout.Width(220));
			GUILayout.Box("forwardSpeed = " + ForwardSpeed);

			if (GUILayout.Button("Slow Motion: " + isSlowMotion) || (Event.current.type == EventType.KeyDown && Event.current.keyCode == KeyCode.T))
			{
				isSlowMotion = !isSlowMotion;
				if (isSlowMotion)
				{
					defaultFixedTime = Time.fixedDeltaTime;

					Time.timeScale = 0.2f;
					Time.fixedDeltaTime *= Time.timeScale;
				}
				else 
				{
					Time.fixedDeltaTime = defaultFixedTime;
					Time.timeScale = 1f;
				}
			}
		}
		GUILayout.EndVertical();
	}
	#endregion Temporary debug area
}
