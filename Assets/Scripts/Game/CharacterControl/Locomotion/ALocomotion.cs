﻿using UnityEngine;
using System.Collections;

public abstract class ALocomotion : MonoBehaviour
{   
	public Vector3 targetVelocityAxisFilter = new Vector3(1f, 0f, 1f);
    public float turnSpeedFactorTowardTarget = 4f;

	protected Vector3 targetVelocity;

	public virtual Vector3 TargetVelocity
	{
		get { return targetVelocity; }
		set 
		{ 
			targetVelocity = value;
			targetVelocity.Scale(targetVelocityAxisFilter);
		}
	}

	public virtual void StopMove()
	{
		TargetVelocity = Vector3.zero;
	}

    public abstract void UpdateMoveLogic();

    public abstract void UpdateTurnLogic();

    public void UpdateLogic()
    {
    	if (!enabled)
    		return;

		UpdateTurnLogic();
        UpdateMoveLogic();
    }

	protected virtual void Update() 
	{
		UpdateLogic();
	}
}
