﻿using UnityEngine;
using System.Collections;

public class RootMotionLocomotion : ALocomotion
{
	[System.NonSerialized]
	public Animator animator;

    [SerializeField]
    private string speedAnimatorParamName = "MoveSpeed";
	private int speedAnimatorParamHash;

	/// <summary>
	/// When using character relative movement this flag should be true to allow reverse walking.
	/// </summary>
	public bool allowReverseMovement = false;
	public float turnSpeedFactorReverseOn = 3.5f;

    /// <summary>
    /// Indicates how fast should the current character move speed lerp to a new target speed. (multiplied with Time.deltaTime)
    /// </summary>
    [SerializeField]
    private float moveSpeedDeltaTimeLerpFactor = 4f;

	/// <summary>
	/// The move speed factor should be a value in the interval [0.0f, 1.0f] so we can tell the Animator the speed percentage based on which
	/// it will interpolate the moving animation corresponding to the Mecanim Controller state machine.
	/// </summary>
	protected float speedFactor;
	protected float targetMoveSpeedFactor;
	protected Vector3 targetMoveDirection = Vector3.zero;


    protected void Awake()
    {
		gameObject.CheckAndInitializeWithInterface(ref animator);
		// Init property with value setup in editor.
		AnimatorSpeedParamName = speedAnimatorParamName;

		if (allowReverseMovement)
			turnSpeedFactorTowardTarget = turnSpeedFactorReverseOn;
    }

	public string AnimatorSpeedParamName
	{
		get { return speedAnimatorParamName; }
		set 
		{
			speedAnimatorParamName = value;
			speedAnimatorParamHash = Animator.StringToHash(speedAnimatorParamName);
		}
	}

    public float MoveSpeedDeltaTimeLerpFactor
    {
        get { return moveSpeedDeltaTimeLerpFactor; }
        set { moveSpeedDeltaTimeLerpFactor = value; }
    }

    public Animator LocomotionAnimator
    {
        get { return animator; }
    }

	public override Vector3 TargetVelocity 
	{
		set 
		{
			base.TargetVelocity = value;

			float velocityMagnitude = targetVelocity.magnitude;
			targetMoveSpeedFactor = Mathf.Clamp01(velocityMagnitude);

			if (velocityMagnitude > 1E-5f)
				targetMoveDirection = targetVelocity / velocityMagnitude;
			else
				targetMoveDirection = Vector3.zero;

			if (allowReverseMovement)
			{
				targetMoveSpeedFactor = Mathf.Clamp(Vector3.Dot(targetVelocity, transform.forward), -1f, 1f);

				float directionFactor = Vector3.Dot(transform.forward, targetMoveDirection);
				if (directionFactor < -1E-5f)
					targetMoveDirection = -targetMoveDirection;
			}
		}
	}

	public override void UpdateTurnLogic()
	{
		if (targetMoveDirection != Vector3.zero)
		{
			transform.localRotation = Quaternion.Lerp(transform.localRotation, Quaternion.LookRotation(targetMoveDirection), Time.deltaTime * turnSpeedFactorTowardTarget);
		}
	}

    public override void UpdateMoveLogic()
    {
        speedFactor = Mathf.Lerp(speedFactor, targetMoveSpeedFactor, Time.deltaTime * moveSpeedDeltaTimeLerpFactor);
		LocomotionAnimator.SetFloat(speedAnimatorParamHash, speedFactor);
    }
}
