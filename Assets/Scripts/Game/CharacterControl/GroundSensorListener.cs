﻿using UnityEngine;
using System.Collections;

public class GroundSensorListener : MonoBehaviour 
{
	public delegate void GroundSensorStateChangeCallback(GroundSensorListener sender, bool isGrounded);
	public delegate void GroundSensorTriggeredCallback(GroundSensorListener sender, Collider collider);

	#region Events
	public event GroundSensorStateChangeCallback OnGroundSensorStateChange;
	public event GroundSensorTriggeredCallback OnGroundSensorEnter;
	public event GroundSensorTriggeredCallback OnGroundSensorExit;
	#endregion Events
	
	private int groundSensorFlagCount = 0;


	public bool IsGroundSensorTriggered
	{
		get { return groundSensorFlagCount > 0; }
	}

	public void SetSensorActiveState(bool isEnabled)
	{
		if (!isEnabled)
		{
			if (groundSensorFlagCount != 0)
				RaiseOnGroundSensorStateChange(false);

			groundSensorFlagCount = 0;
		}
		gameObject.SetActive(isEnabled);
	}

	protected void OnTriggerEnter(Collider enteredCollider)
	{
		RaiseOnGroundSensorEnter(enteredCollider);

		groundSensorFlagCount++;

		if (groundSensorFlagCount == 1)
			RaiseOnGroundSensorStateChange(true);
	}
	
	protected void OnTriggerExit(Collider exitedCollider)
	{
		RaiseOnGroundSensorExit(exitedCollider);

		groundSensorFlagCount--;
		if (groundSensorFlagCount <= 0)
		{
			RaiseOnGroundSensorStateChange(false);
			groundSensorFlagCount = 0;
		}
	}

	protected void RaiseOnGroundSensorStateChange(bool isGrounded)
	{
		var eh = OnGroundSensorStateChange;

		if (eh != null)
			eh(this, isGrounded);
	}

	protected void RaiseOnGroundSensorEnter(Collider collider)
	{
		var eh = OnGroundSensorEnter;

		if (eh != null)
			eh(this, collider);
	}

	protected void RaiseOnGroundSensorExit(Collider collider)
	{
		var eh = OnGroundSensorExit;
		
		if (eh != null)
			eh(this, collider);
	}
}
