﻿public interface IFSMState
{	
	void UpdateState(object stateData);

	void OnEnterState();
	void OnLeaveState();
}
