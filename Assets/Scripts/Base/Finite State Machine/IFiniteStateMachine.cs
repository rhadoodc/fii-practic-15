﻿using System;

public abstract class IFiniteStateMachine<K, T> 
	where T : IFSMState
	where K : IEquatable<K>
{
	public abstract void Update(object StateData);

	public abstract T CurrentState
	{
		get;
	}

	public abstract bool TryEnterState(K targetStateKey);
}
