﻿using System;
using System.Collections.Generic;
using System.Linq;

public class FiniteStateMachine<K, T> : IFiniteStateMachine<K, T> 
	where T : IFSMState 
	where K : IEquatable<K>
{
	private K defaultStateKey = default(K);

	public K DefaultStateKey
	{
		set
		{
			if ((currentStateKey == null) || currentStateKey.Equals(defaultStateKey))
				TryEnterState(value); //plain assignation wouldn't notify OnEnterState

			defaultStateKey = value;
		}
	}

	private K currentStateKey = default(K);

	public Dictionary<K, T> states;

	public override T CurrentState
	{
		get 
		{
 			if ((currentStateKey != null) && states.ContainsKey(currentStateKey))
				return states[currentStateKey];

			return default(T);
		}
	}

	public override bool TryEnterState(K targetStateKey)
	{
		if (states.ContainsKey(targetStateKey))
		{
			if (targetStateKey.Equals(currentStateKey))
				UnityEngine.Debug.LogWarning("Attempting to transition to already active state.");

			if (CurrentState != null)
				CurrentState.OnLeaveState();

			currentStateKey = targetStateKey;

			if (CurrentState != null)
			{
				CurrentState.OnEnterState();
				return true;
			}

			return false;
		}
		else
			return false;
	}

	public override void Update(object StateData)
	{
		//Debug.Log ("FSM Update.");
		if (CurrentState != null)
			CurrentState.UpdateState(StateData);
	}

	public FiniteStateMachine()
	{
		currentStateKey = defaultStateKey;
		states = new Dictionary<K, T>();
	}
}
