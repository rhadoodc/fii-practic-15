﻿using UnityEngine;
using System.Collections;
using SprocketTools.Base;

public class PitTrigger : MonoBehaviour 
{
	private float scoreTimerStart = -1.0f;

	public float scoreDelay = 5.0f;

	private bool scoreCalculated = false;

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (!scoreCalculated && (scoreTimerStart > 0.0f) && (Time.timeSinceLevelLoad - scoreTimerStart > scoreDelay))
		{
			scoreCalculated = true;
			KGameObjectSingleton<PinManager>.Instance.CalculateScore();
		}
	}

	void OnTriggerEnter(Collider other)
	{
		scoreCalculated = false;
		BawlingBallController ballController = other.GetComponent<BawlingBallController>();
		if (ballController != null)
		{
			scoreTimerStart = Time.timeSinceLevelLoad;
		}
	}
}
