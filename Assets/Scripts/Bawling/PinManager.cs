﻿using UnityEngine;
using System.Collections;
using SprocketTools.Base;
using System.Collections.Generic;
using UnityEngine.UI;

public class PinManager : KMonoBehaviour 
{
	private List<BowlingPinController> registeredPins = new List<BowlingPinController>();

	public Text scoreText = null;

	public void RegisterPin(BowlingPinController pin)
	{
		registeredPins.Add(pin);
	}

	// Use this for initialization
	void Start () 
	{
	
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyUp(KeyCode.S))
		{
			CalculateScore();
		}
	}

	public void CalculateScore()
	{
		int score = 0;
		foreach (var pin in registeredPins)
		{
			score += pin.IsGrounded() ? 0 : 1;
		}

		if (scoreText != null)
		{
			scoreText.text = string.Format("Last round score: {0}", score);
		}
	}
}
