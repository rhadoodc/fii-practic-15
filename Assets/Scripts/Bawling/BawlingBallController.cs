﻿using UnityEngine;
using System.Collections;

public class BawlingBallController : MonoBehaviour 
{
	private bool launched = false;

	public Rigidbody rigidbodyComponent = null;

	private Vector3 initialPosition;

	public GameObject camera = null;

	private Vector3 initialMousePosition;

	public float forceMultiplier = 1.0f;

	public float torqueMultiplier = 1.0f;

	void Awake()
	{
		gameObject.CheckAndInitializeWithInterface(ref rigidbodyComponent);

		initialPosition = transform.position;

		if (camera == null)
		{
			camera = Camera.main.gameObject;
		}
	}

	// Use this for initialization
	void Start () 
	{
		Reset();
	}

	private void Reset()
	{
		launched = false;
		rigidbodyComponent.isKinematic = true;

		transform.position = initialPosition;

		rigidbodyComponent.velocity = Vector3.zero;
		rigidbodyComponent.angularVelocity = Vector3.zero;
	}

	private void Launch(float launchPower, float spin)
	{
		launched = false;
		rigidbodyComponent.isKinematic = false;

		rigidbodyComponent.AddForce(camera.transform.forward * launchPower, ForceMode.Impulse);

		rigidbodyComponent.AddTorque(0.0f, spin, 0.0f, ForceMode.Impulse);
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetMouseButtonDown(0))
		{
			initialMousePosition = Input.mousePosition;
		}

		if (Input.GetMouseButtonUp(0))
		{
			Launch(Mathf.Abs((Input.mousePosition - initialMousePosition).y * forceMultiplier), Input.GetAxis("Mouse X") * torqueMultiplier);
		}

		if (Input.GetKeyUp(KeyCode.R))
		{
			Reset();
		}
	}
}
