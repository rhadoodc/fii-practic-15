﻿using UnityEngine;
using System.Collections;
using SprocketTools.Base;

public class BowlingPinController : MonoBehaviour 
{
	public Transform centerOfMass;

	private Vector3 initialPosition;

	public Rigidbody rigidbodyComponent = null;

	// Use this for initialization
	void Awake () 
	{
		KGameObjectSingleton<PinManager>.Instance.RegisterPin(this);

		initialPosition = transform.position;

		gameObject.CheckAndInitializeWithInterface(ref rigidbodyComponent);

		rigidbodyComponent.centerOfMass = centerOfMass.localPosition;
	}

	void Start()
	{
		Reset();
	}

	private void Reset()
	{
		transform.position = initialPosition;

		transform.rotation = Quaternion.identity;

		rigidbodyComponent.velocity = Vector3.zero;
		rigidbodyComponent.angularVelocity = Vector3.zero;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (Input.GetKeyUp(KeyCode.R))
		{
			Reset();
		}
	}

	public bool IsGrounded()
	{
		return Vector3.Dot(transform.up, Vector3.up) > 0.9f;
	}

	public void OnDrawGizmos()
	{
		var col = Gizmos.color;
		
		Gizmos.color = Color.red;
		
		Gizmos.DrawLine(transform.position, transform.position - transform.up * 0.3f);

		Gizmos.color = col;
	}
}
